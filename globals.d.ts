declare module "*.png";

declare var API_URL: string;
declare var SHOULD_MOCK_API: boolean;
declare var SHOULD_RUN_WDYR: boolean;

declare var process: {
  nextTick: () => void;
  env: {
    NODE_ENV: "development" | "production";
  };
};
