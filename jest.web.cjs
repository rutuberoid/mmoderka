const commonConfig = require("./jest.base.cjs");

const config = {
  ...commonConfig,
  testMatch: ["<rootDir>/src/tests/components/*.spec.[jt]s?(x)", "<rootDir>/src/tests/app/*.spec.[jt]s?(x)"],
  testEnvironment: "jsdom",
};

module.exports = config;
