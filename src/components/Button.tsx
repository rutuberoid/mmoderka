import React from "react";
import type { FC, ReactNode } from "react";

export type ButtonProps = {
  onClick: () => void;
  children: ReactNode;
};

export const Button: FC<ButtonProps> = ({ children, onClick }) => <button onClick={() => onClick()}>{children}</button>;
