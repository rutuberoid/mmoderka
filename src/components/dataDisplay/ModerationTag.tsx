import React from "react";
import { Stack, Typography } from "@mui/material";
import { SentimentSatisfiedAlt, SportsSoccer } from "@mui/icons-material";
import { deepOrange, deepPurple, grey } from "@mui/material/colors";
import { commentTranslations } from "@mimir/constants";

type ContentType = "children" | "sport" | string;

type ModerationTagProps = {
  contentType: ContentType;
};

const contentMapper = (contentType: ContentType) => {
  switch (contentType) {
    case "children":
      return {
        text: commentTranslations.labels.CHILDISH_CONTENT,
        Icon: SentimentSatisfiedAlt,
        styles: {
          backgroundColor: deepPurple[50],
          color: deepPurple[700],
        },
      };
    case "sport":
      return {
        text: commentTranslations.labels.SPORT_CONTENT,
        Icon: SportsSoccer,
        styles: {
          backgroundColor: deepOrange[50],
          color: deepOrange[700],
        },
      };
    default:
      return {
        text: contentType,
        styles: {
          backgroundColor: grey[300],
        },
      };
  }
};

const ModerationTag = ({ contentType }: ModerationTagProps) => {
  const { text, Icon, styles } = contentMapper(contentType);

  return (
    <Stack
      direction={"row"}
      display={"inline-flex"}
      alignItems={"center"}
      spacing={0.75}
      paddingX={1}
      paddingY={0.5}
      borderRadius={1}
      sx={styles}
    >
      {Icon && <Icon sx={{ fontSize: 16 }} />}
      <Typography variant={"caption"}>{text}</Typography>
    </Stack>
  );
};

export { ModerationTag };
