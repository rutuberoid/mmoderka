import type { ReactElement } from "react";
import React, { Fragment, isValidElement } from "react";
import type { TextsListType } from "@mimir/types";
import type { SxProps, Theme } from "@mui/material";
import { Stack, Typography } from "@mui/material";

type InlineMessageProps = {
  textsList: TextsListType;
  textsOffset?: number;
  startIcon?: ReactElement;
  endIcon?: ReactElement;
  sx?: SxProps<Theme>;
};
const InlineMessage = ({ startIcon, endIcon, textsOffset, textsList, sx = {} }: InlineMessageProps) => {
  const content = textsList.map((value, index) => {
    if (isValidElement(value)) {
      return <Fragment key={index}>{value}</Fragment>;
    }
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <Typography variant={"subtitle2"} key={index} {...value} />;
  });

  return (
    <Stack spacing={textsOffset || 1} direction={"row"} justifyContent={"flex-start"} alignItems={"center"} sx={sx}>
      <>
        {startIcon}
        {content}
        {endIcon}
      </>
    </Stack>
  );
};

export { InlineMessage };
export type { TextsListType };
