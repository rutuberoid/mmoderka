import React from "react";
import { Stack, Typography, ButtonBase, Box, Link, IconButton, Tooltip } from "@mui/material";
import { PlayArrow as PlayArrowIcon, ContentCopy as ContentCopyIcon } from "@mui/icons-material";
import { commonTranslations } from "@mimir/constants";

import { InlineMessage } from "./InlineMessage";

type VideoPreviewProps = {
  videoPreviewUrl: string;
  openVideoModal: () => void;
  videoTitle: string;
  videoUrl: string;
  videoId: string;
  copyVideoId: () => void;
};

const VideoPreview = ({
  videoPreviewUrl,
  openVideoModal,
  videoTitle,
  videoUrl,
  videoId,
  copyVideoId,
}: VideoPreviewProps) => (
  <Stack spacing={2} direction="row" justifyContent="flex-start" alignItems="center" sx={{ maxWidth: "320px" }}>
    <ButtonBase
      className="hoverVideoPreview"
      sx={{
        height: 68,
        width: 120,
        minWidth: 120,
        borderRadius: "6px",
        backgroundSize: "cover",
        backgroundColor: (theme) => theme.palette.grey[400],
        backgroundImage: `url(${videoPreviewUrl})`,
      }}
      onClick={openVideoModal}
      disableRipple
    >
      <Box className="playIcon" sx={{ display: "none" }}>
        <PlayArrowIcon sx={{ color: "#fff" }} />
      </Box>
    </ButtonBase>
    <Box sx={{ overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>
      <Tooltip
        title={videoTitle}
        placement="bottom-start"
        componentsProps={{
          tooltip: {
            sx: { maxWidth: "184px" },
          },
        }}
      >
        <Link
          href={videoUrl}
          target="_blank"
          rel="noopener noreferrer"
          variant="subtitle1"
          className="hoverVideoTitle"
          underline="none"
          color="inherit"
          sx={{
            color: (theme) => theme.palette.text.secondary,
          }}
        >
          {videoTitle}
        </Link>
      </Tooltip>
      <InlineMessage
        textsList={[
          {
            children: `${commonTranslations.labels.ID}:`,
            component: "span",
          },
          // eslint-disable-next-line react/jsx-key
          <Tooltip title={videoId} placement="bottom-start">
            <Typography variant="subtitle2" sx={{ overflow: "hidden", textOverflow: "ellipsis" }}>
              {videoId}
            </Typography>
          </Tooltip>,
        ]}
        endIcon={
          <IconButton size="small" className="hoverCopyButton" onClick={copyVideoId} sx={{ display: "none" }}>
            <ContentCopyIcon sx={{ color: (theme) => theme.palette.grey[600], fontSize: "14px" }} />
          </IconButton>
        }
        textsOffset={0.5}
        sx={{ height: "24px" }}
      />
    </Box>
  </Stack>
);

export { VideoPreview };
export type { VideoPreviewProps };
