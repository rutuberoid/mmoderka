import React from "react";
import {
  Avatar,
  ButtonBase,
  IconButton,
  Link,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  MenuList,
  Typography,
} from "@mui/material";
import { Block as BlockIcon, ContentCopy as ContentCopyIcon, FilterList as FilterListIcon } from "@mui/icons-material";
import { commonTranslations } from "@mimir/constants";
import { LogoOriginals } from "@mimir/components";
import type { TextsListType } from "@mimir/types";

import { InlineMessage } from "./InlineMessage";

type CreatorInfoPopoverProps = {
  creatorName: string;
  creatorId: string;
  creatorAvatarUrl: string;
  copyCreatorId: (creatorId: string) => void;
  isBanned: boolean;
  canFilterById?: boolean;
  creatorChannelUrl: string;
  filterByCreatorId: (creatorId: string) => void;
  banByCreatorId: (creatorId: string) => void;
};

const CreatorInfoPopover = ({
  banByCreatorId,
  copyCreatorId,
  creatorId,
  creatorAvatarUrl,
  creatorChannelUrl,
  creatorName,
  filterByCreatorId,
  canFilterById,
  isBanned,
}: CreatorInfoPopoverProps) => {
  const [menuAnchorEl, setMenuAnchorEl] = React.useState<HTMLElement | null>(null);

  const creatorTextsList: TextsListType = [
    {
      children: creatorName,
      variant: "subtitle1",
      sx: {
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
      },
    },
  ];

  if (isBanned) {
    creatorTextsList.push({
      children: commonTranslations.labels.BANNED,
      sx: {
        color: (theme) => theme.palette.error.main,
      },
    });
  }

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleCopy = () => {
    copyCreatorId?.(creatorId);
  };

  const handleFilter = () => {
    filterByCreatorId?.(creatorId);
    handleMenuClose();
  };

  const handleBan = () => {
    banByCreatorId?.(creatorId);
    handleMenuClose();
  };

  return (
    <>
      <ButtonBase onClick={handleMenuOpen} disableRipple>
        <Avatar
          src={creatorAvatarUrl}
          alt={creatorName}
          sx={{
            marginRight: 1,
            width: 24,
            height: 24,
          }}
        />
        <Typography
          className={"hoverCreator"}
          component={"span"}
          variant={"button2"}
          sx={{
            maxWidth: 200,
            color: (theme) => theme.palette.grey[600],
            textOverflow: "ellipsis",
            overflow: "hidden",
            whiteSpace: "nowrap",
          }}
        >
          {creatorName}
        </Typography>
      </ButtonBase>
      <Menu
        open={!!menuAnchorEl}
        anchorEl={menuAnchorEl}
        onClose={handleMenuClose}
        MenuListProps={{
          sx: {
            width: 306,
          },
        }}
        anchorOrigin={{
          horizontal: "left",
          vertical: "top",
        }}
        transformOrigin={{
          horizontal: 32,
          vertical: 20,
        }}
      >
        <ListItem
          sx={{
            paddingY: 0.75,
          }}
        >
          <Avatar src={creatorAvatarUrl} alt={creatorName} sx={{ marginRight: 1 }} />
          <ListItemText
            disableTypography={true}
            primary={<InlineMessage textsList={creatorTextsList} />}
            secondary={
              <InlineMessage
                endIcon={
                  <IconButton size={"small"} onClick={handleCopy}>
                    <ContentCopyIcon sx={{ fontSize: "14px" }} />
                  </IconButton>
                }
                textsOffset={0.25}
                textsList={[
                  {
                    children: `${commonTranslations.labels.ID}:`,
                    component: "span",
                  },
                  {
                    children: creatorId,
                    component: "span",
                  },
                ]}
              />
            }
            sx={{
              "&:active": {
                color: (theme) => theme.palette.grey[900],
              },
            }}
          />
        </ListItem>
        <MenuList>
          <MenuItem component={Link} href={creatorChannelUrl} target={"_blank"} rel={"noopener noreferrer"}>
            <ListItemIcon>
              <LogoOriginals />
            </ListItemIcon>
            {commonTranslations.actions.WATCH_ON_RUTUBE}
          </MenuItem>
          {canFilterById && (
            <MenuItem onClick={handleFilter}>
              <ListItemIcon>
                <FilterListIcon />
              </ListItemIcon>
              {commonTranslations.actions.FILTER_BY_ID}
            </MenuItem>
          )}
          <MenuItem onClick={handleBan}>
            <ListItemIcon>
              <BlockIcon />
            </ListItemIcon>
            {commonTranslations.actions.BAN_CREATOR}
          </MenuItem>
        </MenuList>
      </Menu>
    </>
  );
};

export { CreatorInfoPopover };
export type { CreatorInfoPopoverProps };
