import React, { useState } from "react";
import type { Theme } from "@mui/material";
import { Stack, Typography, Box, Accordion, AccordionSummary, AccordionDetails } from "@mui/material";
import { commentTranslations } from "@mimir/constants";
import { TimelineItem, TimelineSeparator, TimelineContent, TimelineConnector } from "@mui/lab";
import type { SvgIconComponent } from "@mui/icons-material";
import {
  ReportOutlined as ReportOutlinedIcon,
  DoneOutlined as DoneOutlinedIcon,
  BlockOutlined as BlockOutlinedIcon,
  CircleOutlined as CircleOutlinedIcon,
  RestartAltOutlined as RestartAltOutlinedIcon,
  DeleteOutlineOutlined as DeleteOutlineOutlinedIcon,
  ModeEditOutlineOutlined as ModeEditOutlineOutlinedIcon,
  AddCommentOutlined as AddCommentOutlinedIcon,
  DoDisturbOffOutlined as DoDisturbOffOutlinedIcon,
  RedoOutlined as RedoOutlinedIcon,
  ExpandMore as ExpandMoreIcon,
} from "@mui/icons-material";
import type { EventUserType, EventType } from "@mimir/types";

type ParamsType = { Icon: SvgIconComponent; color: (theme: Theme) => string };
const DEFAULT_PARAMS: ParamsType = {
  Icon: CircleOutlinedIcon,
  color: (theme) => theme.palette.text.disabled,
};
const PARAMS_BY_TYPE: { [key in EventType]?: ParamsType } = {
  report: {
    Icon: ReportOutlinedIcon,
    color: (theme) => theme.palette.warning.main,
  },
  approve: {
    Icon: DoneOutlinedIcon,
    color: (theme) => theme.palette.success.main,
  },
  delete: {
    Icon: DeleteOutlineOutlinedIcon,
    color: (theme) => theme.palette.error.main,
  },
  ban: {
    Icon: BlockOutlinedIcon,
    color: (theme) => theme.palette.error.main,
  },
  unban: {
    ...DEFAULT_PARAMS,
    Icon: DoDisturbOffOutlinedIcon,
  },
  edit: {
    ...DEFAULT_PARAMS,
    Icon: ModeEditOutlineOutlinedIcon,
  },
  assign: {
    ...DEFAULT_PARAMS,
    Icon: RedoOutlinedIcon,
  },
  add: {
    ...DEFAULT_PARAMS,
    Icon: AddCommentOutlinedIcon,
  },
  restore: {
    ...DEFAULT_PARAMS,
    Icon: RestartAltOutlinedIcon,
  },
};

type ModerationHistoryItemProps = {
  eventType: EventType;
  eventUserType: EventUserType;
  eventUserId: number;
  eventUserName: string;
  eventDetails: Array<string>;
  eventTime: string;
  eventDate: string;
  isFirst: boolean;
  isLast: boolean;
};

const ModerationHistoryItem = ({
  eventDate,
  eventDetails,
  eventTime,
  eventType,
  eventUserId,
  eventUserName,
  eventUserType,
  isFirst,
  isLast,
}: ModerationHistoryItemProps) => {
  const [areReasonsDisplayed, setAreReasonsDisplayed] = useState(false);

  const { Icon, color } = PARAMS_BY_TYPE[eventType] ?? DEFAULT_PARAMS;

  const title = commentTranslations.titles.history({ eventType, eventUserType, eventUserName });
  const label = commentTranslations.labels.history({ eventUserType, eventUserId });
  const eventDetailsLabel = commentTranslations.labels.eventDetails({ eventType, areReasonsDisplayed });

  return (
    <TimelineItem>
      <TimelineSeparator>
        <Icon sx={{ color, marginTop: isFirst ? 0 : 1.25, marginBottom: isLast ? 0 : 1.25 }} />
        {!isLast && <TimelineConnector />}
      </TimelineSeparator>
      <TimelineContent sx={{ px: 2, paddingTop: isFirst ? 0 : 1.5, paddingBottom: isLast ? 0 : 1.5, paddingRight: 0 }}>
        <Stack direction="row" justifyContent="space-between">
          <Box>
            <Typography variant="h5" sx={{ marginBottom: 0.25 }}>
              {title}
            </Typography>
            {label && (
              <Typography variant="body2" sx={{ color: (theme) => theme.palette.grey[600] }}>
                {label}
              </Typography>
            )}
          </Box>
          <Box
            sx={{
              marginLeft: 2,
              textAlign: "right",
            }}
          >
            <Typography variant="h5" component="span">
              {eventDate}
            </Typography>
            <Typography variant="body2" sx={{ color: (theme) => theme.palette.grey[600] }}>
              {eventTime}
            </Typography>
          </Box>
        </Stack>
        {eventDetailsLabel && Boolean(eventDetails.length) && (
          <Accordion
            elevation={0}
            sx={{
              "marginTop": 2,
              "paddingBottom": 1.5,
              "&:before": {
                display: "none",
              },
            }}
            square
            onClick={() => setAreReasonsDisplayed((prev) => !prev)}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon sx={{ color: (theme) => theme.palette.primary.main }} />}
              sx={{
                "color": (theme) => theme.palette.primary.main,
                "padding": 0,
                "minHeight": "unset",
                "&.Mui-expanded": {
                  padding: 0,
                  margin: 0,
                  minHeight: "unset",
                },
                "& > .Mui-expanded.MuiAccordionSummary-content": {
                  padding: 0,
                  margin: 0,
                },
                "& > .MuiAccordionSummary-content": {
                  margin: 0,
                },
                "& > .MuiAccordionSummary-expandIconWrapper": {
                  "order": -1,
                  "marginRight": 0.5,
                  "&.Mui-expanded": {
                    transform: "rotate(-180deg)",
                  },
                },
              }}
            >
              <Typography variant="button2">{eventDetailsLabel}</Typography>
            </AccordionSummary>
            {areReasonsDisplayed && (
              <AccordionDetails
                sx={{
                  padding: 0,
                  paddingTop: areReasonsDisplayed ? 0.75 : 0,
                }}
              >
                <Stack direction="column" spacing={0.75}>
                  {eventDetails.map((eventDetail, index) => (
                    <Typography key={index} variant="body1">
                      {eventDetail}
                    </Typography>
                  ))}
                </Stack>
              </AccordionDetails>
            )}
          </Accordion>
        )}
      </TimelineContent>
    </TimelineItem>
  );
};

export { ModerationHistoryItem };
export type { ModerationHistoryItemProps };
