import type { FC, SyntheticEvent } from "react";
import React from "react";
import { observer } from "mobx-react-lite";
// It is illegal to import something from 'store' here, but this case is exceptional
// also we import only types
// eslint-disable-next-line no-restricted-imports
import type { FormFieldStoreModel, FormValueStoreModel } from "@mimir/store";
import type { TextFieldProps } from "@mui/material";
import { Autocomplete, Checkbox, Chip, ListItemText, MenuItem, TextField } from "@mui/material";
import { CheckBoxOutlineBlank, CheckBox as CheckBoxIcon } from "@mui/icons-material";

export type ProtoFieldProps = {
  field: FormFieldStoreModel;
  textFieldProps?: TextFieldProps;
  changeField: (name: string, value: string | string[] | undefined) => void;
  touchField?: (name: string) => void;
  hasMassDisable?: boolean;
  hasChips?: boolean;
  variant?: TextFieldProps["variant"];
  size?: TextFieldProps["size"];
  className?: string;
};

const icon = <CheckBoxOutlineBlank fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export const ProtoField: FC<ProtoFieldProps> = observer(
  ({
    field,
    textFieldProps,
    changeField,
    touchField,
    hasMassDisable,
    variant = "outlined",
    size = "medium",
    className = "",
    hasChips = false,
  }) => {
    const label = `${field.label}${field.isRequired ? ` ${field.requiredSign}` : ""}`;
    const placeholder = field.placeholder || "";

    const handleDetailsChange = (
      event: React.SyntheticEvent<Element, Event>,
      rawValue?: FormValueStoreModel[] | FormValueStoreModel | string | null
    ) => {
      const isRawValueArray = Array.isArray(rawValue);
      const isRawValueString = typeof rawValue === "string";
      if (field.hasCompletion && field.hasMultipleChoice) {
        changeField(field.id, isRawValueArray ? (rawValue as FormValueStoreModel[]).map((item) => item.id) : []);
      } else if (field.hasCompletion && !isRawValueArray && !isRawValueString) {
        changeField(field.id, rawValue ? rawValue.id : undefined);
      } else {
        changeField(field.id, (event.target as HTMLInputElement).value);
      }
    };

    const handleClick = (_: SyntheticEvent) => {
      // removes pristine flag on the field
      if (touchField) {
        touchField(field.id);
      } else {
        field.touch();
      }
    };

    const handleInputBlur = (_: React.FocusEvent<HTMLInputElement>) => {
      field.inputBlur();
    };

    const handleInputFocus = (_: React.FocusEvent<HTMLInputElement>) => {
      field.inputFocus();
    };

    const handleAutocompleteInput = (_: any, inputValue: string, reason: string) => {
      let value = inputValue;
      if (reason === "reset") {
        value = "";
      }
      field.setSingleValueClearSelected(value);
    };

    const commonProps = {
      id: field.id,
      label,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      error: !!field.error,
      helperText: field.error,
      variant,
      placeholder,
    };

    let value = null;
    if (field.hasMultipleChoice && field.hasCompletion) {
      value = field.values.filter(({ id }) => field.multipleSelectedIds.indexOf(id) > -1);
    } else if (field.hasMultipleChoice) {
      value = field.multipleSelectedIds;
    } else if (field.hasCompletion) {
      value = field.selected ? field.selected : null;
    } else if (field.values.length > 0) {
      value = field.selected ? field.selected.id : "";
    } else {
      value = field.displayedSingleValue || field.displayedValue;
    }

    if (field.hasCompletion) {
      return (
        <Autocomplete
          multiple={field.hasMultipleChoice}
          options={field.values}
          getOptionLabel={(option: typeof field.values[number]) => option.name}
          getOptionDisabled={(option: typeof field.values[number]) => option.isDisabled}
          noOptionsText="Не найдено"
          className={className}
          size={size}
          // undefined for value prop makes autocomplete think it is uncontrolled
          // so use null instead
          value={value as FormValueStoreModel | FormValueStoreModel[]}
          /* eslint-disable-next-line react/jsx-props-no-spreading */
          {...(!field.hasMultipleChoice && {
            onInputChange: handleAutocompleteInput,
            inputValue: field.selected ? field.selected.name : field.singleValue,
          })}
          onOpen={handleClick}
          onChange={handleDetailsChange}
          disabled={hasMassDisable || field.isDisabled}
          renderInput={(params) => (
            <TextField
              /* eslint-disable-next-line react/jsx-props-no-spreading */
              {...({ ...params, ...commonProps } as TextFieldProps)}
              placeholder={placeholder}
            />
          )}
          /* eslint-disable-next-line react/jsx-props-no-spreading */
          {...(field.hasMultipleChoice && {
            renderOption: (props, option, { selected }) => (
              <li
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...props}
                key={option.id}
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...(option.isDummy && {
                  onClick: (_event) => handleDetailsChange(_event, []),
                })}
              >
                <Checkbox
                  id={option.id}
                  icon={icon}
                  checkedIcon={checkedIcon}
                  style={{ marginRight: 8 }}
                  checked={selected}
                />
                {option.name}
              </li>
            ),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            disableCloseOnSelect: true,
          })}
          openOnFocus
          clearOnBlur
        />
      );
    }

    return (
      <TextField
        /* eslint-disable-next-line react/jsx-props-no-spreading */
        {...commonProps}
        value={value}
        disabled={hasMassDisable || field.isDisabled}
        onChange={handleDetailsChange}
        className={className}
        InputProps={{
          onBlur: handleInputBlur,
          onFocus: handleInputFocus,
          onClick: handleClick,
          placeholder,
        }}
        size={size}
        /* eslint-disable-next-line react/jsx-props-no-spreading */
        {...(field.values.length > 0 && {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          select: true,
          SelectProps: {
            onOpen: handleClick,
            ...(field.hasMultipleChoice && {
              // eslint-disable-next-line @typescript-eslint/naming-convention
              multiple: field.hasMultipleChoice,
              renderValue: (_: any) =>
                hasChips ? (
                  <div>
                    {field.multipleSelectedNames.map((name) => (
                      <Chip key={name} label={name} sx={{ marginRight: 1 }} />
                    ))}
                  </div>
                ) : (
                  field.multipleSelectedNames.join(", ")
                ),
            }),
          },
        })}
        /* eslint-disable-next-line react/jsx-props-no-spreading */
        {...textFieldProps}
      >
        {field.values.length > 0 &&
          field.values.map(({ id, name, isDisabled }) => (
            <MenuItem key={id} value={id} disabled={isDisabled}>
              {field.hasMultipleChoice ? (
                <>
                  <Checkbox checked={field.multipleSelectedIds.indexOf(id) > -1} />
                  <ListItemText primary={name} />
                </>
              ) : (
                name
              )}
            </MenuItem>
          ))}
      </TextField>
    );
  }
);
