import type { ButtonProps } from "@mui/material";
import { Button } from "@mui/material";
import type { SvgIconComponent } from "@mui/icons-material";
import {
  Check as CheckIcon,
  DeleteOutline as DeleteOutlineIcon,
  RestartAlt as RestartAltIcon,
  History as HistoryIcon,
  DoDisturbOffOutlined as DoDisturbOffOutlinedIcon,
} from "@mui/icons-material";
import { commonTranslations } from "@mimir/constants";

type ActionType = "confirm" | "delete" | "restore" | "history" | "unban";
const ActionParamsByType: Record<ActionType, { Icon: SvgIconComponent; text: string; color?: ButtonProps["color"] }> = {
  confirm: { Icon: CheckIcon, text: commonTranslations.actions.CONFIRM, color: "primary" },
  delete: { Icon: DeleteOutlineIcon, text: commonTranslations.actions.DELETE, color: "error" },
  restore: { Icon: RestartAltIcon, text: commonTranslations.actions.RESTORE, color: "primary" },
  history: { Icon: HistoryIcon, text: commonTranslations.actions.HISTORY },
  unban: { Icon: DoDisturbOffOutlinedIcon, text: commonTranslations.actions.UNBAN },
};
type ModeratorActionProps = {
  actionType: ActionType;
  onAction: () => void;
};

const ModeratorAction = ({ actionType, onAction }: ModeratorActionProps) => {
  const { Icon, text, color } = ActionParamsByType[actionType];
  const variant = actionType === "history" ? "text" : "outlined";
  const colorProps: ButtonProps = color
    ? { color }
    : {
        sx: { color: (theme) => theme.palette.text.primary },
        color: "inherit",
      };

  return (
    <Button variant={variant} onClick={onAction} startIcon={<Icon />} {...colorProps}>
      {text}
    </Button>
  );
};

export { ModeratorAction };
