import type { FC } from "react";
import React from "react";
import { Circle, SquareRounded } from "@mui/icons-material";
import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { blue, grey } from "@mui/material/colors";

type NavigationSubItemProps = {
  isActive: boolean;
  text: string;
  onClick: () => void;
};

const NavigationSublistItem: FC<NavigationSubItemProps> = ({ isActive, text, onClick }) => (
  <ListItemButton
    onClick={onClick}
    sx={{
      "width": "100%",
      "minHeight": 48,
      "borderRadius": isActive ? 3 : 2,
      "color": grey[900],
      "&:hover": {
        "backgroundColor": "transparent",
        "color": blue[400],
        "& .MuiSvgIcon-root": {
          color: blue[400],
        },
      },
      "&:active": {
        color: blue[400],
      },
      ...(isActive && { color: blue[700] }),
      "& .MuiTouchRipple-root": {
        display: "none",
      },
    }}
  >
    <ListItemIcon
      sx={{
        minWidth: "initial",
      }}
    >
      {isActive ? (
        <Circle
          sx={{
            fontSize: "20px",
            color: blue[700],
            paddingRight: 1.5,
          }}
        />
      ) : (
        <SquareRounded
          sx={{
            fontSize: "18px",
            color: grey[900],
            paddingRight: 1.5,
          }}
        />
      )}
    </ListItemIcon>
    <ListItemText
      primary={text}
      sx={{
        "& > .MuiTypography-root": {
          fontWeight: 500,
        },
      }}
    />
  </ListItemButton>
);

export { NavigationSublistItem };
