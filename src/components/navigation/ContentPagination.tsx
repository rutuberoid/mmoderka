import type { ChangeEvent } from "react";
import React from "react";
import { Pagination, Stack, ButtonBase, Typography, Menu, MenuItem } from "@mui/material";
import { ArrowDropDown } from "@mui/icons-material";
import { commonTranslations } from "@mimir/constants";

type ContentPaginationProps = {
  changePage: (page: number) => void;
  count: number;
  page: number;
  maxPerPage: number;
  perPageAmount: number[];
  changeMaxPerPage: (perPage: number) => void;
};

const ContentPagination = ({
  changePage,
  count,
  page,
  maxPerPage,
  changeMaxPerPage,
  perPageAmount,
}: ContentPaginationProps) => {
  const [menuAnchorEl, setMenuAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleChangePage = (_: ChangeEvent<unknown>, newPage: number) => changePage(newPage);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleAmountChange = (amount: number) => {
    handleMenuClose();
    changeMaxPerPage(amount);
  };

  return (
    <>
      <Stack direction="row">
        <Pagination
          onChange={handleChangePage}
          count={count}
          page={page}
          shape="circular"
          sx={{
            marginLeft: "50%",
            transform: "translateX(-50%)",
          }}
        />
        <ButtonBase
          onClick={handleMenuOpen}
          disableRipple
          sx={{
            marginLeft: "auto",
          }}
        >
          <Typography variant={"body1"}>
            {commonTranslations.labels.PER_PAGE_DISPLAY} {maxPerPage}
          </Typography>
          <ArrowDropDown />
        </ButtonBase>
      </Stack>
      <Menu
        open={!!menuAnchorEl}
        anchorEl={menuAnchorEl}
        onClose={handleMenuClose}
        anchorOrigin={{
          horizontal: "right",
          vertical: "top",
        }}
        transformOrigin={{
          horizontal: 60,
          vertical: "bottom",
        }}
      >
        {perPageAmount.map((amount) => (
          <MenuItem key={amount} onClick={() => handleAmountChange(amount)}>
            {amount}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export { ContentPagination };
