import type { FC } from "react";
import React from "react";
import { Menu } from "@mui/material";
import type { NavigationListItemType } from "@mimir/types";

import { NavigationSubmenuItem } from "./NavigationSubmenuItem";

type NavigationSubmenuProps = {
  anchorEl: HTMLElement | null;
  onClose: () => void;
  onClick: () => void;
  items: NavigationListItemType[];
};

const NavigationSubmenu: FC<NavigationSubmenuProps> = ({ anchorEl, onClick, onClose, items }) => (
  <Menu
    open={!!anchorEl}
    anchorEl={anchorEl}
    onClose={onClose}
    anchorOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: 4,
      horizontal: -12,
    }}
  >
    {items.map(({ text, isActive }) => (
      <NavigationSubmenuItem key={text} text={text} onClick={onClick} isActive={isActive} />
    ))}
  </Menu>
);

export { NavigationSubmenu };
