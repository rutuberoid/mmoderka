import type { FC, ReactNode } from "react";
import React from "react";
import type { SxProps, Theme } from "@mui/material";
import { Divider, Drawer, List } from "@mui/material";
import {
  VideocamOutlined,
  ChatOutlined,
  PodcastsOutlined,
  VolumeUpOutlined,
  MenuBookOutlined,
  DashboardOutlined,
  GroupOutlined,
} from "@mui/icons-material";
import { commonTranslations } from "@mimir/constants";
import type { NavigationListItemType } from "@mimir/types";

import { NavigationItem } from "./NavigationItem";

type NavigationProps = {
  isWide: boolean;
};

type NavItemType = {
  id: string;
  text: string;
  icon: ReactNode;
};

const menuTransition = (): SxProps<Theme> => ({
  transition: (theme) =>
    theme.transitions.create(["width", "padding"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
});

const drawerOpenedMixin = (): SxProps<Theme> => ({
  paddingX: 2,
  width: 280,
  borderRight: 0,
  ...menuTransition(),
});

const drawerClosedMixin = (): SxProps<Theme> => ({
  paddingX: 0.75,
  overflowX: "hidden",
  width: 78,
  borderRight: 0,
  ...menuTransition(),
});

const DASHBOARD_ITEMS: NavItemType[] = [
  {
    id: "dashboard",
    text: commonTranslations.titles.DASHBOARD,
    icon: <DashboardOutlined />,
  },
];
const MODERATION_ITEMS: NavItemType[] = [
  { id: "comments-and-chats", text: commonTranslations.titles.COMMENTS_AND_CHATS, icon: <ChatOutlined /> },
  { id: "video", text: commonTranslations.titles.VIDEO, icon: <VideocamOutlined /> },
  { id: "broadcasts", text: commonTranslations.titles.BROADCASTS, icon: <PodcastsOutlined /> },
  { id: "audio", text: commonTranslations.titles.AUDIO, icon: <VolumeUpOutlined /> },
];
const ADMIN_ITEMS: NavItemType[] = [
  { id: "dictionary", text: commonTranslations.titles.DICTIONARY, icon: <MenuBookOutlined /> },
  { id: "moderators", text: commonTranslations.titles.MODERATORS, icon: <GroupOutlined /> },
];
const TEMP_SUB_ITEMS: NavigationListItemType[] = [
  { text: "First", isActive: true },
  { text: "Second", isActive: false },
  { text: "Third", isActive: false },
];
const navItemMapper = (items: NavItemType[], isWide: boolean) =>
  items.map(({ id, text, icon }) => (
    <NavigationItem key={id} icon={icon} isWide={isWide} text={text} isActive={false} subItems={TEMP_SUB_ITEMS} />
  ));

const Navigation: FC<NavigationProps> = ({ isWide }) => {
  const dashboardItems = navItemMapper(DASHBOARD_ITEMS, isWide);
  const moderationItems = navItemMapper(MODERATION_ITEMS, isWide);
  const adminItems = navItemMapper(ADMIN_ITEMS, isWide);

  return (
    <Drawer variant="permanent" open={isWide} PaperProps={{ style: { border: 0, position: "static" } }}>
      <List
        disablePadding
        sx={{
          flexShrink: "0",
          whiteSpace: "nowrap",
          boxSizing: "border-box",
          paddingY: 1,
          borderRight: 0,
          zIndex: "1",
          ...drawerOpenedMixin(),
          ...(isWide && {
            "paddingX": 2,
            "width": 280,
            "borderRight": 0,
            ".MuiDrawer-paper": {
              marginTop: 8,
              ...drawerOpenedMixin(),
            },
          }),
          ...(!isWide && {
            "paddingX": 2,
            "overflowX": "hidden",
            "width": 100,
            "borderRight": 0,
            ".MuiDrawer-paper": {
              marginTop: 8,
              ...drawerClosedMixin(),
            },
          }),
        }}
      >
        {dashboardItems}
        <Divider
          sx={{
            marginY: 1,
          }}
        />
        {moderationItems}
        <Divider
          sx={{
            marginY: 1,
          }}
        />
        {adminItems}
      </List>
    </Drawer>
  );
};

export { Navigation };
