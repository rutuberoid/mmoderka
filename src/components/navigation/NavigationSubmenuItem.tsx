import type { FC } from "react";
import React from "react";
import { MenuItem } from "@mui/material";

type NavigationSubmenuItemProps = {
  text: string;
  onClick: () => void;
  isActive: boolean;
};

const NavigationSubmenuItem: FC<NavigationSubmenuItemProps> = ({ text, onClick, isActive }) => (
  <MenuItem
    onClick={onClick}
    sx={{
      paddingY: 1.5,
      height: "49px",
      ...(isActive && { backgroundColor: (theme) => theme.palette.custom.blue8p }),
    }}
  >
    {text}
  </MenuItem>
);

export { NavigationSubmenuItem };
