import type { FC, ReactNode } from "react";
import React, { useState } from "react";
import { Box, ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { ChevronRightOutlined, ExpandLessRounded, ExpandMoreRounded } from "@mui/icons-material";
import type { NavigationListItemType } from "@mimir/types";
import { blue, grey } from "@mui/material/colors";

import { NavigationSubmenu } from "./NavigationSubmenu";
import { NavigationSublist } from "./NavigationSublist";

type NavigationItemProps = {
  isWide: boolean;
  text: string;
  icon: ReactNode;
  isActive: boolean;
  subItems?: NavigationListItemType[];
};

const NavigationItem: FC<NavigationItemProps> = ({ isWide, text, icon, isActive, subItems = [] }) => {
  const [isSubListVisible, setIsSubListVisible] = useState(isActive);
  const handleSublistToggle = () => {
    setIsSubListVisible((prev) => !prev);
  };
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleSubmenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleSubmenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box
      component={"li"}
      sx={{
        "display": "flex",
        "flexDirection": "column",
        "alignItems": "stretch",
        "padding": 0,
        "& + &": {
          marginTop: 0.5,
        },
      }}
    >
      <ListItemButton
        onClick={isWide ? handleSublistToggle : handleSubmenuOpen}
        sx={{
          width: "100%",
          minHeight: 52,
          borderRadius: 2,
          ...(isActive && { backgroundColor: (theme) => theme.palette.custom.blue8p }),
          ...(isWide && { paddingRight: 1.5 }),
        }}
      >
        <ListItemIcon
          sx={{
            minWidth: 0,
            mr: 1,
            justifyContent: "center",
            color: isActive ? blue[700] : "inherit",
          }}
        >
          {icon}
          {!isWide && (
            <ChevronRightOutlined
              sx={{
                color: grey[600],
              }}
            />
          )}
        </ListItemIcon>
        <>
          <ListItemText
            primary={text}
            sx={{
              "color": isActive ? blue[700] : "inherit",
              "& > .MuiTypography-root": {
                fontWeight: 500,
              },
              "visibility": isWide ? "initial" : "hidden",
            }}
          />
          {isWide &&
            (isSubListVisible ? (
              <ExpandLessRounded
                sx={{
                  color: grey[600],
                }}
              />
            ) : (
              <ExpandMoreRounded
                sx={{
                  color: grey[600],
                }}
              />
            ))}
        </>
      </ListItemButton>
      {!isWide && (
        <NavigationSubmenu
          anchorEl={anchorEl}
          onClick={() => undefined}
          onClose={handleSubmenuClose}
          items={subItems}
        />
      )}
      {isWide && <NavigationSublist isOpen={isSubListVisible} onClick={() => undefined} items={subItems} />}
    </Box>
  );
};

export { NavigationItem };
