import { Collapse, List } from "@mui/material";
import type { FC } from "react";
import React from "react";
import type { NavigationListItemType } from "@mimir/types";
import { grey } from "@mui/material/colors";

import { NavigationSublistItem } from "./NavigationSublistItem";

type NavigationSublistProps = {
  isOpen: boolean;
  items: NavigationListItemType[];
  onClick: () => void;
};
const NavigationSublist: FC<NavigationSublistProps> = ({ isOpen, items, onClick }) => (
  <Collapse in={isOpen} timeout="auto" unmountOnExit>
    <List
      component="div"
      disablePadding
      sx={{
        display: "flex",
        flexDirection: "column",
        padding: 0,
        alignSelf: "stretch",
        marginTop: 0.5,
        marginLeft: 3.25,
        borderLeft: "1px solid",
        borderColor: grey[300],
      }}
    >
      {items.map(({ text, isActive }) => (
        <NavigationSublistItem key={text} text={text} isActive={isActive} onClick={onClick} />
      ))}
    </List>
  </Collapse>
);

export { NavigationSublist };
