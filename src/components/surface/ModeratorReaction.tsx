import React from "react";
import { Avatar, Box, Paper, Tooltip, Typography } from "@mui/material";
import type { ReactionCodeType, TextsListType } from "@mimir/types";
import { InlineMessage } from "@mimir/components";
import { PolicyOutlined, TimerOffOutlined } from "@mui/icons-material";
import { commentTranslations, commonTranslations } from "@mimir/constants";

type ReactionType = "MANUAL" | "AUTO" | "OVERDUE" | "RADIOCONTROL";
type ReasonType = { shortReason: string; verboseReason: string };

type ModeratorReactionProps = {
  avatarUrl?: string | undefined;
  dateUntil?: string | undefined;
  reactionDate?: string | undefined;
  moderatorName: string;
  reactionCode: ReactionCodeType;
  reactionType?: ReactionType;
  reasons: Array<ReasonType>;
};

type StartIconArgsType = Pick<ModeratorReactionProps, "reactionType" | "moderatorName" | "avatarUrl">;
type TextsListArgsType = Omit<ModeratorReactionProps, "reasons" | "avatarUrl" | "dateUntil">;

const getStartIcon = ({ reactionType, moderatorName, avatarUrl = "" }: StartIconArgsType) => {
  if (reactionType === "AUTO" || reactionType === "RADIOCONTROL") {
    return <PolicyOutlined sx={{ color: (theme) => theme.palette.grey[600] }} />;
  }
  if (reactionType === "OVERDUE") {
    return <TimerOffOutlined sx={{ color: (theme) => theme.palette.grey[600] }} />;
  }
  return <Avatar src={avatarUrl} alt={moderatorName} sx={{ width: 24, height: 24 }} />;
};

const getTextsList = ({ reactionType, reactionCode, moderatorName, reactionDate }: TextsListArgsType) => {
  const list: TextsListType = [];

  if (reactionType === "OVERDUE") {
    list.push({ children: commentTranslations.labels.OVERDUE });
  } else if (reactionType === "MANUAL" || reactionType === "AUTO" || reactionType === "RADIOCONTROL") {
    const isManual = reactionType === "MANUAL";
    list.push(
      <Typography variant={"subtitle2"}>
        {isManual ? commentTranslations.labels[reactionCode] : commentTranslations.labels[`DELETION_${reactionType}`]}{" "}
        <Typography variant={"subtitle2"} sx={{ fontWeight: 500 }} component={"span"}>
          {isManual ? moderatorName : commentTranslations.labels[`DELETED_BY_${reactionType}`]}
        </Typography>
      </Typography>
    );
  }

  list.push({ children: <>&#8226;</> }, { children: reactionDate });

  return list;
};

const getReasonsTextsList = (reasons: ReasonType[]): TextsListType => {
  if (!reasons?.length) {
    return [];
  }
  const reasonsFormatted = reasons.map(({ shortReason, verboseReason }, index) => (
    <Tooltip key={shortReason} sx={{ maxWidth: "500px" }} title={verboseReason} placement={"bottom-start"}>
      <Typography
        variant={"subtitle2"}
        sx={{
          textDecorationStyle: "dashed",
          textDecorationLine: "underline",
          textUnderlineOffset: "3px",
        }}
      >
        {shortReason}
        {index === reasons.length - 1 ? "" : ","}
      </Typography>
    </Tooltip>
  ));
  return [{ children: `${commentTranslations.labels.REASON}:` }, ...reasonsFormatted];
};

const ModeratorReaction = ({
  avatarUrl,
  dateUntil,
  moderatorName,
  reactionCode,
  reactionDate,
  reactionType = "MANUAL",
  reasons,
}: ModeratorReactionProps) => {
  const startIcon = getStartIcon({ reactionType, moderatorName, avatarUrl });
  const textsList = getTextsList({ reactionType, reactionCode, moderatorName, reactionDate });
  const reasonsTextsList = getReasonsTextsList(reasons);

  return (
    <Box>
      <Paper
        elevation={0}
        sx={{
          background: (theme) => theme.palette.grey[100],
          color: (theme) => theme.palette.grey[600],
          padding: 1,
          borderRadius: 1,
        }}
      >
        <InlineMessage startIcon={startIcon} textsList={textsList} />
        {dateUntil && (
          <Typography sx={{ marginLeft: 4 }} variant={"subtitle2"}>
            {commentTranslations.labels.TERM}: {commonTranslations.prepositions.UNTIL.toLowerCase()} {dateUntil}
          </Typography>
        )}
        {reasons?.length > 0 && <InlineMessage sx={{ marginLeft: 4 }} textsList={reasonsTextsList} />}
      </Paper>
    </Box>
  );
};

export { ModeratorReaction };
