import React, { useEffect, useState } from "react";
import type { SlideProps } from "@mui/material";
import { Box, Button, CircularProgress, Slide, Snackbar, SnackbarContent, Stack, Typography } from "@mui/material";
import { commentTranslations } from "@mimir/constants";

type MessageType = {
  id: string;
  text: string;
};

type ProgressSnackbarProps = {
  canOpen: boolean;
  onClose: () => void;
  message: MessageType;
};

type TransitionProps = Omit<SlideProps, "direction" | "appear">;

// eslint-disable-next-line react/jsx-props-no-spreading
const SlideTransition = (props: TransitionProps) => <Slide {...props} direction="right" />;

const ProgressSnackbar = ({ canOpen, onClose, message }: ProgressSnackbarProps) => {
  const [progress, setProgress] = useState<number>(100);
  const [timer, setTimer] = useState<number | null>(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [snackPack, setSnackPack] = useState<MessageType[]>([]);
  const [messageInfo, setMessageInfo] = useState<MessageType | undefined>(undefined);

  const handleClose = () => {
    setIsOpen(false);
    onClose();
  };

  useEffect(() => {
    if (snackPack.length && !messageInfo) {
      setMessageInfo(snackPack[0]);
      setSnackPack((prev) => prev.slice(1));
      setIsOpen(true);
    } else if (snackPack.length && messageInfo && isOpen) {
      setIsOpen(false);
    }
  }, [snackPack, messageInfo, isOpen]);

  useEffect(() => {
    if (canOpen) {
      const timerId = window.setInterval(() => {
        setProgress((prevProgress) => {
          let currentProgress = 0;
          if (prevProgress > 0) {
            currentProgress = prevProgress - 20;
          }
          return currentProgress;
        });
      }, 800);

      setTimer((prevTimer) => {
        if (prevTimer) {
          clearInterval(prevTimer);
          setProgress(100);
        }

        return timerId;
      });
      setSnackPack((prev) => [...prev, message]);
    }
  }, [canOpen, message]);

  useEffect(() => {
    if (progress === 0) {
      setIsOpen(false);
      timer && clearInterval(timer);
    }
  }, [progress, timer]);

  const messageContent = messageInfo && (
    <Stack direction={"row"} spacing={1.5} alignItems={"center"}>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          height: "fit-content",
        }}
      >
        <CircularProgress variant={"determinate"} value={progress} sx={{ color: "white" }} />
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%,-50%)",
          }}
        >
          <Typography variant={"h5"} component={"div"}>
            {Math.round(progress / 20)}
          </Typography>
        </Box>
      </Box>
      <Box sx={{ width: 182 }}>
        <Typography variant={"body2"}>{commentTranslations.notifications.COMMENT_DELETED}</Typography>
        <Typography
          variant={"subtitle2"}
          sx={{
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            color: "white",
          }}
        >
          {messageInfo.text}
        </Typography>
      </Box>
    </Stack>
  );

  const actionContent = messageInfo && (
    <Button onClick={handleClose} color={"info"}>
      {commentTranslations.notifications.RESTORE}
    </Button>
  );

  return (
    <Snackbar
      key={messageInfo?.id || undefined}
      open={isOpen}
      TransitionComponent={SlideTransition}
      TransitionProps={{
        onExited: () => setMessageInfo(undefined),
      }}
    >
      <SnackbarContent message={messageContent} action={actionContent} />
    </Snackbar>
  );
};

export { ProgressSnackbar };
export type { ProgressSnackbarProps, MessageType };
