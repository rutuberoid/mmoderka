import React from "react";
import type { FC } from "react";

export type TodoItem = {
  todoId: string;
};

export const TodoItem: FC<TodoItem> = ({ todoId }) => <div>todo # {todoId}</div>;
