import React from "react";
import type { FC, ReactNode } from "react";
import { Box, Stack } from "@mui/material";

type CommentsLayoutProps = {
  commentsHeader: ReactNode;
  commentsToolbox?: ReactNode;
  commentsList: ReactNode;
  commentsGap?: number;
  paginationTool?: ReactNode;
};

const CommentsLayout: FC<CommentsLayoutProps> = ({
  commentsHeader,
  commentsToolbox,
  commentsList,
  commentsGap,
  paginationTool,
}) => (
  <>
    <Box>{commentsHeader}</Box>
    {commentsToolbox && (
      <Box
        sx={{
          marginTop: 2.5,
        }}
      >
        {commentsToolbox}
      </Box>
    )}
    <Stack
      direction={"column"}
      justifyContent={"flex-start"}
      alignItems={"stretch"}
      spacing={commentsGap || 1.5}
      mt={2.5}
    >
      {commentsList}
    </Stack>
    {paginationTool && (
      <Box
        sx={{
          marginTop: 2.5,
        }}
      >
        {paginationTool}
      </Box>
    )}
  </>
);

export { CommentsLayout };
