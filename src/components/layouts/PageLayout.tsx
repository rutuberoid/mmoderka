import React from "react";
import type { FC, ReactNode } from "react";
import { Box, Container, Paper } from "@mui/material";

type LayoutProps = {
  pageHeader: ReactNode;
  navPanel: ReactNode;
  content: ReactNode;
};

const PageLayout: FC<LayoutProps> = ({ pageHeader, navPanel, content }) => (
  <Box
    sx={{
      minHeight: "100vh",
      width: "100%",
    }}
  >
    <Paper
      sx={{
        display: "flex",
        flexWrap: "wrap",
        alignContent: "flex-start",
      }}
      elevation={0}
      square
    >
      <Box
        sx={{
          flexGrow: 1,
          flexBasis: "100%",
          height: (theme) => theme.custom.appBarHeight,
        }}
      >
        {pageHeader}
      </Box>
      <Box
        sx={{
          position: "sticky",
          top: (theme) => theme.custom.appBarHeight,
          flexGrow: 0,
          height: (theme) => `calc(100vh - ${theme.custom.appBarHeight})`,
        }}
      >
        <Paper elevation={0} square>
          {navPanel}
        </Paper>
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          marginRight: 1,
          marginBottom: 1,
        }}
      >
        <Paper
          sx={{
            padding: "20px 0 24px",
            height: "100%",
            backgroundColor: (theme) => theme.palette.grey["100"],
            borderRadius: 2,
          }}
          elevation={0}
          square
        >
          <Container maxWidth={"xl"}>{content}</Container>
        </Paper>
      </Box>
    </Paper>
  </Box>
);

export { PageLayout };
