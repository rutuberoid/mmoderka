import { matchPathToHandler } from "@mimir/utils";
import type { URLParametersHandlerType, URLParametersType } from "@mimir/types";

type TestDataType = {
  routeHandlers: Record<string, URLParametersHandlerType>;
  handlerSpies: Record<string, () => void>;
  paths: Record<string, string>;
  handlerParams: Record<string, URLParametersType>;
};
const generateData = (newData?: Partial<TestDataType>) => {
  const todoRouteHandlerSpy = jest.fn();
  const todoDocumentRouteHandlerSpy = jest.fn();
  const todoDescriptionRouteHandlerSpy = jest.fn();
  return {
    routeHandlers: {
      "/todos": todoRouteHandlerSpy,
      "/todos/:todoId": todoDocumentRouteHandlerSpy,
      "/todos/:todoId/:descriptionId": todoDescriptionRouteHandlerSpy,
    },
    handlerSpies: {
      todoRouteHandlerSpy,
      todoDocumentRouteHandlerSpy,
      todoDescriptionRouteHandlerSpy,
    },
    paths: {
      todosPath: "/todos",
      todoDocumentPath: "/todos/1",
      todoDescriptionPath: "/todos/1/2",
    },
    handlerParams: {
      todoDocument: { todoId: "1" },
      todoDescription: {
        todoId: "1",
        descriptionId: "2",
      },
    },
    ...newData,
  };
};

describe("matchPathToHandler", () => {
  it("Should signal if named parameters were matched in path", () => {
    const { routeHandlers, paths } = generateData();
    const pathMatcher = matchPathToHandler(routeHandlers);

    expect(pathMatcher(paths.todosPath)).toBeTruthy();
    expect(pathMatcher(paths.todoDocumentPath)).toBeTruthy();
    expect(pathMatcher(paths.todoDescriptionPath)).toBeTruthy();
    // Mismatch
    expect(pathMatcher("/todo/1/2")).toBeFalsy();
    expect(pathMatcher("/todos/1/2/3")).toBeFalsy();
  });

  it("Should call path handlers with correct named parameters", () => {
    const { routeHandlers, paths, handlerSpies, handlerParams } = generateData();
    const pathMatcher = matchPathToHandler(routeHandlers);

    pathMatcher(paths.todosPath);
    expect(handlerSpies.todoRouteHandlerSpy).toHaveBeenCalledTimes(1);
    expect(handlerSpies.todoRouteHandlerSpy).toHaveBeenCalledWith();

    pathMatcher(paths.todoDocumentPath);
    expect(handlerSpies.todoDocumentRouteHandlerSpy).toHaveBeenCalledTimes(1);
    expect(handlerSpies.todoDocumentRouteHandlerSpy).toHaveBeenCalledWith(handlerParams.todoDocument);

    pathMatcher(paths.todoDescriptionPath);
    expect(handlerSpies.todoDescriptionRouteHandlerSpy).toHaveBeenCalledTimes(1);
    expect(handlerSpies.todoDescriptionRouteHandlerSpy).toHaveBeenCalledWith(handlerParams.todoDescription);
  });
});
