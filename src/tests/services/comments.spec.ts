import { COMMENT_ITEM, HISTORY_ITEM, userLogout } from "@mimir/mocks";
import {
  getAuthorizeUserInfo,
  getCommentTaskHistoryList,
  getCommentTasksList,
  getSearchCommentTasksList,
  postApproveCommentTask,
  postCommentTasksList,
  postRejectCommentTask,
} from "@mimir/services";

beforeEach(userLogout);

describe("CommentsAPI: get assigned tasks", () => {
  it("Should return auth error", async () => {
    try {
      await getCommentTasksList();
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a comment-task list", async () => {
    await getAuthorizeUserInfo();

    const response = await getCommentTasksList();
    const dataItem = response.data[0];

    expect(dataItem).toEqual(COMMENT_ITEM);
  });
});

describe("CommentsAPI: assign tasks", () => {
  it("Should return auth error", async () => {
    try {
      await postCommentTasksList({ limit: 1 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a comment-task item", async () => {
    await getAuthorizeUserInfo();

    const response = await postCommentTasksList({ limit: 1 });
    expect(response.data[0]).toEqual(COMMENT_ITEM);
    expect(response.total_count).toEqual(1);
  });
});

describe("CommentsAPI: approve comment", () => {
  it("Should return auth error", async () => {
    try {
      await postApproveCommentTask({ taskId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(postApproveCommentTask({ taskId: 0 })).resolves.toBeTruthy();
  });
});

describe("CommentsAPI: reject comment", () => {
  it("Should return auth error", async () => {
    try {
      await postRejectCommentTask({ taskId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(postRejectCommentTask({ taskId: 0 })).resolves.toBeTruthy();
  });
});

describe("CommentsAPI: get history", () => {
  it("Should return auth error", async () => {
    try {
      await getCommentTaskHistoryList({ taskId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return array of history items", async () => {
    await getAuthorizeUserInfo();

    const response = await getCommentTaskHistoryList({ taskId: 0 });
    expect(response.data[0]).toEqual(HISTORY_ITEM);
  });
});

describe("CommentsAPI: Search comments", () => {
  it("Should return auth error", async () => {
    try {
      await getSearchCommentTasksList();
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return array of comment items", async () => {
    await getAuthorizeUserInfo();

    const response = await getSearchCommentTasksList({ limit: 1 });
    expect(response.data[0]).toEqual(COMMENT_ITEM);
    expect(response.total_count).toEqual(1);
  });
});
