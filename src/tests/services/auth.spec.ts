import { getAuthorizeUserInfo, postLogoutUser } from "@mimir/services";
import { AUTHORIZED_USER_INFO, isUserAuthenticated, userLogout } from "@mimir/mocks";

beforeEach(userLogout);

describe("AuthAPI", () => {
  it("Should be logged in", async () => {
    await expect(getAuthorizeUserInfo()).resolves.toEqual(AUTHORIZED_USER_INFO);
    expect(isUserAuthenticated()).toBeTruthy();
  });
  it("Should be logged out", async () => {
    await expect(getAuthorizeUserInfo()).resolves.toEqual(AUTHORIZED_USER_INFO);
    await expect(postLogoutUser()).resolves.toBeTruthy();
    expect(isUserAuthenticated()).toBeFalsy();
  });
});
