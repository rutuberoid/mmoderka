import { getAuthorizeUserInfo, getReasonsList } from "@mimir/services";
import { ACTION_REASONS } from "@mimir/mocks";

describe("DictionaryAPI: get reasons", () => {
  it("Should return auth error", async () => {
    try {
      await getReasonsList();
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return action-reasons array", async () => {
    await getAuthorizeUserInfo();

    expect(getReasonsList()).resolves.toEqual(ACTION_REASONS);
  });
});
