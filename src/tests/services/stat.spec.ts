import { TASKS_STATS } from "@mimir/mocks";
import { getAuthorizeUserInfo, getTaskStatsList } from "@mimir/services";

describe("StatAPI: get stats", () => {
  it("Should return auth error", async () => {
    try {
      await getTaskStatsList();
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return an object with categories", async () => {
    await getAuthorizeUserInfo();

    expect(getTaskStatsList()).resolves.toEqual(TASKS_STATS);
  });
});
