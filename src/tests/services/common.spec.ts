import axios from "axios";
import { postCommentTasksList } from "@mimir/services";
import { userLogout } from "@mimir/mocks";
import { baseURLTest } from "@mimir/utils";

beforeEach(userLogout);

describe("Request headers test:", () => {
  it("Should call genTraceId on each request", async () => {
    const genTraceId = jest.fn(() => Math.random());
    const transport = axios.create();

    transport.interceptors.request.use((config) => {
      config.headers = {
        ...config.headers,
        mimirTraceId: genTraceId(),
      };
      return config;
    });

    await transport(`${baseURLTest}/login`);
    await transport(`${baseURLTest}/login`);
    expect(genTraceId).toBeCalledTimes(2);
  });
});

describe("Request Error", () => {
  it("Should return error with message", async () => {
    try {
      await postCommentTasksList({
        limit: 1,
        errorOptions: {
          shouldShowServerError: true,
        },
      });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
      const { message, mimirTraceId } = JSON.parse(error.message);
      expect(typeof mimirTraceId).toEqual("string");
      expect(message).toEqual("Incorrect JWT token");
    }
  });
  it("Should return error without message", async () => {
    try {
      await postCommentTasksList({
        limit: 1,
      });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
      expect(error.message).toEqual("");
    }
  });
});
