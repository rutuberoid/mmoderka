import { BANNED_USER_ITEM, userLogout } from "@mimir/mocks";
import { getAuthorizeUserInfo, getBannedUsersList, postBanUser, postUnbanUser } from "@mimir/services";

beforeEach(userLogout);

describe("UserAPI: get banned users", () => {
  it("Should return auth error", async () => {
    try {
      await getBannedUsersList();
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return an array with one banned user", async () => {
    await getAuthorizeUserInfo();

    const response = await getBannedUsersList({ limit: 1 });
    expect(response.data[0]).toEqual(BANNED_USER_ITEM);
    expect(response.total_count).toEqual(1);
  });
});

describe("UserAPI: ban user", () => {
  it("Should return auth error", async () => {
    try {
      await postBanUser({ userId: 0, reasons: [1] });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return bad params error", async () => {
    try {
      await getAuthorizeUserInfo();

      await postBanUser({ userId: 0, reasons: [] });
    } catch (error) {
      expect(error.name).toEqual("BadRequestError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(postBanUser({ userId: 0, reasons: [1] })).resolves.toBeTruthy();
  });
});

describe("UserAPI: unban user", () => {
  it("Should return auth error", async () => {
    try {
      await postUnbanUser({ userId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(postUnbanUser({ userId: 0 })).resolves.toBeTruthy();
  });
});
