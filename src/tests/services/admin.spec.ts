import { deleteUser, getAuthorizeUserInfo, postCreateUser } from "@mimir/services";
import { userLogout } from "@mimir/mocks";

beforeEach(userLogout);

describe("AdminAPI: add moderator", () => {
  it("Should return auth error", async () => {
    try {
      await postCreateUser({ userId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(postCreateUser({ userId: 0 })).resolves.toBeTruthy();
  });
});

describe("AdminAPI: delete moderator", () => {
  it("Should return auth error", async () => {
    try {
      await deleteUser({ userId: 0 });
    } catch (error) {
      expect(error.name).toEqual("AccessDeniedError");
    }
  });
  it("Should return a success message", async () => {
    await getAuthorizeUserInfo();

    expect(deleteUser({ userId: 0 })).resolves.toBeTruthy();
  });
});
