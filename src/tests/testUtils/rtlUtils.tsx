import type { FC, ReactNode, ReactElement } from "react";
import type { RenderHookOptions, RenderOptions } from "@testing-library/react";
import { render, renderHook } from "@testing-library/react";
import { StoreProvider } from "@mimir/store";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const theme = createTheme();

const AllTheProviders: FC<{ children: ReactNode }> = ({ children }) => (
  <StoreProvider>
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
  </StoreProvider>
);

const customRender = (component: ReactElement, options?: RenderOptions) =>
  render(component, { wrapper: AllTheProviders, ...options });

const customRenderHook = <Result, Props>(hook: (initialProps: Props) => Result, options?: RenderHookOptions<Props>) =>
  renderHook(hook, { wrapper: AllTheProviders, ...options });

export * from "@testing-library/react";
export { customRender as render, customRenderHook as renderHook };
