import type { ParamsInitFieldType } from "@mimir/types";

const getSelectFields = (): ParamsInitFieldType[] => [
  {
    fieldAnchor: "selectField1",
    label: "selectLabel1",
    index: 1,
    placeholder: "selectPlaceholder1",
  },
  {
    fieldAnchor: "selectField2",
    label: "selectLabel2",
    index: 50,
    placeholder: "selectPlaceholder2",
  },
  {
    fieldAnchor: "selectField3",
    label: "selectLabel3",
    index: 100,
    placeholder: "selectPlaceholder3",
  },
];

const updateFunc = async () => ({
  list: [
    { name: "optionName1", id: "optionId1" },
    { name: "optionName2", id: "optionId2" },
    { name: "optionName3", id: "optionId3" },
  ],
  attributes: {
    value: "id",
    name: "name",
  },
});

const getSingleValueFields = (): ParamsInitFieldType[] => [
  {
    fieldAnchor: "singleField1",
    label: "singleLabel1",
    index: 20,
    placeholder: "singlePlaceholder1",
    singleValue: "singleValue1",
  },
  {
    fieldAnchor: "singleField2",
    label: "singleLabel2",
    index: 40,
    placeholder: "singlePlaceholder2",
    singleValue: "singleValue2",
  },
  {
    fieldAnchor: "singleField3",
    label: "singleLabel3",
    index: 60,
    placeholder: "singlePlaceholder3",
    singleValue: "singleValue3",
  },
];

export { getSingleValueFields, getSelectFields, updateFunc };
