import type { ViewHooksType } from "@test/store/models";
import type { URLParametersType } from "@mimir/types";
import { ViewStore } from "@test/store/models";

type TestDataType = {
  hooks?: ViewHooksType;
  parameters: URLParametersType;
  path: string;
  name: string;
  routingProps?: Record<string, any>;
};

const generateData = (newData?: Partial<TestDataType>): TestDataType => ({
  name: "todos",
  parameters: {
    todoId: "1",
    descriptionId: "2",
  },
  routingProps: {
    todoId: "1",
    importantMessage: "hello",
  },
  path: "/todos/:todoId/:descriptionId",
  ...newData,
});

jest.mock("@mimir/constants", () => {
  const originalModule = jest.requireActual("@mimir/constants");

  return {
    __esModule: true,
    ...originalModule,
    ROUTING_PATH_NAMES: [...originalModule.ROUTING_PATH_NAMES, "todos"],
  };
});

describe("ViewStore", () => {
  it("Should set view and expand ONLY existing parameters", () => {
    const { parameters, routingProps, ...testData } = generateData();
    const store = ViewStore.create(testData);

    expect(store.name).toEqual("todos");
    expect(store.path).toEqual(testData.path);
    expect(store.routingProps).toEqual({});
    store.setRoutingProps(routingProps!);
    expect(store.routingProps).toEqual(routingProps);
    expect(store.expandParameters(parameters)).toEqual("/todos/1/2");

    const mismatchParameters = {
      todoId: "1",
      mismatchId: "2",
    };
    expect(store.expandParameters(mismatchParameters)).toEqual("/todos/1/:descriptionId");
  });

  it("Should call hooks", async () => {
    const beforeHookSpy = jest.fn(() => true);
    const onHookSpy = jest.fn();
    const { parameters, ...testData } = generateData({
      hooks: {
        beforeEnter: beforeHookSpy,
        beforeExit: beforeHookSpy,
        onExit: onHookSpy,
        onEnter: onHookSpy,
      },
    });
    const store = ViewStore.create(testData);

    const canContinueEnter = await store.beforeEnter(parameters);
    expect(canContinueEnter).toBeTruthy();
    expect(beforeHookSpy).toHaveBeenCalledTimes(1);
    expect(beforeHookSpy).toHaveBeenCalledWith(store, parameters);
    const canContinueExit = await store.beforeExit(parameters);
    expect(canContinueExit).toBeTruthy();
    expect(beforeHookSpy).toHaveBeenCalledTimes(2);
    await store.onEnter(parameters);
    expect(onHookSpy).toHaveBeenCalledTimes(1);
    await store.onExit(parameters);
    expect(onHookSpy).toHaveBeenCalledTimes(2);
  });
});
