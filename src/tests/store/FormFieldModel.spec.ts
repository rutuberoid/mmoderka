import { FormFieldStore } from "@test/store/models";
import * as mst from "mobx-state-tree";

import { updateFunc } from "../testConstants";

const getFieldInitialData = ({ hasMultipleChoice = false } = {}) => ({
  id: "uniqueId",
  fieldAnchor: "field",
  label: "",
  singleValue: "single",
  requiredSign: "&&&",
  hasMultipleChoice,
});

describe("FormFieldModel", () => {
  it("Should set values to: isDisabled, isRequired, error, isPristine, requiredSign", () => {
    const field = FormFieldStore.create(getFieldInitialData());

    field.toggleDisabled(true);
    field.toggleRequired(true);
    field.setError("test-error");
    field.touch();

    expect(field.isDisabled).toBeTruthy();
    expect(field.isRequired).toBeTruthy();
    expect(field.error).toEqual("test-error");
    expect(field.isPristine).toBeFalsy();
    expect(field.requiredSign).toEqual("&&&");
  });
  it("Should update values and sort them", async () => {
    const field = FormFieldStore.create(getFieldInitialData());
    // Should throw an error when updated function is not assigned
    expect(field.updateValues()).rejects.toThrow("The function for updating values was not assigned.");
    field.updateFieldInitialDefinitions({
      updateFunc,
    });
    field.createSortFunc({ sortOrder: "desc" });
    expect(field?.values?.length).toEqual(0);
    await field.updateValues();
    expect(field?.values[0].name).toEqual("optionName3");
    expect(field?.values?.length).toEqual(3);
  });
  it("Should set single value with validator function and without it", async () => {
    const field = FormFieldStore.create(getFieldInitialData());

    expect(field?.singleValue).toEqual("single");
    field.setValue("test1");
    expect(field?.singleValue).toEqual("test1");

    field.updateFieldInitialDefinitions({
      inputValidatorFunc: (value) => `val-${value}`,
    });

    field.setValue("test101");
    expect(field?.singleValue).toEqual("val-test101");
  });
  it("Should reset selected and set singleValue", async () => {
    const field = FormFieldStore.create(getFieldInitialData());
    field.updateFieldInitialDefinitions({
      updateFunc,
    });
    await field.updateValues();
    field.setValue("optionId1");
    expect(field.selected).toBeTruthy();
    field.setSingleValueClearSelected("test");
    expect(field.selected).toEqual(undefined);
    expect(field.singleValue).toEqual("test");
  });
  it("Should set error and reset it on setValue", () => {
    const field = FormFieldStore.create(getFieldInitialData());
    field.touch();

    field.setError("test error");
    expect(field.error).toEqual("test error");

    field.setValue("test");
    expect(field.error).toEqual("");
  });
  it("Should assign a value to 'selected' after call setValue (select/multiSelect)", async () => {
    const field = FormFieldStore.create(getFieldInitialData({ hasMultipleChoice: true }));
    field.updateFieldInitialDefinitions({
      updateFunc,
    });

    expect(field?.values?.length).toEqual(0);
    await field.updateValues();
    expect(field?.selected).toEqual(undefined);
    field.setValue("optionId1");
    expect(field?.values?.length).toEqual(3);
    expect(field?.selected?.value).toEqual("optionId1");
    // set two options by their unique ids
    field.setValue(field.values.slice(1).map((value) => value.id));
    // check that they are assigned to field
    expect(field?.multipleSelectedNames.indexOf("optionName2") > -1).toBeTruthy();
    expect(field?.multipleSelectedNames.indexOf("optionName3") > -1).toBeTruthy();
  });
  it("Should change displayedSingleValue (blur and focus)", () => {
    const field = FormFieldStore.create(getFieldInitialData());
    field.updateFieldInitialDefinitions({
      blurFunc: (value) => `test1-${value}`,
      focusFunc: (value) => `test2-${value}`,
    });
    expect(field.displayedSingleValue.startsWith("test1-")).toBeFalsy();
    field.inputBlur();
    expect(field.displayedSingleValue.startsWith("test1-")).toBeTruthy();
    field.inputFocus();
    expect(field.displayedSingleValue.startsWith("test2-")).toBeTruthy();
  });
  it("Should format field values", () => {
    const getValueFn = (item: { name: string; id?: string; isDummy: boolean }) => item?.id || item?.isDummy;
    const field = FormFieldStore.create(getFieldInitialData());
    const [firstValue, secondValue, thirdValue] = field!.formatFieldValues({
      list: [
        { name: "First", id: "firstId", isDummy: false },
        { name: "Second", id: "secondId", isDummy: false },
        { name: "Third", isDummy: false },
      ],
      attributes: {
        value: "id",
        name: "name",
        meta: {
          field: getValueFn,
          anotherField: getValueFn,
        },
        disabled: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          firstId: true,
        },
        dummy: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          secondId: true,
        },
      },
    });

    expect(firstValue.id).not.toEqual(secondValue.id);
    expect(firstValue.name).toEqual("First");
    expect(firstValue.value).toEqual("firstId");
    expect(firstValue.meta?.length).toEqual(2);
    expect(firstValue.isDisabled).toBeTruthy();
    expect(firstValue.isDummy).toBeFalsy();

    expect(secondValue.id).not.toEqual(thirdValue.id);
    expect(secondValue.isDisabled).toBeFalsy();
    expect(secondValue.isDummy).toBeTruthy();

    expect(thirdValue.isDummy).toBeFalsy();
    expect(thirdValue.isDisabled).toBeFalsy();
  });
  it("Should createSortFunc", () => {
    const valueArray = [{ name: "a1" }, { name: "a2" }, { name: "A1" }, { name: "  a1  " }];
    const booleanArray = [{ isDummy: true }, { isDummy: false }, { isDummy: true }, { isDummy: false }];
    const store = FormFieldStore.create(getFieldInitialData());

    // With default sort params
    store.createSortFunc();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...valueArray].sort(store.sortFunc)).toEqual([
      { name: "  a1  " },
      { name: "a1" },
      { name: "A1" },
      { name: "a2" },
    ]);

    // with serialization to lowerCase
    store.createSortFunc({ shouldIgnoreCase: true });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...valueArray].sort(store.sortFunc)).toEqual([
      { name: "  a1  " },
      { name: "a1" },
      { name: "A1" },
      { name: "a2" },
    ]);

    // with trim
    store.createSortFunc({ shouldTrim: true });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...valueArray].sort(store.sortFunc)).toEqual([
      { name: "a1" },
      { name: "A1" },
      // because of 0 index, trimmed value stays on it's position
      { name: "  a1  " },
      { name: "a2" },
    ]);

    // desc sort
    store.createSortFunc({ sortOrder: "desc" });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...valueArray].sort(store.sortFunc)).toEqual([
      { name: "a2" },
      { name: "a1" },
      { name: "A1" },
      { name: "  a1  " },
    ]);

    // boolean asc (true > false)
    store.createSortFunc({ matchValue: "isDummy" });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...booleanArray].sort(store.sortFunc)).toEqual([
      { isDummy: true },
      { isDummy: true },
      { isDummy: false },
      { isDummy: false },
    ]);
    // boolean desc (true < false)
    store.createSortFunc({ matchValue: "isDummy", sortOrder: "desc" });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    expect([...booleanArray].sort(store.sortFunc)).toEqual([
      { isDummy: false },
      { isDummy: false },
      { isDummy: true },
      { isDummy: true },
    ]);
  });
  it("Should reset field to initial", () => {
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");
    const field = FormFieldStore.create(getFieldInitialData());
    field.touch();
    field.setValue("test");
    expect(field.singleValue).toEqual("test");
    field.updateFieldState();
    expect(applySnapshotSpy).toBeCalledTimes(1);
    expect(field.singleValue).toEqual("single");
  });
  it("Should update field initial definition functions", () => {
    const field = FormFieldStore.create(getFieldInitialData());

    const jestFn = jest.fn();

    expect(field.updateFunc).toEqual(null);
    expect(field.dummySelection).toEqual(null);
    expect(field.defaultSelections).toEqual(null);
    expect(field.requestParams).toEqual({});
    expect(field.inputValidatorFunc).toEqual(null);
    expect(field.valueFormatter).toEqual(null);
    expect(field.blurFunc).toEqual(null);
    expect(field.focusFunc).toEqual(null);
    expect(field.initialDefinition).not.toEqual(null);
    expect(field.nullReferenceError).toEqual(null);

    field.updateFieldInitialDefinitions({
      updateFunc: jestFn,
      dummySelection: {
        dummyName: "dumb",
        isDisabled: true,
        dummyValue: "dumbValue",
      },
      defaultSelections: {
        list: [{ id: "testId", name: "testName" }],
        attributes: {
          value: "id",
          name: "name",
        },
      },
      requestParams: { test: "test" },
      inputValidatorFunc: jestFn,
      valueFormatter: jestFn,
      blurFunc: jestFn,
      focusFunc: jestFn,
      initialDefinition: { id: "test" },
      nullReferenceError: "test null error",
    });

    expect(field.updateFunc).toEqual(jestFn);

    expect(field.dummySelection?.name).toEqual("dumb");
    expect(field.dummySelection?.value).toEqual("dumbValue");
    expect(field.dummySelection?.isDisabled).toBeTruthy();

    expect(field.defaultSelections).toEqual({
      list: [{ id: "testId", name: "testName" }],
      attributes: {
        value: "id",
        name: "name",
      },
    });

    expect(field.requestParams).toEqual({ test: "test" });
    expect(field.inputValidatorFunc).toEqual(jestFn);
    expect(field.valueFormatter).toEqual(jestFn);
    expect(field.blurFunc).toEqual(jestFn);
    expect(field.focusFunc).toEqual(jestFn);
    expect(field.initialDefinition).toEqual({ id: "test" });
    expect(field.nullReferenceError).toEqual("test null error");
  });
  it("Should return array of values from dummy and default selections", async () => {
    const field = FormFieldStore.create(getFieldInitialData());

    // case with empty defaultList
    field.updateFieldInitialDefinitions({
      defaultSelections: {
        list: [],
        attributes: {
          value: "id",
          name: "name",
        },
      },
      updateFunc,
    });
    expect(field.addDefaults().length).toEqual(0);
    await field.updateValues();
    expect(field.values.length).toEqual(3);

    // normal case
    field.updateFieldInitialDefinitions({
      dummySelection: {
        dummyName: "dumb",
        isDisabled: true,
        dummyValue: "dumbValue",
      },
      defaultSelections: {
        list: [{ id: "testId", name: "testName" }],
        attributes: {
          value: "id",
          name: "name",
        },
      },
    });

    await field.updateValues();
    expect(field.values.length).toEqual(5);
    field.setValue("dumbValue");
    expect(field.selected?.isDummy).toBeTruthy();
    // because of current logic, if we touch field and then try to setValue it throws an error
    field.touch();
    expect(() => field.setValue("optionId1")).toThrowError();
    expect(() => field.setValue(field.values[2].id)).not.toThrowError();
    expect(field.selected?.value).toEqual("optionId1");

    field.updateFieldInitialDefinitions({
      nullReferenceError: "ErrorText",
    });

    // if nullReferenceError is set earlier, it sets field.error and doesn't throw an error
    field.setValue("optionId1");
    expect(field.error).toEqual("ErrorText");
    expect(field.selected).toEqual(undefined);
  });
  it("Should return displayedValue", () => {
    const field = FormFieldStore.create(getFieldInitialData());

    expect(field.displayedValue).toEqual("single");
    field.updateFieldInitialDefinitions({
      valueFormatter: (value) => `test-${value}`,
    });
    expect(field.displayedValue).toEqual("test-single");
  });
  it("Should return multipleSelectedIds and multipleSelectedNames", async () => {
    const field = FormFieldStore.create(getFieldInitialData({ hasMultipleChoice: true }));

    field.updateFieldInitialDefinitions({
      updateFunc,
    });
    await field.updateValues();
    field.setValue(["optionId1", "optionId2"]);
    expect(field.multipleSelectedIds.length).toEqual(2);
    expect(field.multipleSelectedNames.length).toEqual(2);

    field.multipleSelectedIds.forEach((option) => {
      expect(option.startsWith("optionId")).toBeTruthy();
    });
    field.multipleSelectedNames.forEach((option) => {
      expect(option.startsWith("optionName")).toBeTruthy();
    });
  });
  it("Should reset selected values on dummy choice", async () => {
    const field = FormFieldStore.create(getFieldInitialData({ hasMultipleChoice: true }));

    field.updateFieldInitialDefinitions({
      updateFunc,
      dummySelection: {
        dummyName: "dumb",
        isDisabled: false,
        dummyValue: "dumbValue",
      },
    });

    await field.updateValues();
    field.touch();
    const [dummyValue, ...values] = field.values.map((option) => option.id);

    field.setValue(values);
    expect(field.multipleSelected.length).toEqual(values.length);

    // dummyValue on last position must reset multipleSelected to empty array
    field.setValue([...values, dummyValue]);
    expect(field.multipleSelected.length).toEqual(0);
  });
});
