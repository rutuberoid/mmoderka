import type { AuthStoreModel } from "@test/store/models";
import * as mst from "mobx-state-tree";
import qs from "qs";
import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";
import { AuthStore } from "@test/store/models";
import { genResponse } from "@mimir/mocks";

import { server } from "../../mocks/server";

type SetupType = {
  authStore: AuthStoreModel;
  hasInitialAuthorization: boolean;
};
type ParamsSetupType = Partial<SetupType>;

const setup = ({ hasInitialAuthorization = false }: ParamsSetupType = {}): SetupType => {
  const authStore = AuthStore.create({
    hasInitialAuthorization,
  });

  return {
    authStore,
    hasInitialAuthorization,
  } as SetupType;
};

describe("AuthStore", () => {
  it("Should set auth state to default values on initialization", async () => {
    const { authStore } = setup();
    expect(authStore.moderatorId).toEqual("");
    expect(authStore.moderatorName).toEqual("");
    expect(authStore.moderatorAvatarUrl).toEqual("");
    expect(authStore.hasInitialAuthorization).toEqual(false);
    expect(authStore.isLoading).toEqual(false);
  });

  it("Should set auth state to values received from service API", async () => {
    const { authStore } = setup();

    await authStore.login();
    expect(authStore.moderatorId).toEqual("123");
    expect(authStore.moderatorName).toEqual("Tester");
    expect(authStore.moderatorAvatarUrl).toEqual("https://test.url");
    expect(authStore.hasInitialAuthorization).toEqual(true);
  });

  it("Should skip login process, if initial login has already happened", async () => {
    const { authStore } = setup({ hasInitialAuthorization: true });
    const loginSpy = jest.fn();
    server.use(rest.get(`${baseURLTest}/login`, loginSpy));

    expect(authStore.hasInitialAuthorization).toEqual(true);
    await authStore.login();
    expect(loginSpy).not.toHaveBeenCalled();
  });

  it("Should redirect, if login service returned 401", async () => {
    const { authStore } = setup();
    const replaceSpy = jest.fn();
    jest.spyOn(mst, "getEnv").mockReturnValue({
      routerStore: {
        browserHistory: {
          replace: replaceSpy,
        },
      },
    });

    server.use(rest.get(`${baseURLTest}/login`, (_, res, ctx) => genResponse(res, ctx, 401)));

    const loginParams = qs.stringify({
      "referer": window.location.href,
      "new-next": window.location.href,
      "snake-yaCounter": 66791995,
      "_ym_debug": "1",
    });
    await authStore.login();
    expect(replaceSpy).toHaveBeenCalledTimes(1);
    expect(replaceSpy).toHaveBeenCalledWith(
      `http://test1.it/multipass/login/?new_login=true&login-params=${loginParams}`
    );
  });

  it("Should redirect on logout", async () => {
    const { authStore } = setup();
    const replaceSpy = jest.fn();
    jest.spyOn(mst, "getEnv").mockReturnValue({
      routerStore: {
        browserHistory: {
          replace: replaceSpy,
        },
      },
    });

    const loginParams = qs.stringify({
      "referer": window.location.href,
      "new-next": window.location.href,
      "snake-yaCounter": 66791995,
      "_ym_debug": "1",
    });
    await authStore.login();
    await authStore.logout();
    expect(replaceSpy).toHaveBeenCalledTimes(1);
    expect(replaceSpy).toHaveBeenCalledWith(
      `http://test1.it/multipass/login/?new_login=true&login-params=${loginParams}`
    );
  });

  it("Should throw, if login service returned 500", async () => {
    const { authStore } = setup();
    server.use(rest.get(`${baseURLTest}/login`, (_, res, ctx) => genResponse(res, ctx, 500)));
    try {
      await authStore.login();
    } catch (error) {
      expect(error.message).toEqual("Ошибка получения информации");
    }
  });
});
