import { FieldGroupStore } from "@test/store/models";
import * as mst from "mobx-state-tree";

import { getSelectFields, updateFunc, getSingleValueFields } from "../testConstants";

describe("FieldGroupStore", () => {
  it("Should have fields", () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();

    expect(store.hasFields).toBeFalsy();
    fields.forEach(store.initField);
    expect(store.hasFields).toBeTruthy();
  });
  it("Should return fieldIds if there's no float indexes", () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();

    fields.forEach(store.initField);

    expect(store.fieldIds.length).toEqual(3);

    store.fieldIds.forEach((idItem) => {
      expect(idItem.length).toEqual(2);
    });

    store.initField({
      fieldAnchor: "special1",
      label: "",
    });

    expect(store.fieldIds.some((tuple) => tuple[1] === "special1")).toBeTruthy();

    store.initField({
      fieldAnchor: "singleId",
      index: 3.02,
      label: "",
    });

    try {
      void store.fieldIds;
    } catch (error) {
      expect(error.message).toEqual("Cannot use this value 3.02 as an index for an array");
    }
  });
  it("Should touch field", async () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();

    expect(() => store.innerTouchField("unknown")).toThrowError("Unable to touch a nonexisting field");

    const fieldId = store.initField(fields[1]);
    const field = store.fieldGroup.get(fieldId);

    expect(field?.isPristine).toBeTruthy();
    store.innerTouchField(fieldId);
    expect(field?.isPristine).toBeFalsy();
  });
  it("Should change 'selected' field property after call innerChangeField", async () => {
    const fields = getSelectFields();
    const store = FieldGroupStore.create();

    fields.forEach(store.initField);
    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc,
      });
      await field.updateValues();
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    const firstField = store.getFieldByAttribute({ anchor: "selectField1" });
    expect(firstField?.selected).toEqual(undefined);

    try {
      store.innerChangeField("unknown", "unknown");
    } catch (error) {
      expect(error.message).toEqual("Unable to set a value to a nonexisting field");
    }

    store.innerChangeField(firstField!.id, "optionId1");

    await Promise.resolve(process.nextTick);

    expect(firstField?.selected?.value).toEqual("optionId1");
  });
  it("Should set .next/.children and reset selected of next fields on setValue", async () => {
    const fields = getSingleValueFields();

    const updateFuncSpy = jest.fn(async () => await updateFunc());

    const store = FieldGroupStore.create();
    fields.forEach(store.initField);
    store.initField({
      ...fields[0],
      fieldAnchor: "singleField4",
    });

    const field1 = store.getFieldByAttribute({ anchor: "singleField1" });
    const field2 = store.getFieldByAttribute({ anchor: "singleField2" });
    const field3 = store.getFieldByAttribute({ anchor: "singleField3" });
    const field4 = store.getFieldByAttribute({ anchor: "singleField4" });

    expect(field1).toBeTruthy();
    expect(field2).toBeTruthy();
    expect(field3).toBeTruthy();

    field1?.setChildren([field2!.id, field3!.id]);
    field3?.setChildren(field4!.id);
    expect(field1?.children.length).toEqual(2);
    expect(field3?.next).toBeTruthy();

    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc: updateFuncSpy,
      });
      await field.updateValues();
      field.setValue("optionId2");
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    expect(updateFuncSpy).toBeCalledTimes(4);

    store.fieldGroup.forEach((field) => {
      expect(field?.selected?.value).toEqual("optionId2");
    });

    const field2Values = JSON.stringify(field2?.values);
    const field3Values = JSON.stringify(field3?.values);

    field1?.setValue(field1?.values[0]?.id);
    expect(field1?.selected?.value).toEqual("optionId1");
    expect(field2?.selected).toEqual(undefined);
    expect(field3?.selected).toEqual(undefined);
    expect(field4?.selected).toEqual(undefined);

    // wait for field3 and field4 to update
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    expect(updateFuncSpy).toBeCalledTimes(6);

    // check that field has values
    expect(field2?.values.length).toBeGreaterThan(0);
    expect(field3?.values.length).toBeGreaterThan(0);
    // compare values
    expect(JSON.stringify(field2?.values)).not.toEqual(field2Values);
    expect(JSON.stringify(field3?.values)).not.toEqual(field3Values);
  });
  it("Should throw an error, if a field has both .children and .next simultaneously", async () => {
    const fields = getSingleValueFields();

    const store = FieldGroupStore.create();
    fields.forEach(store.initField);

    const field1 = store.getFieldByAttribute({ anchor: "singleField1" });
    const field2 = store.getFieldByAttribute({ anchor: "singleField2" });
    const field3 = store.getFieldByAttribute({ anchor: "singleField3" });

    expect(field1).toBeTruthy();
    expect(field2).toBeTruthy();
    expect(field3).toBeTruthy();

    field1?.setChildren([field2!.id, field3!.id]);
    field1?.setChildren(field2!.id);
    expect(field1?.children.length).toEqual(2);
    expect(field1?.next).toBeTruthy();

    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc,
      });
      await field.updateValues();
      field.setValue("optionId1");
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    try {
      await field1?.updateValues();
    } catch (error) {
      expect(error.message).toEqual("A field cannot have multiple child nodes and a single child node simultaneously.");
    }
  });
  it("Should throw an error if field has error and reset it on setValue", () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();

    fields.forEach(store.initField);
    store.fieldGroup.forEach((field) => {
      field.setValue("");
      field.toggleRequired(true);
    });

    try {
      store.detectError();
    } catch (error) {
      expect(error.message).toEqual("Required values missing");
    }

    store.fieldGroup.forEach((field) => {
      expect(field.error).toBeTruthy();
    });

    store.fieldGroup.forEach((field) => {
      field.setValue("test");
      expect(field.error).toEqual("");
    });
  });
  // FIXME: test below tests incorrect behavior
  it.skip("Should reset fields (single)", () => {
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");
    const fields = getSingleValueFields();

    const store = FieldGroupStore.create();
    fields.forEach(store.initField);

    store.fieldGroup.forEach((field) => {
      field.touch();
      field.setValue("test");
      field.toggleRequired(true);
      field.toggleDisabled(true);

      expect(field.isPristine).toBeFalsy();
      expect(field.singleValue).toEqual("test");
      expect(field.displayedSingleValue).toEqual("test");
      expect(field.isRequired).toBeTruthy();
      expect(field.isDisabled).toBeTruthy();
    });

    store.resetFields();
    expect(applySnapshotSpy).toBeCalledTimes(3);
    store.fieldGroup.forEach((field) => {
      expect(field?.singleValue).toEqual("");
      expect(field?.displayedSingleValue).toEqual("");
    });

    store.resetFieldsLeaveValuesAndSelected();
    expect(applySnapshotSpy).toBeCalledTimes(6);

    store.fieldGroup.forEach((field) => {
      expect(field?.isPristine).toBeTruthy();
      expect(field?.singleValue).toBeTruthy();
      expect(field?.displayedSingleValue).toEqual("");
      expect(field?.isRequired).toBeFalsy();
      expect(field?.isDisabled).toBeFalsy();
    });
  });
  // FIXME: test below tests incorrect behavior
  it.skip("Should reset fields (select)", async () => {
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");
    const fields = getSelectFields();
    const store = FieldGroupStore.create();

    fields.forEach(store.initField);
    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc,
      });
      await field.updateValues();
      field.setValue("optionId1");
      field.touch();
      field.toggleRequired(true);
      field.toggleDisabled(true);
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    store.fieldGroup.forEach((field) => {
      expect(field.selected).toBeTruthy();
      expect(field.isPristine).toBeFalsy();
      expect(field.isRequired).toBeTruthy();
      expect(field.isDisabled).toBeTruthy();
      expect(field.values.length).toEqual(3);
    });

    store.resetFields();
    expect(applySnapshotSpy).toBeCalledTimes(3);

    store.fieldGroup.forEach((field) => {
      expect(field.values.length).toEqual(0);
      expect(field.selected).toEqual(undefined);
    });

    store.resetFieldsLeaveValues();
    expect(applySnapshotSpy).toBeCalledTimes(6);

    store.fieldGroup.forEach((field) => {
      expect(field.values.length).toEqual(3);
      expect(field.selected).toEqual(undefined);
    });

    store.resetFieldsLeaveValuesAndSelected();
    expect(applySnapshotSpy).toBeCalledTimes(9);

    store.fieldGroup.forEach((field) => {
      expect(field.selected).toBeTruthy();
      expect(field.isPristine).toBeTruthy();
      expect(field.isRequired).toBeFalsy();
      expect(field.isDisabled).toBeFalsy();
      expect(field.values.length).toEqual(3);
    });
  });
  it("Should updateValuesLeaveAnySelection (select)", async () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();
    const updateFuncSpy = jest.fn(async () => await updateFunc());

    fields.forEach(store.initField);
    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc: updateFuncSpy,
      });
      await field.updateValues();
      field.setValue("");
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    expect(updateFuncSpy).toBeCalledTimes(3);

    store.updateValuesLeaveAnySelection(true);

    expect(updateFuncSpy).toBeCalledTimes(6);
  });
  it("Should updateValuesLeaveAnySelection (multiSelect)", async () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();
    const updateFuncSpy = jest.fn(async () => await updateFunc());

    fields.forEach((field) =>
      store.initField({
        ...field,
        hasMultipleChoice: true,
      })
    );

    store.fieldGroup.forEach(async (field) => {
      field.updateFieldInitialDefinitions({
        updateFunc: updateFuncSpy,
      });
      await field.updateValues();
      field.setValue("");
    });

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    expect(updateFuncSpy).toBeCalledTimes(3);

    store.updateValuesLeaveAnySelection(true);

    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    expect(updateFuncSpy).toBeCalledTimes(6);
  });
  it("Should return field by attribute", () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();
    const fieldId = store.initField(fields[0]);

    const fieldByAnchor = store.getFieldByAttribute({ anchor: fields[0].fieldAnchor });
    const fieldById = store.getFieldByAttribute({ fieldId });

    expect(fieldByAnchor?.fieldAnchor).toEqual(fields[0].fieldAnchor);
    expect(fieldById?.fieldAnchor).toEqual(fields[0].fieldAnchor);
    expect(store.getFieldByAttribute({ fieldId: "unknown", anchor: "unknown" })).toEqual(undefined);
  });
  it("Should throw an error for required field and set 'error' property", () => {
    const fields = getSingleValueFields();
    const store = FieldGroupStore.create();
    const customError = `${Math.random()}`.slice(1);

    const requiredFieldId = store.initField({
      fieldAnchor: "SuperField",
      label: "Hero field",
      index: 1,
      isRequired: true,
    });
    const notRequiredFieldId = store.initField(fields[0]);

    try {
      store.detectError(customError);
    } catch (error) {
      expect(error.message).toEqual("Required values missing");
    }

    const requiredField = store.fieldGroup.get(requiredFieldId);
    const notRequiredField = store.fieldGroup.get(notRequiredFieldId);

    expect(requiredField?.error).toEqual(customError);
    expect(notRequiredField?.error).toEqual(undefined);
  });
});
