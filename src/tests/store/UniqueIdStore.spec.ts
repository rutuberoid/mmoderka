import { UniqueIdStore } from "@test/store/models";

describe("UniqueIdStore:", () => {
  it("Should rewrite uniqueId on call with same prefix", () => {
    const store = UniqueIdStore.create();
    const firstValue = store.generateUniqueId("a");
    const secondValue = store.generateUniqueId("a");
    expect(store.uniqueIds.get("a")?.uniqueId).not.toEqual(firstValue);
    expect(store.uniqueIds.get("a")?.uniqueId).toEqual(secondValue);
  });
  it("Should contain different uniqueIds for different prefixes", () => {
    const store = UniqueIdStore.create();
    store.generateUniqueId("a");
    store.generateUniqueId("b");
    const objectA = store.uniqueIds.get("a");
    const objectB = store.uniqueIds.get("b");

    expect(objectA).toBeTruthy();
    expect(objectB).toBeTruthy();

    expect(objectA?.uniqueId).not.toEqual(objectB?.uniqueId);
  });
  it("Should change separator", () => {
    const store = UniqueIdStore.create();
    const fistValue = store.generateUniqueId("a");

    expect(fistValue.startsWith("a&")).toBeTruthy();

    store.setSeparator("-&-");

    const secondValue = store.generateUniqueId("b");
    expect(secondValue.startsWith("b-&-")).toBeTruthy();
  });
});
