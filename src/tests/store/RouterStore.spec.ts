import type { RouterStoreModel, ViewHooksType, ViewStoreModel, TransitionViewType } from "@test/store/models";
import type { URLParametersType, RoutingPathNamesType } from "@mimir/types";
import { createMemoryHistory } from "history";
import * as mst from "mobx-state-tree";
import { reaction, entries } from "mobx";
import { RouterStore, ViewStore } from "@test/store/models";

type TestDataType = {
  viewStates: {
    hooks?: ViewHooksType;
    path: string;
    name: RoutingPathNamesType;
    routingProps?: Record<string, any>;
    isPrivatePath?: boolean;
  }[];
  parameters: URLParametersType[];
  paths: Record<string, string>;
};

type SetupType = {
  views: Record<RoutingPathNamesType, ViewStoreModel>;
  routerStore: RouterStoreModel;
  defaultCurrentView?:
    | {
        name: string;
        parameters?: TestDataType["parameters"];
      }
    | undefined;
  testData: TestDataType;
};

type ParamsSetupType = Partial<
  Omit<SetupType, "testData"> & {
    testData: Partial<TestDataType>;
  }
>;

jest.mock("history", () => {
  const originalModule = jest.requireActual("history");

  return {
    __esModule: true,
    ...originalModule,
    createBrowserHistory: originalModule.createMemoryHistory,
  };
});

jest.mock("@mimir/constants", () => {
  const originalModule = jest.requireActual("@mimir/constants");

  return {
    __esModule: true,
    ...originalModule,
    ROUTING_PATH_NAMES: [...originalModule.ROUTING_PATH_NAMES, "home", "todo", "todos"],
  };
});

const setup = ({ testData, defaultCurrentView }: ParamsSetupType = {}): SetupType => {
  const finalTestData: TestDataType = {
    viewStates: [
      {
        name: "todo",
        path: "/todos/:todoId",
        isPrivatePath: false,
      },
      {
        name: "todos",
        path: "/todos",
        isPrivatePath: false,
      },
      {
        name: "home",
        path: "/",
        isPrivatePath: false,
      },
    ],
    paths: {
      todoDescriptionPath: "/todos/1/2",
      todoDocumentPath: "/todos/1",
      todosPath: "/todos",
      homePath: "/",
    },
    parameters: [{ todoId: "1" }],
    ...testData,
  };
  const views = finalTestData.viewStates.reduce((acc, viewState) => {
    acc[viewState.name] = ViewStore.create(viewState);
    return acc;
  }, {} as SetupType["views"]);
  const routerStore = RouterStore.create({
    views,
    currentView: defaultCurrentView ? defaultCurrentView.name : undefined,
    parameters: defaultCurrentView?.parameters || {},
  });

  return {
    testData: finalTestData,
    views: views!,
    routerStore,
    defaultCurrentView,
  };
};

describe("RouterStore", () => {
  it("Should set router state and handle state change", async () => {
    const {
      testData: { paths },
      views,
      routerStore,
    } = setup();

    expect(Object.fromEntries(entries(routerStore.views))).toEqual(views);
    expect(routerStore.currentView).toEqual(views.home);
    expect(routerStore.parameters).toEqual({});
    expect(routerStore.currentUrl).toEqual(paths.homePath);

    expect(routerStore.isLoading).toBeFalsy();
    routerStore.setLoading(true);
    expect(routerStore.isLoading).toBeTruthy();
  });

  it("Should set history in router state and perform navigation", () => {
    const history = createMemoryHistory();
    const historyBackSpy = jest.spyOn(history, "back");
    const historyForwardSpy = jest.spyOn(history, "forward");
    history.push("/home");
    const { routerStore } = setup();

    routerStore.setHistory(null);
    expect(routerStore.browserHistory).toBeNull();
    routerStore.setHistory(history);
    expect(routerStore.browserHistory).toEqual(history);
    routerStore.goBack();
    expect(historyBackSpy).toHaveBeenCalledTimes(1);
    expect(routerStore.browserHistory?.location.pathname).toEqual("/");
    routerStore.goForward();
    expect(historyForwardSpy).toHaveBeenCalledTimes(1);
    expect(routerStore.browserHistory?.location.pathname).toEqual("/home");
  });

  it("Should synchronize history with router state on view change", async () => {
    const {
      routerStore,
      testData: { viewStates, paths },
    } = setup();
    const historyPushSpy = jest.spyOn(routerStore.browserHistory!, "push");
    await routerStore.setView(viewStates[1].name);

    expect(historyPushSpy).toHaveBeenCalledTimes(1);
    expect(historyPushSpy).toHaveBeenCalledWith(paths.todosPath);
  });

  it("Should synchronize router state with history on history go back", async () => {
    const {
      routerStore,
      testData: { viewStates },
    } = setup();
    expect(routerStore.currentView?.name).toEqual(viewStates[2].name);
    await routerStore.setView(viewStates[1].name);
    expect(routerStore.currentView?.name).toEqual(viewStates[1].name);

    const setViewSpy = jest.spyOn(routerStore, "setView");
    routerStore.goBack();
    expect(setViewSpy).toHaveBeenCalledTimes(1);
    expect(setViewSpy).toHaveBeenCalledWith(viewStates[2].name, undefined);

    expect(routerStore.currentView?.name).toEqual(viewStates[2].name);
  });

  it("Should give a list of path handlers, that set a current view", async () => {
    const {
      testData: { viewStates, paths },
      views,
      routerStore,
    } = setup();

    const setViewSpy = jest.spyOn(routerStore, "setView");
    const pathHandlers = routerStore.pathHandlers;
    const path1 = viewStates[0].path;
    const path2 = viewStates[1].path;
    const path3 = viewStates[2].path;

    expect(Object.keys(pathHandlers)).toEqual([path1, path2, path3]);
    expect(typeof pathHandlers[path1]).toEqual("function");
    expect(typeof pathHandlers[path2]).toEqual("function");
    expect(typeof pathHandlers[path3]).toEqual("function");

    let newView = null;
    reaction(
      () => ({ name: routerStore.currentView?.name, url: routerStore.currentUrl }),
      (reactionView) => (newView = reactionView)
    );

    // Set another view via path handler
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    await pathHandlers[path2]();
    expect(setViewSpy).toHaveBeenCalledTimes(1);
    expect(setViewSpy).toHaveBeenCalledWith(views.todos.name, undefined);
    expect(newView).toEqual({ name: views.todos.name, url: paths.todosPath });
  });

  it("Should abort routing and restore previous state, if the previous view hook stops execution", async () => {
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");
    const beforeExitSpy = jest.fn(() => false);
    const {
      testData: { viewStates },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todos",
            path: "/todos",
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            hooks: {
              beforeExit: beforeExitSpy,
            },
            isPrivatePath: false,
          },
        ],
        parameters: [],
      },
    });

    // I cheated here, for I don't know how to catch snapshot, when isLoading is set to true
    routerStore.setLoading(true);
    const routerState = mst.getSnapshot(routerStore);
    let newView: string | undefined = undefined;
    reaction(
      () => routerStore.currentView?.name,
      (reactionViewName) => (newView = reactionViewName)
    );

    // Initial view is set automatically by synchronizing with history
    expect(routerStore.currentUrl).toEqual("/");
    expect(routerStore.currentView?.name).toEqual(viewStates[1].name);
    routerStore.setLoading(false);
    expect(routerStore.isLoading).toBeFalsy();
    // There should be no currently processed view
    expect(routerStore.runningView).toBeNull();

    await routerStore.setView(viewStates[0].name);
    expect(beforeExitSpy).toHaveBeenCalledTimes(1);
    expect(beforeExitSpy).toHaveBeenCalledWith(views.home, {});
    // View was not changed
    expect(newView).toBeUndefined();

    // Running view must reset
    expect(routerStore.runningView).toBeNull();
    expect(routerStore.currentView?.name).toEqual(viewStates[1].name);

    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);
    expect(applySnapshotSpy).toHaveBeenCalledWith(routerStore, routerState);
  });

  it("Should skip the first view change and route to the next view, if a user requested it immediately, and the initial view allows to exit it", async () => {
    const beforeExitSpy = jest.fn(() => true);
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");

    const {
      testData: { viewStates, parameters },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todo",
            path: "/todos/:todoId",
            isPrivatePath: false,
          },
          {
            name: "todos",
            path: "/todos",
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            hooks: {
              // Initial view has beforeExit hook, that allows routing from it.
              beforeExit: beforeExitSpy,
            },
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    let newRunningView: TransitionViewType = null;
    reaction(
      () => routerStore.runningView,
      (runningView) => (newRunningView = runningView)
    );
    let newQueuedView: TransitionViewType = null;
    reaction(
      () => routerStore.queuedView,
      (queuedView) => (newQueuedView = queuedView)
    );

    routerStore.setView(viewStates[1].name);
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    // Immediately set another view
    routerStore.setView(viewStates[0].name, parameters[0]);
    // The setView is still processing the first requested view
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    expect((newQueuedView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    // The first setView has finished beforeExit hook execution
    expect(routerStore.currentView?.name).toEqual(views.home.name);

    await Promise.resolve(process.nextTick);
    // Final setView is currently processed
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    expect(newQueuedView).toBeNull();
    // The current view is still "home"
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);
    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);

    // The value of ms to wait must be equal to the value of ms in the loop generator setTimeout
    await new Promise((resolve) => setTimeout(() => resolve(1), 100));
    expect(routerStore.currentView?.name).toEqual(views.todo.name);
  });

  it("Should skip the first view change and route to the next view, if a user requested it immediately, and the initial view halts on exit", async () => {
    const onExitSpy = jest.fn();
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");

    const {
      testData: { viewStates, parameters },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todo",
            path: "/todos/:todoId",
            isPrivatePath: false,
          },
          {
            name: "todos",
            path: "/todos",
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            hooks: {
              // Initial view has onExit hook, that halts synchronous routing
              onExit: onExitSpy,
            },
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    let newRunningView: TransitionViewType = null;
    reaction(
      () => routerStore.runningView,
      (runningView) => (newRunningView = runningView)
    );
    let newQueuedView: TransitionViewType = null;
    reaction(
      () => routerStore.queuedView,
      (queuedView) => (newQueuedView = queuedView)
    );

    routerStore.setView(viewStates[1].name);
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    // Immediately set another view
    routerStore.setView(viewStates[0].name, parameters[0]);
    // The setView is still processing the first requested view
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    expect((newQueuedView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    // The first setView has finished onExit hook execution
    expect(routerStore.currentView?.name).toEqual(views.home.name);

    await Promise.resolve(process.nextTick);
    // Final setView is currently processed
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    expect(newQueuedView).toBeNull();
    // The current view is still "home"
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);

    // The value of ms to wait must be equal to the value of ms in the loop generator setTimeout
    await new Promise((resolve) => setTimeout(() => resolve(1), 100));
    expect(routerStore.currentView?.name).toEqual(views.todo.name);
  });

  it("Should abort routing and restore previous state, if the next view hook stops execution", async () => {
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");
    const beforeEnterSpy = jest.fn(() => false);
    const {
      testData: { viewStates },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todos",
            path: "/todos",
            hooks: {
              beforeEnter: beforeEnterSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
        parameters: [],
      },
    });
    // I cheated here, for I don't know how to catch snapshot, when isLoading is set to true
    routerStore.setLoading(true);
    const routerState = mst.getSnapshot(routerStore);

    let newView: string | undefined = undefined;
    reaction(
      () => routerStore.currentView?.name,
      (reactionViewName) => (newView = reactionViewName)
    );
    // Initial view is set automatically by synchronizing with history
    expect(routerStore.currentUrl).toEqual("/");
    expect(routerStore.currentView?.name).toEqual(viewStates[1].name);
    routerStore.setLoading(false);
    expect(routerStore.isLoading).toBeFalsy();
    // There should be no currently processed view
    expect(routerStore.runningView).toBeNull();

    await routerStore.setView(viewStates[0].name);
    expect(beforeEnterSpy).toHaveBeenCalledTimes(1);
    expect(beforeEnterSpy).toHaveBeenCalledWith(views.todos, undefined);
    // View was not changed
    expect(newView).toBeUndefined();
    // Running view must reset
    expect(routerStore.runningView).toBeNull();
    expect(routerStore.currentView?.name).toEqual(viewStates[1].name);

    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);
    expect(applySnapshotSpy).toHaveBeenCalledWith(routerStore, routerState);
  });

  it("Should skip the first view change and route to the next view, if a user requested it immediately, and the first view allows routing to itself", async () => {
    const beforeEnterSpy = jest.fn(() => true);
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");

    const {
      testData: { viewStates, parameters },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todo",
            path: "/todos/:todoId",
            isPrivatePath: false,
          },
          {
            name: "todos",
            path: "/todos",
            hooks: {
              // Initial view has beforeEnter hook, that allows routing to it.
              beforeEnter: beforeEnterSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    let newRunningView: TransitionViewType = null;
    reaction(
      () => routerStore.runningView,
      (runningView) => (newRunningView = runningView)
    );
    let newQueuedView: TransitionViewType = null;
    reaction(
      () => routerStore.queuedView,
      (queuedView) => (newQueuedView = queuedView)
    );

    routerStore.setView(viewStates[1].name);
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    // Immediately set another view
    routerStore.setView(viewStates[0].name, parameters[0]);
    // The setView is still processing the first requested view
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    expect((newQueuedView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    // The first setView has finished beforeEnter hook execution
    expect(routerStore.currentView?.name).toEqual(views.home.name);

    await Promise.resolve(process.nextTick);
    // Final setView is currently processed
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    expect(newQueuedView).toBeNull();
    // The current view is still "home"
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);

    // The value of ms to wait must be equal to the value of ms in the loop generator setTimeout
    await new Promise((resolve) => setTimeout(() => resolve(1), 100));
    expect(routerStore.currentView?.name).toEqual(views.todo.name);
  });

  it("Should skip the first view change and route to the next view, if a user requested it immediately, and the first view halts on enter", async () => {
    const onEnterSpy = jest.fn();
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");

    const {
      testData: { viewStates, parameters },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todo",
            path: "/todos/:todoId",
            isPrivatePath: false,
          },
          {
            name: "todos",
            path: "/todos",
            hooks: {
              // Initial view has onEnter hook, that halts synchronous routing
              onEnter: onEnterSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    let newRunningView: TransitionViewType = null;
    reaction(
      () => routerStore.runningView,
      (runningView) => (newRunningView = runningView)
    );
    let newQueuedView: TransitionViewType = null;
    reaction(
      () => routerStore.queuedView,
      (queuedView) => (newQueuedView = queuedView)
    );

    routerStore.setView(viewStates[1].name);
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    // Immediately set another view
    routerStore.setView(viewStates[0].name, parameters[0]);
    // The setView is still processing the first requested view
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todos.name);
    expect((newQueuedView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    // The first setView has finished onEnter hook execution
    expect(routerStore.currentView?.name).toEqual(views.home.name);

    await Promise.resolve(process.nextTick);
    // Final setView is currently processed
    expect((newRunningView as TransitionViewType)?.view.name).toEqual(views.todo.name);
    expect(newQueuedView).toBeNull();
    // The current view is still "home"
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(applySnapshotSpy).toHaveBeenCalledTimes(1);

    // The value of ms to wait must be equal to the value of ms in the loop generator setTimeout
    await new Promise((resolve) => setTimeout(() => resolve(1), 100));
    expect(routerStore.currentView?.name).toEqual(views.todo.name);
  });

  it("Should not block routing, if views don't have blocking hooks", async () => {
    const {
      testData: { paths, viewStates },
      views,
      routerStore,
    } = setup();

    let newView: string | undefined = undefined;
    reaction(
      () => routerStore.currentView?.name,
      (reactionViewName) => (newView = reactionViewName)
    );

    expect(routerStore.currentUrl).toEqual(paths.homePath);
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(routerStore.runningView).toBeNull();

    routerStore.setView(viewStates[1].name);
    expect(newView).toEqual(viewStates[1].name);

    routerStore.setView(viewStates[2].name);
    expect(newView).toEqual(viewStates[2].name);
  });

  it("Should abort routing if view doesn't exist", async () => {
    const beforeEnterSpy = jest.fn(() => true);
    const {
      testData: { viewStates, paths },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todos",
            path: "/todos",
            hooks: {
              beforeEnter: beforeEnterSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    let newRunningView: TransitionViewType[] = [];
    reaction(
      () => routerStore.runningView,
      (runningView) => newRunningView.push(runningView)
    );
    // Check that existing view can be routed to
    const returnCode = await routerStore.setView(viewStates[0].name);

    expect(newRunningView).toEqual(
      expect.arrayContaining([
        {
          key: paths.todosPath,
          params: undefined,
          view: views.todos,
        },
      ])
    );
    newRunningView = [];
    expect(returnCode).toEqual(0);

    const returnCodeError = await routerStore.setView("noView" as RoutingPathNamesType);
    expect(newRunningView).toEqual([]);
    expect(returnCodeError).toEqual(1);
  });

  it("Should abort routing, if a requested view is the same as a pending view", async () => {
    const beforeEnterSpy = jest.fn(() => true);
    const {
      testData: { viewStates },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todos",
            path: "/todos",
            hooks: {
              beforeEnter: beforeEnterSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(routerStore.currentView).toEqual(views.home);

    routerStore.setView(viewStates[0].name);
    await Promise.resolve(process.nextTick);
    expect(routerStore.runningView?.view.name).toEqual(views.todos.name);
    const returnCodeError = await routerStore.setView(viewStates[0].name);
    expect(returnCodeError).toEqual(0);
  });

  it("Should NOT return any view on initial render, if the view hook prevents routing to it. Further navigation should be possible", async () => {
    const beforeEnterSpy = jest.fn(() => false);
    const applySnapshotSpy = jest.spyOn(mst, "applySnapshot");

    const {
      testData: { paths, viewStates },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todos",
            path: "/todos",
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            hooks: {
              // Initial view has onEnter hook, that halts synchronous routing
              beforeEnter: beforeEnterSpy,
            },
            isPrivatePath: false,
          },
        ],
      },
    });

    expect(beforeEnterSpy).toHaveBeenCalledTimes(1);
    // The store state MUST NOT be reverted on store creation
    expect(applySnapshotSpy).toHaveBeenCalledTimes(0);

    expect(routerStore.currentView).toBeNull();
    expect(routerStore.browserHistory?.location.pathname).toEqual(paths.homePath);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);
    await Promise.resolve(process.nextTick);

    await routerStore.setView(viewStates[0].name);
    expect(routerStore.currentView?.name).toEqual(views.todos.name);
    expect(routerStore.browserHistory?.location.pathname).toEqual(paths.todosPath);
  });

  it("Should return browser url to previous url, if routing through browser navigation was blocked by the previous route", async () => {
    const beforeExitSpy = jest.fn(() => false);
    const {
      testData: { paths, viewStates },
      views,
      routerStore,
    } = setup({
      testData: {
        viewStates: [
          {
            name: "todo",
            path: "/todos/:todoId",
            isPrivatePath: false,
          },
          {
            name: "todos",
            path: "/todos",
            hooks: {
              beforeExit: beforeExitSpy,
            },
            isPrivatePath: false,
          },
          {
            name: "home",
            path: "/",
            isPrivatePath: false,
          },
        ],
      },
    });

    let newUrl: string | undefined = undefined;
    reaction(
      () => routerStore.currentUrl,
      (reactionUrl) => (newUrl = reactionUrl)
    );

    expect(routerStore.currentUrl).toEqual(paths.homePath);
    expect(routerStore.currentView?.name).toEqual(views.home.name);
    expect(routerStore.runningView).toBeNull();

    await routerStore.setView(viewStates[1].name);
    expect(newUrl).toEqual(paths.todosPath);
    expect(routerStore.browserHistory?.location.pathname).toEqual(paths.todosPath);
    expect(routerStore.currentView).toEqual(views.todos);
    routerStore.goBack();
    expect(newUrl).toEqual("");
    expect(routerStore.browserHistory?.location.pathname).toEqual(paths.homePath);

    // I am using setTimeout here to let the process in setView, invoked
    // by browser URL change, to finish before this test checks.

    await new Promise((resolve) => setTimeout(() => resolve(1), 0));
    expect(newUrl).toEqual(paths.todosPath);
    expect(routerStore.browserHistory?.location.pathname).toEqual(paths.todosPath);
  });

  it("Should block routing, if a view is private and authorization token is missing, and request authorization process", async () => {
    const loginSpy = jest.fn();
    jest.spyOn(mst, "getEnv").mockReturnValue({
      authStore: {
        hasInitialAuthorization: false,
        login: loginSpy,
      },
    });
    setup({
      testData: {
        viewStates: [
          {
            name: "home",
            path: "/",
            isPrivatePath: true,
          },
        ],
      },
    });

    expect(loginSpy).toHaveBeenCalledTimes(1);
  });

  it("Should NOT block routing, if a view is private, but authorization token is present", async () => {
    jest.spyOn(mst, "getEnv").mockReturnValue({
      authStore: {
        hasInitialAuthorization: true,
      },
    });
    const {
      views,
      routerStore,
      testData: { paths },
    } = setup({
      testData: {
        viewStates: [
          {
            name: "home",
            path: "/",
            isPrivatePath: true,
          },
        ],
      },
    });

    expect(routerStore.currentUrl).toEqual(paths.homePath);
    expect(routerStore.currentView?.name).toEqual(views.home.name);
  });
});
