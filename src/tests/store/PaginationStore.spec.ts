import { PaginationStore } from "@test/store/models";

describe("PaginationStore", () => {
  it("Should create store with default data", () => {
    const store = PaginationStore.create();

    expect(store.page).toEqual(1);
    expect(store.totalPages).toEqual(0);
    expect(store.maxPerPage).toEqual(10);
    expect(store.perPageAmounts).toEqual([100, 50, 20, 10]);
  });
  it("Should change default data of the store", () => {
    const store = PaginationStore.create();

    store.changePage(5);
    expect(store.page).toEqual(5);

    store.setTotalPages(30);
    expect(store.totalPages).toEqual(30);

    store.changeMaxPerPage(20);
    expect(store.maxPerPage).toEqual(20);
  });
});
