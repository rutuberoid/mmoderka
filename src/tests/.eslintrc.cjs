/*
 * This file will inherit root config
 * see: https://eslint.org/docs/latest/user-guide/configuring/configuration-files#cascading-and-hierarchy
 */
module.exports = {
  /*
   * Check all eslint rules here:
   * https://eslint.org/docs/latest/rules/<name of the rule>
   * Check all typescript-eslint rules here:
   * https://typescript-eslint.io/rules/<name of the rule>
   */
  rules: {
    "no-restricted-imports": [
      "error",
      {
        patterns: [
          {
            group: ["**/constants/*", "**/constants"],
            message: 'Usage of "constants" modules is not allowed inside of "tests" directory.',
          },
          {
            group: ["**/stories/*", "**/stories"],
            message: 'Usage of "stories" modules is not allowed inside of "tests" directory.',
          },
          {
            group: ["**/app/*", "**/app"],
            message: 'Usage of "app" modules is not allowed inside of "tests" directory.',
          },
        ],
      },
    ],
    "react/jsx-props-no-spreading": "off",
  },
};
