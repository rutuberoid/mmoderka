import { Button } from "@mimir/components";

import { render, screen } from "../testUtils";

describe("Showcase testing component", () => {
  it("Should render", () => {
    render(<Button onClick={jest.fn}>hello</Button>);
  }),
    it("Should display button label", () => {
      render(<Button onClick={jest.fn}>hello</Button>);
      expect(screen.getByText("hello")).toBeInTheDocument();
    });
});
