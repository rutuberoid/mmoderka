import type { VideoPreviewProps } from "@mimir/components";
import { VideoPreview } from "@mimir/components";
import userEvent from "@testing-library/user-event";

import { render, screen } from "../testUtils";

const getProps = (newProps?: Partial<VideoPreviewProps>): VideoPreviewProps => ({
  copyVideoId: jest.fn(),
  openVideoModal: jest.fn(),
  videoId: "3e2495c423c2c60753274d664df0f80e",
  videoTitle: "Холостяк, 9 сезон, 11 выпуск",
  videoUrl: "https://rutube.dev/video/3e2495c423c2c60753274d664df0f80e/",
  videoPreviewUrl: "https://pic.rutube.dev/promoitem/ba/13/ba13306ef8c8557ccbb1f66ee9dab177.jpg",
  ...newProps,
});

describe("VideoPreview", () => {
  it("Should render correctly", () => {
    const props = getProps();

    render(<VideoPreview {...props} />);

    expect(screen.getByText(props.videoTitle)).toBeInTheDocument();
    expect(screen.getByText(props.videoId)).toBeInTheDocument();
  });

  it("Should invoke open video modal handler on click", () => {
    const props = getProps();

    const { container } = render(<VideoPreview {...props} />);

    const videoPreviewButton = container.getElementsByClassName("hoverVideoPreview")[0];
    userEvent.click(videoPreviewButton);

    expect(props.openVideoModal).toBeCalled();
  });

  it("Should invoke copy video id handler on click", () => {
    const props = getProps();

    const { container } = render(<VideoPreview {...props} />);

    const copyVideoIdButton = container.getElementsByClassName("hoverCopyButton")[0];
    userEvent.click(copyVideoIdButton);

    expect(props.copyVideoId).toBeCalled();
  });
});
