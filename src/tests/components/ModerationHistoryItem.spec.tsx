import type { ModerationHistoryItemProps } from "@mimir/components";
import { ModerationHistoryItem } from "@mimir/components";
import userEvent from "@testing-library/user-event";
import type { EventUserType, EventType } from "@mimir/types";

import { render, screen } from "../testUtils";

const getProps = (newProps?: Partial<ModerationHistoryItemProps>): ModerationHistoryItemProps => ({
  eventType: "edit",
  eventUserType: "author",
  eventUserId: 23126416,
  eventUserName: "Валерий Иванов",
  eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
  ...newProps,
});

const TEST_CASES: Array<{
  params: { eventUserType?: EventUserType; eventType?: EventType };
  expected: {
    title?: (eventUserName: string) => string;
    subtitle?: (eventUserId: number) => string;
    icon?: string;
    eventDetailsLabel?: string;
  };
}> = [
  {
    params: {
      eventUserType: "automoderator",
      eventType: "delete",
    },
    expected: {
      title: () => "Комментарий удалён Автомодерацией",
      icon: "DeleteOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "radioControl",
      eventType: "delete",
    },
    expected: {
      title: () => "Комментарий удалён по требованию ГРЧЦ",
      icon: "DeleteOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "creator",
      eventType: "delete",
    },
    expected: {
      title: (eventUserName) => `Комментарий удалил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID креатора: ${eventUserId}`,
      icon: "DeleteOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "creator",
      eventType: "ban",
    },
    expected: {
      title: (eventUserName) => `Комментарий забанил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID креатора: ${eventUserId}`,
      eventDetailsLabel: "Посмотреть причину",
      icon: "BlockOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "creator",
      eventType: "unban",
    },
    expected: {
      title: (eventUserName) => `Комментарий разбанил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID креатора: ${eventUserId}`,
      icon: "DoDisturbOffOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "creator",
      eventType: "restore",
    },
    expected: {
      title: (eventUserName) => `Комментарий восстановил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID креатора: ${eventUserId}`,
      icon: "RestartAltOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "author",
      eventType: "delete",
    },
    expected: {
      title: (eventUserName) => `Комментарий удалил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID автора: ${eventUserId}`,
      icon: "DeleteOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "author",
      eventType: "add",
    },
    expected: {
      title: (eventUserName) => `Комментарий опубликован автором ${eventUserName}`,
      subtitle: (eventUserId) => `ID автора: ${eventUserId}`,
      icon: "AddCommentOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "author",
      eventType: "edit",
    },
    expected: {
      title: (eventUserName) => `Комментарий изменён автором ${eventUserName}`,
      subtitle: (eventUserId) => `ID автора: ${eventUserId}`,
      eventDetailsLabel: "Посмотреть изменения",
      icon: "ModeEditOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "user",
      eventType: "report",
    },
    expected: {
      title: (eventUserName) => `Жалоба от пользователя ${eventUserName}`,
      subtitle: (eventUserId) => `ID пользователя: ${eventUserId}`,
      eventDetailsLabel: "Посмотреть жалобу",
      icon: "ReportOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "assign",
    },
    expected: {
      title: (eventUserName) => `Комментарий назначен на ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      icon: "RedoOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "approve",
    },
    expected: {
      title: (eventUserName) => `Комментарий утвердил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      icon: "DoneOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "delete",
    },
    expected: {
      title: (eventUserName) => `Комментарий удалил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      icon: "DeleteOutlineOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "ban",
    },
    expected: {
      title: (eventUserName) => `Комментарий забанил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      eventDetailsLabel: "Посмотреть причину",
      icon: "BlockOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "unban",
    },
    expected: {
      title: (eventUserName) => `Комментарий разбанил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      icon: "DoDisturbOffOutlinedIcon",
    },
  },
  {
    params: {
      eventUserType: "moderator",
      eventType: "restore",
    },
    expected: {
      title: (eventUserName) => `Комментарий восстановил(а) ${eventUserName}`,
      subtitle: (eventUserId) => `ID модератора: ${eventUserId}`,
      icon: "RestartAltOutlinedIcon",
    },
  },
];

describe("ModerationHistoryItem", () => {
  it("Should render correctly", () => {
    const props = getProps();

    render(<ModerationHistoryItem {...props} />);

    expect(screen.getByText("Посмотреть изменения")).toBeInTheDocument();
    expect(screen.getByText(`ID автора: ${props.eventUserId}`)).toBeInTheDocument();
    expect(screen.getByText(`Комментарий изменён автором ${props.eventUserName}`)).toBeInTheDocument();
  });

  describe("Should display different titles and icons based on eventType and eventUserType props", () => {
    TEST_CASES.forEach(({ params, expected: { title, subtitle, eventDetailsLabel, icon } }) => {
      const eventUserTypeLabel = `${params.eventUserType ? ` eventUserType is ${params.eventUserType}` : ""}`;
      const eventTypeLabel = `${params.eventType ? ` and eventType is ${params.eventType}` : ""}`;
      const testName = `When${eventUserTypeLabel}${eventTypeLabel}`;

      it(testName, () => {
        const props = getProps(params);

        render(<ModerationHistoryItem {...props} />);

        if (title) {
          expect(screen.getByText(title(props.eventUserName))).toBeInTheDocument();
        }
        if (subtitle) {
          expect(screen.getByText(subtitle(props.eventUserId))).toBeInTheDocument();
        }
        if (eventDetailsLabel) {
          expect(screen.getByText(eventDetailsLabel)).toBeInTheDocument();
        }
        if (icon) {
          expect(screen.getByTestId(icon)).toBeInTheDocument();
        }
      });
    });
  });

  it("Should not render accordion when event details are empty", () => {
    const props = getProps({ eventDetails: [] });

    const { queryByRole } = render(<ModerationHistoryItem {...props} />);

    expect(queryByRole("button")).not.toBeInTheDocument();
  });

  it("Should render event details by clicking accordion button", () => {
    const props = getProps();

    const { getByText, getByRole } = render(<ModerationHistoryItem {...props} />);

    const accordion = getByRole("button");
    userEvent.click(accordion);

    expect(getByText(props.eventDetails[0])).toBeInTheDocument();
  });
});
