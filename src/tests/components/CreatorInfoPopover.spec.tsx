import type { CreatorInfoPopoverProps } from "@mimir/components";
import { CreatorInfoPopover } from "@mimir/components";
import userEvent from "@testing-library/user-event";
import { waitFor } from "@testing-library/react";

import { render, screen } from "../testUtils";

const getProps = (newProps?: Partial<CreatorInfoPopoverProps>): CreatorInfoPopoverProps => ({
  banByCreatorId: jest.fn(),
  copyCreatorId: jest.fn(),
  filterByCreatorId: jest.fn(),
  creatorId: "1522312",
  creatorAvatarUrl: "https://pic.rutube.ru/user/87/3c/873cb1a321bc427821d92f53dd93795a.jpg",
  creatorChannelUrl: "https://rutube.ru/channel/23178409/",
  creatorName: "Bob DragonBorn",
  isBanned: true,
  canFilterById: true,
  ...newProps,
});

const getTexts = () => ({
  ban: /Забанить/i,
  filter: /Отфильтровать/i,
  watch: /Посмотреть на Rutube/i,
  banned: /Забанен/i,
});

describe("Showcase testing component", () => {
  it("Should render", () => {
    const props = getProps();
    render(<CreatorInfoPopover {...props} />);
  });
  it("Should display creator's avatar and name on button", () => {
    const props = getProps();
    render(<CreatorInfoPopover {...props} />);
    // check creator name
    expect(screen.getByText(props.creatorName)).toBeInTheDocument();
    expect(screen.getByText(props.creatorName)).toHaveClass("hoverCreator");
    // check avatar
    expect(screen.getByAltText(props.creatorName)).toBeInTheDocument();
    expect(screen.getByAltText(props.creatorName)).toHaveAttribute("src");
  });
  it("Should display avatar fallback when creatorAvatar is invalid", () => {
    const props = getProps({
      creatorAvatarUrl: "",
    });
    render(<CreatorInfoPopover {...props} />);
    // check avatar
    expect(screen.getByTestId("PersonIcon")).toBeInTheDocument();
  });
  it("Should display menu on click", () => {
    const props = getProps();
    const { banned } = getTexts();
    render(<CreatorInfoPopover {...props} />);
    // open menu
    userEvent.click(screen.getByText(props.creatorName));
    // check elements
    expect(screen.getByText(props.creatorId)).toBeInTheDocument();
    expect(screen.getAllByText(props.creatorName).length).toEqual(2);
    expect(screen.getByText(banned)).toBeInTheDocument();
  });
  it("Should contain link with specified params", () => {
    const props = getProps();
    const { watch } = getTexts();
    render(<CreatorInfoPopover {...props} />);
    // open menu
    userEvent.click(screen.getByText(props.creatorName));
    // getting label
    const linkEl = screen.getByText(watch);
    expect(linkEl).toHaveAttribute("href", props.creatorChannelUrl);
    expect(linkEl).toHaveAttribute("target", "_blank");
    expect(linkEl).toHaveAttribute("rel", "noopener noreferrer");
  });

  it("Should pass 'creatorId' into functions arguments and close menu on filter or ban buttons", async () => {
    const filterByCreatorIdSpy = jest.fn();
    const banByCreatorIdSpy = jest.fn();
    const copyCreatorIdSpy = jest.fn();
    const props = getProps({
      filterByCreatorId: filterByCreatorIdSpy,
      banByCreatorId: banByCreatorIdSpy,
      copyCreatorId: copyCreatorIdSpy,
    });
    const { filter, ban } = getTexts();
    render(<CreatorInfoPopover {...props} />);

    // open menu
    userEvent.click(screen.getByText(props.creatorName));

    // copy click
    userEvent.click(screen.getByTestId("ContentCopyIcon"));
    expect(copyCreatorIdSpy).toBeCalledTimes(1);
    expect(copyCreatorIdSpy).toBeCalledWith(props.creatorId);

    // filter click
    userEvent.click(screen.getByText(filter));
    expect(filterByCreatorIdSpy).toBeCalledTimes(1);
    expect(filterByCreatorIdSpy).toBeCalledWith(props.creatorId);

    // menu closes by click on filter or ban buttons
    await waitFor(() => {
      expect(screen.queryByText(filter)).not.toBeInTheDocument();
    });

    // open menu again
    userEvent.click(screen.getByText(props.creatorName));

    // ban click
    userEvent.click(screen.getByText(ban));
    expect(banByCreatorIdSpy).toBeCalledTimes(1);
    expect(banByCreatorIdSpy).toBeCalledWith(props.creatorId);

    await waitFor(() => {
      expect(screen.queryByText(ban)).not.toBeInTheDocument();
    });
  });

  it("Should not display 'Banned' label if user is not banned", () => {
    const props = getProps({
      isBanned: false,
    });
    const { banned } = getTexts();
    render(<CreatorInfoPopover {...props} />);
    // open menu
    userEvent.click(screen.getByText(props.creatorName));
    // searching label
    expect(screen.queryByText(banned)).not.toBeInTheDocument();
  });
});
