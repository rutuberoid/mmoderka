import type { Instance } from "mobx-state-tree";
import { types } from "mobx-state-tree";

import type { RouterStoreModel, AuthStoreModel } from "./index";
import { RouterStore, AuthStore } from "./index";

export type RootStoreModel = Instance<typeof RootStore>;

export type RootStoreEnv = {
  routerStore: RouterStoreModel;
  authStore: AuthStoreModel;
};

export const RootStore = types.model("RootStore", {
  routerStore: RouterStore,
  authStore: AuthStore,
});
