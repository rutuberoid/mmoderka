import type { Instance, IAnyStateTreeNode } from "mobx-state-tree";
import type { URLParametersType } from "@mimir/types";
import { types, flow, getEnv } from "mobx-state-tree";
import { ROUTING_PATH_NAMES } from "@mimir/constants";

export type ViewStoreModel = Instance<typeof ViewStore>;
export type ViewHooksType = Partial<{
  beforeEnter: (selfState: IAnyStateTreeNode, parameters?: URLParametersType) => Promise<boolean> | boolean;
  beforeExit: (selfState: IAnyStateTreeNode, parameters?: URLParametersType) => Promise<boolean> | boolean;
  onExit: (selfState: IAnyStateTreeNode, parameters?: URLParametersType) => Promise<void> | void;
  onEnter: (selfState: IAnyStateTreeNode, parameters?: URLParametersType) => Promise<void> | void;
}>;

export const ViewStore = types
  .model("ViewStore", {
    name: types.refinement(types.identifier, (identifier) => ROUTING_PATH_NAMES.includes(identifier as any)),
    path: types.string,
    // TODO: change to true when auth store is ready
    isPrivatePath: types.optional(types.boolean, true),
    routingProps: types.optional(types.frozen<Record<string, any>>(), {}),
    hooks: types.optional(types.frozen<ViewHooksType>(), {}),
  })
  .views((self) => ({
    get router() {
      return getEnv(self).routerStore;
    },
  }))
  .actions((self) => ({
    setRoutingProps(routingProps: Record<string, any>) {
      self.routingProps = routingProps;
    },
    // Parameters order matters
    expandParameters: (parameters?: URLParametersType) => {
      if (!parameters) {
        return self.path;
      }

      let url = self.path;
      Object.entries(parameters).forEach(([key, value]) => {
        url = url.replace(`:${key}`, value);
      });
      return url;
    },
    beforeEnter: flow(function* beforeEnter(parameters?: URLParametersType) {
      let canContinueEnter = true;
      if (self.hooks.beforeEnter) {
        canContinueEnter = yield Promise.resolve(self.hooks.beforeEnter(self, parameters));
      }
      return canContinueEnter;
    }),
    onEnter: flow(function* onEnter(parameters?: URLParametersType) {
      if (self.hooks.onEnter) {
        yield Promise.resolve(self.hooks.onEnter(self, parameters));
      }
    }),
    beforeExit: flow(function* beforeExit(parameters?: URLParametersType) {
      let canContinueExit = true;
      if (self.hooks.beforeExit) {
        canContinueExit = yield Promise.resolve(self.hooks.beforeExit(self, parameters));
      }
      return canContinueExit;
    }),
    onExit: flow(function* onExit(parameters?: URLParametersType) {
      if (self.hooks.onExit) {
        yield Promise.resolve(self.hooks.onExit(self, parameters));
      }
    }),
  }));
