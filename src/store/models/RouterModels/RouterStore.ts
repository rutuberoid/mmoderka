import type { Instance } from "mobx-state-tree";
import type { BrowserHistory } from "history";
import { createBrowserHistory } from "history";
import { types, flow, getRoot, getEnv, getSnapshot, applySnapshot } from "mobx-state-tree";
import type { URLParametersType, RoutingPathNamesType } from "@mimir/types";
import { keys, reaction } from "mobx";
import { matchPathToHandler } from "@mimir/utils";

import type { ViewStoreModel } from "./index";
import { ViewStore } from "./index";

export type RouterStoreModel = Instance<typeof RouterStore>;
export type TransitionViewType = {
  key: string;
  view: ViewStoreModel;
  params: URLParametersType | undefined;
} | null;

export const RouterStore = types
  .model("RouterStore", {
    views: types.map(ViewStore),
    currentView: types.maybeNull(types.reference(ViewStore)),
    parameters: types.frozen(),
    isLoading: types.optional(types.boolean, false),
    isStoreInitialized: types.optional(types.boolean, false),
  })
  .volatile(
    (): {
      browserHistory: BrowserHistory | null;
      runningView: TransitionViewType;
      queuedView: TransitionViewType;
    } => ({
      browserHistory: null,
      runningView: null,
      queuedView: null,
    })
  )
  .views((self) => ({
    get root() {
      return getRoot(self);
    },
    get currentUrl() {
      return !self.runningView && self.currentView ? self.currentView.expandParameters(self.parameters) : "";
    },
  }))
  .actions((self) => ({
    getViewByName(viewName: string) {
      return self.views.get(viewName);
    },
    setHistory(history: BrowserHistory | null) {
      self.browserHistory = history;
    },
    goBack() {
      if (self.browserHistory) {
        self.browserHistory.back();
      }
    },
    goForward() {
      if (self.browserHistory) {
        self.browserHistory.forward();
      }
    },
    setLoading(isLoading: boolean) {
      self.isLoading = isLoading;
    },
    setStoreInitialized(isStoreInitialized: boolean) {
      self.isStoreInitialized = isStoreInitialized;
    },
  }))
  .actions((self) => ({
    // This function can operate on 3 views simultaneously:
    // - currently chosen by a user or this view
    // - currently processed or running view
    // - queued view
    // @return returnCode number
    //  - 0: successful routing or routing is not needed
    //  - 1: view doesn't exist
    //  - 2: routing was aborted
    //  - 3: routing was aborted, because a new route was requested
    setView: flow(function* setView(viewName: RoutingPathNamesType, params?: URLParametersType) {
      const view = self.views.get(viewName);
      if (!view) {
        return 1;
      }
      const authStore = getEnv(self).authStore;

      if (view.isPrivatePath && authStore && !authStore.hasInitialAuthorization) {
        authStore.login();
      }

      const thisView = {
        key: view.expandParameters(params),
        view: view,
        params: params,
      };

      if (self.runningView) {
        // If setView is already running or queued on this route, ignore
        if (
          !self.isStoreInitialized ||
          self.runningView.key === thisView.key ||
          self.queuedView?.key === thisView.key
        ) {
          return 0;
        }

        self.queuedView = thisView;

        // Spin this thread until thisView is no longer queued
        while (self.queuedView !== null) {
          yield new Promise((resolve) => setTimeout(resolve, 100));
        }

        // Check that this is still the setView to process
        if (self.runningView?.key !== thisView.key) {
          return 0;
        }
      }

      // The routing may take some time, and a user is not blocked from
      // navigating elsewhere. So we cache the being processed view (before all operations with
      // the previous and the next views), to compare it with any possible future views,
      // requested by an impatient user.
      self.runningView = thisView;

      // Block out page for loading
      self.setLoading(true);

      // Save a snapshot to rollback to if something goes wrong
      const rootSnapshot = getSnapshot(self.root);

      const rollback = () => {
        if (self.isStoreInitialized) {
          applySnapshot(self.root, rootSnapshot);
        }

        if (self.queuedView) {
          self.runningView = self.queuedView;
          self.queuedView = null;
        } else {
          // There is no awaiting views to process and the routing for the last
          // view was aborted. Release the initial view and display it to the user.
          self.runningView = null;
          self.isLoading = false;
        }
      };

      const oldView = self.currentView;
      const oldParams = self.parameters;

      // Check if routing is allowed from this view
      if (oldView?.hooks.beforeExit) {
        const canContinueExit = yield oldView.beforeExit(oldParams);
        // Rollback to the previously active view
        if (canContinueExit === false) {
          rollback();
          return 2;
        }
      }

      // Check if route has been changed and substitute runningView with queuedView
      if (self.queuedView) {
        rollback();
        return 3;
      }

      if (oldView?.hooks.onExit) {
        yield oldView.onExit(oldParams);
      }

      // Check if route has been changed and substitute runningView with queuedView
      if (self.queuedView) {
        rollback();
        return 3;
      }

      // Check if this route is accessible
      if (view.hooks.beforeEnter) {
        const canContinueEnter = yield view.beforeEnter(params);
        // Rollback to the previously active view
        if (canContinueEnter === false) {
          rollback();
          return 2;
        }
      }

      // Check if route has been changed and substitute runningView with queuedView
      if (self.queuedView) {
        rollback();
        return 3;
      }

      // On enter new view
      if (view.hooks.onEnter) {
        yield view.onEnter(params);
      }

      // Check if route has been changed and substitute runningView with queuedView
      if (self.queuedView) {
        rollback();
        return 3;
      }

      // Update current url
      self.currentView = view;
      self.parameters = params || {};

      // Free up page to render
      self.setLoading(false);

      self.runningView = null;

      if (!self.isStoreInitialized) {
        self.setStoreInitialized(true);
      }
      return 0;
    }),
  }))
  .views((self) => ({
    get pathHandlers() {
      const pathHandlersList: Record<string, (params?: URLParametersType) => Promise<number>> = {};
      const keyList = keys(self.views);
      keyList.forEach((key) => {
        const view = self.views.get(key as RoutingPathNamesType);
        if (view) {
          pathHandlersList[view.path] = (params?: URLParametersType) =>
            self.setView(view.name as RoutingPathNamesType, params);
        }
      });
      return pathHandlersList;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      const history = createBrowserHistory();
      self.setHistory(history);

      const pathMatcher = matchPathToHandler(self.pathHandlers);

      // Call path handler when url has been changed by back button
      history.listen(({ location, action }) => {
        switch (action) {
          case "POP":
            pathMatcher(location.pathname);
            break;
          default:
            break;
        }
      });

      // Update browser url based on this state url
      reaction(
        () => self.currentUrl,
        (url) => {
          if (history.location.pathname !== url) {
            history.push(url);
          }
        }
      );

      // Initial view is set automatically by synchronizing with history
      pathMatcher(history.location.pathname);
    },
  }));
