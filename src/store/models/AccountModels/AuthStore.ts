import type { Instance } from "mobx-state-tree";
import { types, flow, getEnv } from "mobx-state-tree";
import qs from "qs";
import { getMainDomain, identifyHttpError } from "@mimir/utils";
import { getAuthorizeUserInfo, postLogoutUser } from "@mimir/services";
import type { ResponseAuthorizeUserInfo } from "@mimir/types";

import type { RootStoreEnv } from "../internal";

const getLoginParams = () =>
  qs.stringify({
    "referer": window.location.href,
    "new-next": window.location.href,
    "snake-yaCounter": 66791995,
    "_ym_debug": "1",
  });

export type AuthStoreModel = Instance<typeof AuthStore>;
export const AuthStore = types
  .model("AuthStore", {
    moderatorId: types.optional(types.string, ""),
    moderatorName: types.optional(types.string, ""),
    moderatorAvatarUrl: types.optional(types.string, ""),
    hasInitialAuthorization: types.optional(types.boolean, false),
    isLoading: types.optional(types.boolean, false),
  })
  .actions((self) => ({
    login: flow(function* login() {
      if (self.hasInitialAuthorization) {
        return;
      }
      try {
        self.isLoading = true;
        const authData: ResponseAuthorizeUserInfo = yield getAuthorizeUserInfo();

        self.moderatorId = authData.moderator_id;
        self.moderatorName = authData.moderator_name;
        self.moderatorAvatarUrl = authData.moderator_avatar_url;
      } catch (error) {
        const identifiedError = identifyHttpError(error);
        if (identifiedError.shouldLogout) {
          self.isLoading = true;
          const routerStore = getEnv<RootStoreEnv>(self).routerStore;
          const loginParams = getLoginParams();
          routerStore.browserHistory?.replace(
            `${getMainDomain()}/multipass/login/?new_login=true&login-params=${loginParams}`
          );
        } else {
          throw new Error(identifiedError.clientMessage);
        }
      } finally {
        self.isLoading = true;
        self.hasInitialAuthorization = true;
      }
    }),
    logout: flow(function* logout() {
      try {
        self.isLoading = true;
        yield postLogoutUser({});

        const routerStore = getEnv<RootStoreEnv>(self).routerStore;
        const loginParams = getLoginParams();
        routerStore.browserHistory?.replace(
          `${getMainDomain()}/multipass/login/?new_login=true&login-params=${loginParams}`
        );
      } catch (error) {
        const identifiedError = identifyHttpError(error);
        self.isLoading = false;
        throw new Error(identifiedError.clientMessage);
      }
    }),
  }));
