import { types, cast, flow, getSnapshot, applySnapshot } from "mobx-state-tree";
import type { Instance, IAnyModelType } from "mobx-state-tree";
import type {
  ParamsUpdateFieldDefinitionsType,
  FormatValuesType,
  FormValueSortOptionsType,
  FormattedValuesObjectType,
  UpdateValuesType,
  InputValidatorType,
  ValueFormatterType,
  BlurFuncType,
  FocusFuncType,
  SortFuncType,
} from "@mimir/types";

import { UniqueIdStore, MatrixStore } from "../BaseModels";

export type FormFieldStoreModel = Instance<typeof FormFieldStore>;
export type FormValueStoreModel = Instance<typeof FormValueStore>;
export type FormValueStorePropertiesType = typeof FormValueStore.properties;
type FormFieldStoreVolatileType = {
  updateFunc: null | UpdateValuesType;
  requestParams: Record<string, unknown>;
  dummySelection: null | FormattedValuesObjectType;
  defaultSelections: null | FormatValuesType;
  inputValidatorFunc: null | InputValidatorType;
  valueFormatter: null | ValueFormatterType;
  blurFunc: null | BlurFuncType;
  focusFunc: null | FocusFuncType;
  sortFunc: null | SortFuncType;
  tempValues: null | FormattedValuesObjectType[];
  // it is very hard to define typings for it
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  initialDefinition: any;
  nullReferenceError: null | string;
};

export const FormValueStore = types
  .model("FormValueStore", {
    id: types.identifier,
    name: types.string,
    value: types.string,
    // analog to cloud of tags for a field
    meta: types.maybe(MatrixStore),
    isDisabled: types.boolean,
    isDummy: types.optional(types.boolean, false),
  })
  .actions(() => ({
    checkHealth() {
      return true;
    },
  }));

export const FormFieldStore = types
  .compose(
    "FormFieldStore",
    UniqueIdStore,
    types.model({
      id: types.identifier,
      fieldAnchor: types.string,
      label: types.optional(types.string, ""),
      shouldDisplayExternalLabel: types.optional(types.boolean, false),
      isDisabled: types.optional(types.boolean, false),
      isRequired: types.optional(types.boolean, false),
      hasCompletion: types.optional(types.boolean, false),
      hasMultipleChoice: types.optional(types.boolean, false),
      requiredSign: types.optional(types.string, "*"),
      placeholder: types.maybe(types.string),
      // if field is initialized and not mutated by a user
      // it is pristine
      isPristine: types.optional(types.boolean, true),
      error: types.maybe(types.string),
      fieldIndex: types.maybe(types.string),
      values: types.optional(types.array(FormValueStore), []),
      singleValue: types.optional(types.string, ""),
      displayedSingleValue: types.optional(types.string, ""),
      selected: types.maybe(types.reference(types.late(() => FormValueStore))),
      multipleSelected: types.array(types.reference(types.late(() => FormValueStore))),
      // never use 2 attributes bellow simultaneously per field
      // if the field has a dependant field assign it on a initialization
      next: types.maybe(types.reference(types.late((): IAnyModelType => FormFieldStore))),
      // if the field has a list of dependant fields (tree nodes) assign it on initialization
      children: types.array(types.reference(types.late((): IAnyModelType => FormFieldStore))),
    })
  )
  .volatile(
    (): FormFieldStoreVolatileType => ({
      updateFunc: null,
      // TODO check if this property can be moved to model with frozen type
      requestParams: {},
      dummySelection: null,
      defaultSelections: null,
      inputValidatorFunc: null,
      valueFormatter: null,
      blurFunc: null,
      focusFunc: null,
      sortFunc: null,
      tempValues: null,
      initialDefinition: null,
      nullReferenceError: null,
    })
  )
  .views((self) => ({
    get displayedValue() {
      return self.valueFormatter ? self.valueFormatter(self.singleValue) : self.singleValue;
    },
    get multipleSelectedIds() {
      return self.multipleSelected.map((value) => value.id);
    },
    get multipleSelectedNames() {
      return self.multipleSelected.map((value) => value.name);
    },
  }))
  .actions((self) => ({
    setValue(selected: string | string[] | undefined, options?: { isInternalSet?: boolean }) {
      if (self.error) {
        self.error = "";
      }
      // [NEW BRANCH] - logic for input vs select
      // selection of multiple values is possible
      // only when array of values has items
      // OR this condition is met, when setValue
      // is called from the inside of the model
      if (self.values.length > 0 || options?.isInternalSet) {
        let isDummyChoice = false;

        if (self.dummySelection && selected) {
          isDummyChoice = self.dummySelection.id === selected;
        }

        // [NEW BRANCH] - logic for select / autocomplete (single string) vs multi-select
        // reference must be reset before
        // values are updated
        // OR select supports an empty selection
        if (!selected || isDummyChoice) {
          self.selected = undefined;
        } else if (typeof selected === "string") {
          let selectedValue = undefined;
          // when value is set manually and it is not dummy (check above)
          // the selected value is without a unique identifier
          // so this selected value is used to get a unique id from the
          // uniqueIds dictionary
          if (self.isPristine) {
            selectedValue = self.uniqueIds.get(selected)?.uniqueId;
            // preserve initial selected value (not a dummy choice)
            self.initialDefinition = { ...self.initialDefinition, selected: selectedValue };
          } else {
            // general case of selecting one value
            selectedValue = selected;
          }
          try {
            // @ts-expect-error MST resolves references by string id. Here TS sees that a string is assigned
            // to an object, but it is a correct behavior
            self.selected = selectedValue;
            // check if referenced value exists
            self.selected?.checkHealth();
          } catch (error) {
            // if it is acceptable to have null reference
            // notify user about it
            if (self.nullReferenceError) {
              self.error = self.nullReferenceError;
              self.selected = undefined;
            } else {
              throw error;
            }
          }
        } else if (self.hasMultipleChoice) {
          // when multiple selection is supported
          let selectedArray: string[] = [];
          // when values are set manually
          // the selected values are without unique identifiers
          // so these selected values are used to get unique ids from the
          // uniqueIds dictionary
          if (self.isPristine) {
            selectedArray = selected.map((selectedItem) => {
              const uniqueStore = self.uniqueIds.get(selectedItem);
              if (uniqueStore) {
                return uniqueStore.uniqueId;
              }
              return "";
            });
            // preserve initial selected values
            self.initialDefinition = { ...self.initialDefinition, multipleSelected: selectedArray };
          } else if (self.dummySelection && selected[selected.length - 1] === self.dummySelection.id) {
            // dummy case that signals about field reset
            selectedArray = [];
          } else if (selected.length > 0) {
            // general case of selecting values
            selectedArray = selected;
          }
          self.multipleSelected = cast(selectedArray);
        }
        // When a field is modified programmatically
        // we need to make it dirty explicitly, so that "dirty" logic
        // will work;
        // Do not walk through the linked list or tree nodes if
        // this field receives a default value from the server
        // and user has no possibility to modify it yet
        if (self.isPristine) {
          self.isPristine = false;
          return;
        }

        // Continuing logic for select / autocomplete
        if (typeof selected === "string" || typeof selected === "undefined") {
          if (self.next && self.children.length) {
            throw new Error("A field cannot have multiple child nodes and a single child node simultaneously.");
          }

          // all values must have prefix of a server value
          // prepended to a unique id to make this logic work
          const requestId = selected ? selected.split(self.separator)[0] : selected;
          // doesn't matter if a selected variable received a reference or not
          // require an update for a child
          if (self.next) {
            self.next.updateValues({
              id: requestId,
              isInternalSet: options?.isInternalSet,
              isDummyChoice,
            });
          }

          if (self.children.length) {
            self.children.forEach((child) => child.updateValues({ id: requestId }));
          }
        }
      } else {
        // Input logic
        let singleValue = "";
        if (typeof self.inputValidatorFunc === "function") {
          // Validate input if necessary or leave the value untouched
          singleValue = self.inputValidatorFunc(`${selected}`);
        } else {
          singleValue = selected ? `${selected}` : "";
        }
        self.singleValue = singleValue;
        self.displayedSingleValue = singleValue;
        if (self.isPristine) {
          self.isPristine = false;
          // preserve initial singleValue and displayedSingleValue
          self.initialDefinition = {
            ...self.initialDefinition,
            singleValue: self.singleValue,
            displayedSingleValue: self.displayedSingleValue,
          };
        }
      }
    },
    setChildren(children: string | string[]) {
      if (typeof children === "string") {
        self.next = children;
      } else {
        self.children = cast(children);
      }
      // get value not a link to object
      self.initialDefinition = getSnapshot(self);
    },
    toggleDisabled(isDisabled: boolean) {
      self.isDisabled = isDisabled;
    },
    setError(error: string) {
      self.error = error;
    },
    toggleRequired(isRequired: boolean) {
      self.isRequired = isRequired;
    },
    touch() {
      if (self.isPristine) {
        self.isPristine = false;
      }
    },
    inputBlur() {
      if (self.blurFunc) {
        // this function is only for displayedSingleValue
        self.displayedSingleValue = self.blurFunc(self.singleValue);
      }
    },
    inputFocus() {
      if (self.focusFunc) {
        // this function is only for displayedSingleValue
        self.displayedSingleValue = self.focusFunc(self.displayedSingleValue);
      }
    },
    formatFieldValues({ list, attributes }: FormatValuesType) {
      return list.map((item) => {
        const valueObject: FormattedValuesObjectType = {
          id: self.generateUniqueId(`${item[attributes.value]}`),
          name: item[attributes.name],
          value: `${item[attributes.value]}`,
          meta: [],
          isDisabled: false,
          isDummy: false,
        };

        if (attributes.meta) {
          Object.entries(attributes.meta).map(([key, getValue]) => {
            const presumedValue = getValue(item);
            if (presumedValue || typeof presumedValue === "boolean") {
              valueObject.meta?.push([key, presumedValue]);
            }
            return 0;
          });
        }
        if (attributes.disabled) {
          valueObject.isDisabled = !!attributes.disabled[valueObject.value];
        }
        if (attributes.dummy) {
          valueObject.isDummy = !!attributes.dummy[valueObject.value];
        }
        return valueObject;
      });
    },
    createSortFunc({
      shouldIgnoreCase = true,
      shouldTrim = false,
      matchValue = "name",
      sortOrder = "asc",
    }: FormValueSortOptionsType = {}) {
      self.sortFunc = (itemA, itemB) => {
        let candidateA = itemA[matchValue];
        let candidateB = itemB[matchValue];
        let sortNumber = 0;

        if (typeof candidateA === "string" && typeof candidateB === "string") {
          if (shouldIgnoreCase) {
            candidateA = candidateA.toLowerCase();
            candidateB = candidateB.toLowerCase();
          }
          if (shouldTrim) {
            candidateA = candidateA.trim();
            candidateB = candidateB.trim();
          }
          if (sortOrder === "asc") {
            if (candidateA === candidateB) {
              sortNumber = 0;
            } else if (candidateA > candidateB) {
              sortNumber = 1;
            } else {
              sortNumber = -1;
            }
          } else if (sortOrder === "desc") {
            if (candidateA === candidateB) {
              sortNumber = 0;
            } else if (candidateA < candidateB) {
              sortNumber = 1;
            } else {
              sortNumber = -1;
            }
          }
        } else if (typeof candidateA === "boolean" && typeof candidateB === "boolean") {
          if (sortOrder === "asc") {
            // true values first
            if (candidateA === candidateB) {
              sortNumber = 0;
            } else if (candidateA) {
              sortNumber = -1;
            } else {
              sortNumber = 1;
            }
          } else if (sortOrder === "desc") {
            // false values first
            if (candidateA === candidateB) {
              sortNumber = 0;
            } else if (candidateA) {
              sortNumber = 1;
            } else {
              sortNumber = -1;
            }
          }
        }
        return sortNumber;
      };
    },
    updateFieldState() {
      // initialDefinition must be set beforehand
      // it can be done either automatically on afterCreate hook
      // or manually via updateFieldInitialDefinitions action
      applySnapshot(self, self.initialDefinition);
    },
    updateFieldInitialDefinitions({
      updateFunc,
      dummySelection,
      defaultSelections,
      requestParams,
      inputValidatorFunc,
      valueFormatter,
      blurFunc,
      focusFunc,
      initialDefinition,
      nullReferenceError,
    }: ParamsUpdateFieldDefinitionsType) {
      if (typeof updateFunc === "function") {
        self.updateFunc = updateFunc;
      }
      if (typeof inputValidatorFunc === "function") {
        self.inputValidatorFunc = inputValidatorFunc;
      }
      if (typeof valueFormatter === "function") {
        self.valueFormatter = valueFormatter;
      }
      if (typeof blurFunc === "function") {
        self.blurFunc = blurFunc;
      }
      if (typeof focusFunc === "function") {
        self.focusFunc = focusFunc;
      }
      if (initialDefinition) {
        self.initialDefinition = initialDefinition;
      }
      if (dummySelection) {
        if (dummySelection.dummyValue) {
          const id = self.generateUniqueId();

          self.dummySelection = {
            id,
            name: dummySelection.dummyName,
            value: `${dummySelection.dummyValue}`,
            isDummy: true,
            meta: dummySelection.meta,
            isDisabled: !!dummySelection.isDisabled,
          };
        }
      }
      if (requestParams) {
        self.requestParams = requestParams;
      }
      // if defaultSelections attribute is provided
      // and its list attribute is a non-empty array
      // and any element of that array is not empty
      if (
        defaultSelections &&
        defaultSelections.list?.length &&
        defaultSelections.list.some((item) => item[defaultSelections.attributes.value])
      ) {
        self.defaultSelections = defaultSelections;
      }
      if (nullReferenceError) {
        self.nullReferenceError = nullReferenceError;
      }
    },
  }))
  .actions((self) => ({
    addDefaults() {
      const defaultValues: FormattedValuesObjectType[] = [];
      // insert in the list of values
      // a dummy one if needed
      if (self.dummySelection && (self.isPristine || self.dummySelection.isDummy)) {
        const id = `${self.dummySelection.value}`;
        self.uniqueIds.set(id, {
          id,
          uniqueId: self.dummySelection.id,
        });
        defaultValues.push(self.dummySelection);
      }
      if (self.defaultSelections && self.isPristine) {
        // get value name from attributes
        const valueName = self.defaultSelections.attributes.value;
        // leave only default values that do not
        // occur in main values
        self.defaultSelections.list = self.defaultSelections.list.filter(
          (defaultValue) => !self.tempValues?.some((tempValue) => tempValue.value === defaultValue[valueName])
        );
        defaultValues.push(...self.formatFieldValues(self.defaultSelections));
      }
      self.tempValues = null;
      return defaultValues;
    },
  }))
  .actions((self) => ({
    updateValues: flow(function* updateValues({ isDummyChoice, ...params }: Record<string, unknown> = {}) {
      if (!self.updateFunc) {
        throw new Error("The function for updating values was not assigned.");
      }
      if (!self.isDisabled) {
        self.isDisabled = true;
      }
      // set selected value only when field is mutated by a user
      if (!self.isPristine) {
        self.setValue(undefined, { isInternalSet: true });
      }
      try {
        // request values on initialization
        // or repeat requests when parent field
        // changes its selection
        if (!params?.isInternalSet && !isDummyChoice) {
          if (self.isPristine || params?.id) {
            const rawValues = yield self.updateFunc({
              ...params,
              ...self.requestParams,
            });
            const newValues = self.formatFieldValues(rawValues);

            if (newValues.length) {
              self.tempValues = newValues;
              const defaultValues = self.addDefaults();

              let formattedValues = [
                // if dummy selection is provided remove dummy value
                // so that it won't be sorted
                ...(self.dummySelection ? defaultValues.slice(1) : defaultValues),
                ...newValues,
              ];
              if (self.sortFunc && newValues.length > 0) {
                formattedValues = formattedValues.sort(self.sortFunc);
              }

              // prepend dummy option into un/sorted array
              if (self.dummySelection) {
                formattedValues.unshift(defaultValues[0]);
              }

              self.values = cast(formattedValues);

              self.isDisabled = false;
              self.error = "";
              if (self.isPristine) {
                // preserve initial values
                self.initialDefinition = {
                  ...self.initialDefinition,
                  values: getSnapshot(self.values),
                };
              }
            }
          }
        }
      } catch (error) {
        self.error = error.message;
      }
    }),
    resetToInitial() {
      const {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        selected,
        singleValue,
        displayedSingleValue,
        values,
        /* eslint-enable */
        ...fieldDefinitions
      } = self.initialDefinition;
      applySnapshot(self, fieldDefinitions);
    },
    resetToInitialWithValues() {
      const {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        selected,
        singleValue,
        displayedSingleValue,
        /* eslint-enable */
        ...fieldDefinitions
      } = self.initialDefinition;
      applySnapshot(self, fieldDefinitions);
    },
    resetToInitialWithValuesAndSelected() {
      applySnapshot(self, self.initialDefinition);
      self.inputBlur();
    },
    setSingleValueClearSelected(value: string) {
      if (self.selected) {
        self.setValue(undefined, { isInternalSet: true });
      }
      self.singleValue = value;
    },
  }))
  .actions((self) => ({
    updateValuesWithSelected: flow(function* updateValuesWithSelected(parentId?: string) {
      // when field is dynamic and needs to be always up to date
      // use this method (only single selection);
      const chosenValue = self.selected?.value;
      self.setValue(undefined);
      self.isPristine = true;
      yield self.updateValues({ id: parentId });
      self.setValue(chosenValue);
    }),
    updateValuesWithMultipleSelected: flow(function* updateValuesWithMultipleSelected(parentId?: string) {
      // when field is dynamic and needs to be always up to date
      // use this method (only single selection);
      const chosenValues = self.multipleSelected.map(({ value }) => value);
      self.setValue([]);
      self.isPristine = true;
      yield self.updateValues({ id: parentId });
      self.setValue(chosenValues);
    }),
    afterCreate() {
      self.initialDefinition = getSnapshot(self);
    },
  }));
