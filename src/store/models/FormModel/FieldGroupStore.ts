import type { Instance } from "mobx-state-tree";
import { types } from "mobx-state-tree";
import { formTranslations } from "@mimir/constants";
import type { ParamsInitFieldType } from "@mimir/types";

import { UniqueIdStore } from "../BaseModels";

import { FormFieldStore } from "./index";
import type { FormFieldStoreModel } from "./index";

export type FieldGroupStoreModel = Instance<typeof FieldGroupStore>;

export const FieldGroupStore = types
  .compose(
    "FieldGroupStore",
    UniqueIdStore,
    types.model({
      fieldGroup: types.map(FormFieldStore),
      massDisable: types.optional(types.boolean, false),
    })
  )
  .views((self) => ({
    get fieldIds() {
      const fieldIds: string[][] = [];
      self.fieldGroup.forEach(({ id, fieldIndex, fieldAnchor }) => {
        if (id) {
          if (fieldIndex) {
            const numberIndex = Number(fieldIndex);
            if (Number.isInteger(numberIndex)) {
              fieldIds.splice(numberIndex, 0, [id, fieldAnchor]);
            } else {
              throw new Error(`Cannot use this value ${fieldIndex} as an index for an array`);
            }
          } else {
            fieldIds.push([id, fieldAnchor]);
          }
        }
      });

      return fieldIds;
    },
    get hasFields() {
      return self.fieldGroup.size > 0;
    },
  }))
  .actions((self) => ({
    innerChangeField(fieldId: string, selected: string) {
      if (!self.massDisable) {
        const field = self.fieldGroup.get(fieldId);
        if (!field) {
          throw new Error("Unable to set a value to a nonexisting field");
        }
        field.setValue(selected);
      }
    },
    innerTouchField(fieldId: string) {
      // this may be useful when field doesn't have a default value
      const field = self.fieldGroup.get(fieldId);
      if (!field) {
        throw new Error("Unable to touch a nonexisting field");
      }
      field.touch();
    },
    resetFields() {
      self.fieldGroup.forEach((field) => {
        field.resetToInitial();
      });
    },
    resetFieldsLeaveValues() {
      self.fieldGroup.forEach((field) => {
        field.resetToInitialWithValues();
      });
    },
    resetFieldsLeaveValuesAndSelected() {
      self.fieldGroup.forEach((field) => {
        field.resetToInitialWithValuesAndSelected();
      });
    },
    updateValuesLeaveAnySelection(needsMassDisable = false) {
      // disable fields mutation in case there is connectivity errors
      // and fields are dynamic
      if (needsMassDisable) {
        self.massDisable = needsMassDisable;
      }
      self.fieldGroup.forEach((field) => {
        if (field.hasMultipleChoice) {
          field.updateValuesWithMultipleSelected(field.id);
        } else {
          field.updateValuesWithSelected(field.id);
        }
      });
      if (self.massDisable) {
        self.massDisable = false;
      }
    },
    getFieldByAttribute({ fieldId, anchor }: { fieldId?: string; anchor?: string }) {
      let requiredField = undefined;
      self.fieldGroup.forEach((field) => {
        if (fieldId && fieldId === field.id) {
          requiredField = field;
        }
        if (anchor && anchor === field.fieldAnchor) {
          requiredField = field;
        }
      });
      return requiredField as FormFieldStoreModel | undefined;
    },
    detectError(errorMessage?: string) {
      let detectedErrorsCounter = 0;
      self.fieldGroup.forEach((field) => {
        // check compulsory values
        // last check is XOR
        if (field.isRequired && (field.selected === undefined ? !field.singleValue : !!field.singleValue)) {
          field.setError(errorMessage || formTranslations.errors.REQUIRED_FIELD);
          ++detectedErrorsCounter;
        }
      });
      if (detectedErrorsCounter > 0) {
        throw new Error("Required values missing");
      }
    },
    initField({
      fieldAnchor,
      label = "",
      isDisabled = false,
      isRequired = false,
      canHavePrefix = true,
      shouldDisplayExternalLabel = false,
      requiredSign = "*",
      index,
      singleValue = "",
      placeholder,
      hasCompletion = false,
      hasMultipleChoice = false,
    }: ParamsInitFieldType) {
      const fieldId = self.generateUniqueId(canHavePrefix ? fieldAnchor : undefined);
      let fieldIndex: string | undefined = undefined;
      if (typeof index === "number") {
        fieldIndex = `${index}`;
      }
      self.fieldGroup.set(fieldId, {
        id: fieldId,
        fieldAnchor,
        label,
        shouldDisplayExternalLabel,
        isDisabled,
        isRequired,
        requiredSign,
        singleValue,
        fieldIndex,
        placeholder,
        hasCompletion,
        hasMultipleChoice,
      });
      return fieldId;
    },
  }));
