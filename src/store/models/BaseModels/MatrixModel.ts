import type { Instance } from "mobx-state-tree";
import { types } from "mobx-state-tree";

export type MatrixStoreModel = Instance<typeof MatrixStore>;

export const MatrixStore = types.array(types.array(types.union(types.string, types.number, types.boolean)));
