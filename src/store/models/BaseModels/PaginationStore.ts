import type { Instance } from "mobx-state-tree";
import { types } from "mobx-state-tree";

export type PaginationStoreModel = Instance<typeof PaginationStore>;

export const PaginationStore = types
  .model("PaginationStore", {
    page: types.optional(types.number, 1),
    totalPages: types.optional(types.number, 0),
    maxPerPage: types.optional(types.number, 10),
  })
  .views(() => ({
    get perPageAmounts() {
      return [100, 50, 20, 10];
    },
  }))
  .actions((self) => ({
    changePage(page = 1) {
      self.page = page;
    },
    setTotalPages(totalPages: number) {
      self.totalPages = totalPages;
    },
    changeMaxPerPage(maxPerPage: number) {
      self.maxPerPage = maxPerPage;
    },
  }));
