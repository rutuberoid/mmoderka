import type { Instance } from "mobx-state-tree";
import { types } from "mobx-state-tree";
import { nanoid } from "nanoid/non-secure";

export type UniqueIdStoreModel = Instance<typeof UniqueIdStore>;

export const UniqueIdStore = types
  .model("UniqueIdStore", {
    uniqueIds: types.map(
      types.model({
        id: types.identifier,
        uniqueId: types.string,
      })
    ),
    separator: types.optional(types.string, "&"),
  })
  .actions((self) => ({
    setSeparator(newSeparator: string) {
      self.separator = newSeparator;
    },
    generateUniqueId(prefix?: string) {
      let uniqueId = nanoid(8);
      if (prefix) {
        uniqueId = prefix + self.separator + uniqueId;
        self.uniqueIds.set(prefix, {
          id: prefix,
          uniqueId: uniqueId,
        });
      }
      return uniqueId;
    },
  }));
