// Import { flow } from "mobx-state-tree";
import { ViewStore } from "@mimir/store";

// IMPORTANT! An object key must match a view name
export const views = {
  home: ViewStore.create({
    name: "home",
    path: "/",
  }),
  example: ViewStore.create({
    name: "example",
    path: "/example",
    hooks: {
      beforeEnter: async (_) => {
        await new Promise((resolve) => setTimeout(resolve, 2000));

        return true;
      },
    },
  }),
  todos: ViewStore.create({
    name: "todos",
    path: "/todos",
    hooks: {
      beforeExit: async (_) => {
        await new Promise((resolve) => setTimeout(resolve, 2000));

        return false;
      },
    },
  }),
  todo: ViewStore.create({
    name: "todo",
    path: "/todos/:todoId",
    hooks: {
      beforeEnter: async (self, params) => {
        await new Promise((resolve) => setTimeout(resolve, 2000));

        self.setRoutingProps({
          todoId: params?.todoId || "",
        });
        return true;
      },
    },
  }),
};
