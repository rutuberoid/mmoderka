import { configureStore } from "mst-storebooster";
import type { RootStoreModel, RootStoreEnv } from "@mimir/store";
import { RouterStore, RootStore, AuthStore } from "@mimir/store";

import { views } from "./createViews";

export type InstantiatedStoresType = typeof InstantiatedStoresBox;
export type StoreBusType = typeof StoreBus;

const routerStoreInstance = RouterStore.create({
  views,
});
const authStoreInstance = AuthStore.create();
export const InstantiatedStoresBox = {
  routerStore: routerStoreInstance,
  authStore: authStoreInstance,
};

export const StoreBus = {
  routerStore: routerStoreInstance,
  authStore: authStoreInstance,
};

// Create root store with necessary stores
// and export all utils

export const { StoreContext, StoreProvider, useStore, rootStore } = configureStore<
  RootStoreModel,
  RootStoreEnv,
  InstantiatedStoresType,
  StoreBusType
>({
  RootStore,
  InstantiatedStoresBox,
  Bus: StoreBus,
  ExecutionEnv: process.env.NODE_ENV,
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type MapStoreType<T> = (store: any, selectedData?: any) => T;

export type PickStoreType<T> = (store: RootStoreModel | null) => T;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useInject = <T1, T2>(mapStore: MapStoreType<T1>, selection?: Record<string, any> | PickStoreType<T2>) => {
  const store = useStore();
  if (typeof selection === "function") {
    const specificStore = selection(store);
    return mapStore(store, specificStore);
  }
  return mapStore(store, selection);
};
