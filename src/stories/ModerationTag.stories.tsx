import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationTag } from "@mimir/components";
import { Stack } from "@mui/material";

export default {
  title: "Data Display/ModerationTag",
  component: ModerationTag,
  argTypes: {
    contentType: {
      table: {
        category: "Data",
        type: {
          summary: "Controls colors of the component and texts, that are displayed.",
        },
      },
    },
  },
} as ComponentMeta<typeof ModerationTag>;

const Template: ComponentStory<typeof ModerationTag> = (args) => <ModerationTag {...args} />;

export const AllTags: ComponentStory<typeof ModerationTag> = () => (
  <Stack direction={"row"} flexWrap={"wrap"} spacing={1}>
    <ModerationTag contentType={"sport"} />
    <ModerationTag contentType={"children"} />
  </Stack>
);
AllTags.storyName = "All tags";

export const Sport = Template.bind({});
Sport.args = {
  contentType: "sport",
};
Sport.storyName = "Sport";

export const Children = Template.bind({});
Children.args = {
  contentType: "children",
};
Children.storyName = "Childish content";
