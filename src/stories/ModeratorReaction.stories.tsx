import React from "react";
import type { ComponentMeta, ComponentStory } from "@storybook/react";
import { ModeratorReaction } from "@mimir/components";

export default {
  title: "Surface/ModeratorReaction",
  component: ModeratorReaction,
  argTypes: {
    moderatorName: {
      table: {
        category: "Data",
        type: {
          summary: "Moderator name",
        },
      },
    },
    reactionType: {
      table: {
        category: "Data",
        type: {
          detail:
            '"Signals that a moderation was automatic, manual or overdue. One of:\n' +
            '- "manual" (default)\n' +
            '- "auto"\n' +
            '- "overdue"',
        },
      },
      control: {
        type: "select",
      },
    },
    reactionCode: {
      table: {
        category: "Data",
        type: {
          detail: "Shows moderator reaction to the comment",
        },
      },
      control: {
        type: "select",
      },
    },
    reactionDate: {
      table: {
        category: "Data",
        type: {
          summary: "Shows, when moderator reacted to the comment",
        },
      },
      control: {
        type: "text",
      },
    },
    dateUntil: {
      table: {
        category: "Data",
        type: {
          summary: "Shows until when the comment has moderator restrictions",
        },
      },
      control: {
        type: "text",
      },
    },
    avatarUrl: {
      table: {
        category: "Data",
        type: {
          summary: "Avatar URL",
        },
      },
    },
    reasons: {
      table: {
        category: "Data",
        type: {
          summary: "Array of objects, that consists of 'short reason' and 'verbose reason' properties.",
        },
      },
    },
  },
} as ComponentMeta<typeof ModeratorReaction>;

const Template: ComponentStory<typeof ModeratorReaction> = (args) => (
  <ModeratorReaction
    /* eslint-disable-next-line react/jsx-props-no-spreading */
    {...args}
  />
);

export const ManualBan = Template.bind({});
ManualBan.args = {
  avatarUrl: "https://pic.rutube.ru/user/87/3c/873cb1a321bc427821d92f53dd93795a.jpg",
  reactionType: "MANUAL",
  reactionDate: "12.07.2022 16:42",
  moderatorName: "Olga Waterfall",
  reasons: [
    {
      shortReason: "1. Пропаганда",
      verboseReason: "Japanese mantis shrimp, is a species of mantis shrimp found in the Western Pacific.",
    },
    {
      shortReason: "4. Дискриминация",
      verboseReason: "Emperor Crabs were a large, extinct species of crab native to the region of SkyRim.",
    },
    {
      shortReason: "6. Оскорбление",
      verboseReason: "The most notable characteristic of the cats is their feline appearance.",
    },
  ],
  reactionCode: "BAN_MANUAL",
  dateUntil: "24.07.2023 20:00",
};

export const ManualUnban = Template.bind({});
ManualUnban.args = {
  ...ManualBan.args,
  reactionCode: "UNBAN_MANUAL",
  reasons: [],
  dateUntil: "",
};

export const ManualApprove = Template.bind({});
ManualApprove.args = {
  ...ManualUnban.args,
  reactionCode: "APPROVAL_MANUAL",
};

export const ManualDelete = Template.bind({});
ManualDelete.args = {
  ...ManualUnban.args,
  reactionCode: "DELETION_MANUAL",
};

export const ManualRestore = Template.bind({});
ManualRestore.args = { ...ManualUnban.args, reactionCode: "RESTORE_MANUAL" };

export const AutoModeration = Template.bind({});
AutoModeration.args = { ...ManualUnban.args, reactionType: "AUTO" };

export const RadioControl = Template.bind({});
RadioControl.args = { ...ManualUnban.args, reactionType: "RADIOCONTROL" };

export const Overdue = Template.bind({});
Overdue.args = { ...ManualUnban.args, reactionType: "OVERDUE" };
