import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModeratorAction } from "@mimir/components";
import { action } from "@storybook/addon-actions";

export default {
  title: "Input/ModeratorAction",
  component: ModeratorAction,
  argTypes: {
    actionType: {
      table: {
        category: "Data",
        type: {
          detail:
            "Set action type. One of:\n" +
            '- "confirm" (default)\n' +
            '- "delete"\n' +
            '- "restore"\n' +
            '- "history"\n' +
            '- "unban"',
        },
      },
      control: {
        type: "select",
      },
    },
    onAction: {
      table: {
        category: "Event",
        type: {
          summary: "Action handler",
        },
      },
    },
  },
} as ComponentMeta<typeof ModeratorAction>;

const Template: ComponentStory<typeof ModeratorAction> = (args) => <ModeratorAction {...args} />;

export const ConfirmAction = Template.bind({});
ConfirmAction.args = {
  actionType: "confirm",
  onAction: action("Action has happened"),
};
ConfirmAction.storyName = "Confirm";

export const DeleteAction = Template.bind({});
DeleteAction.args = {
  ...ConfirmAction.args,
  actionType: "delete",
};
DeleteAction.storyName = "Delete";

export const RestoreAction = Template.bind({});
RestoreAction.args = {
  ...ConfirmAction.args,
  actionType: "restore",
};
RestoreAction.storyName = "Restore";

export const HistoryAction = Template.bind({});
HistoryAction.args = {
  ...ConfirmAction.args,
  actionType: "history",
};
HistoryAction.storyName = "History";

export const UnbanAction = Template.bind({});
UnbanAction.args = {
  ...ConfirmAction.args,
  actionType: "unban",
};
UnbanAction.storyName = "Unban";
