import React, { useState } from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import type { MessageType } from "@mimir/components";
import { ProgressSnackbar } from "@mimir/components";
import { Button, Stack } from "@mui/material";
import { action } from "@storybook/addon-actions";

export default {
  title: "Feedback/ProgressSnackbar",
  component: ProgressSnackbar,
  argTypes: {
    value: {
      table: {
        category: "Data",
        type: {
          summary: "An object with message-text and unique message-id",
        },
      },
    },
    onClose: {
      table: {
        category: "Event",
        type: {
          summary: "Called, when the component is removed from the DOM",
        },
      },
    },
    canOpen: {
      table: {
        category: "Visual",
        type: {
          summary: "Allows the component to show itself",
        },
      },
    },
  },
} as ComponentMeta<typeof ProgressSnackbar>;

const genKey = () => `${Date.now()}_${`${Math.random()}`.slice(-5)}`;

const getMessage = () => {
  const messages = [
    "Валерий Кузнецов: В бухгалтерии всё перепутали",
    "Олег Император: В галактике всё перепутали",
    "Анна Некроз: В зиккурате всё перепутали",
    "Денис Петров: В аэропорту всё перепутали",
    "Николай Должанский: В званном ужине всё перепутали",
  ];
  const index = Math.floor(messages.length * Math.random());

  return {
    id: genKey(),
    text: messages[index],
  };
};

const Template: ComponentStory<typeof ProgressSnackbar> = ({ canOpen, onClose, message }) => {
  const [messageInfo, setMessageInfo] = useState<MessageType>(getMessage);
  const handleClick = () => {
    setMessageInfo(getMessage());
  };

  return (
    <>
      <Button onClick={handleClick} variant={"outlined"}>
        Show message
      </Button>
      <ProgressSnackbar canOpen={canOpen} onClose={onClose} message={message?.text ? message : messageInfo} />
    </>
  );
};

const TemplateAlternative: ComponentStory<typeof ProgressSnackbar> = ({ canOpen, onClose }) => {
  const [messageInfo, setMessageInfo] = useState<MessageType>({
    id: genKey(),
    text: "First render value",
  });
  const handleClick = (text: string) => {
    setMessageInfo({
      id: genKey(),
      text,
    });
  };

  return (
    <>
      <Stack spacing={2} direction={"row"}>
        <Button onClick={() => handleClick("Text A: First value")} variant={"outlined"}>
          Show message A
        </Button>
        <Button onClick={() => handleClick("Text B: Second value")} variant={"outlined"}>
          Show message B
        </Button>
        <Button onClick={() => handleClick("Text C: Third value")} variant={"outlined"}>
          Show message C
        </Button>
      </Stack>
      <ProgressSnackbar canOpen={canOpen} onClose={onClose} message={messageInfo} />
    </>
  );
};

export const CommonCase = Template.bind({});
CommonCase.args = {
  onClose: action("Snackbar closed"),
  canOpen: true,
  message: {
    id: "",
    text: "",
  },
};
CommonCase.storyName = "Common case";

export const TurnedOff = Template.bind({});
TurnedOff.args = {
  ...CommonCase.args,
  canOpen: false,
};
TurnedOff.storyName = "Turned off";

export const StaticMessages = TemplateAlternative.bind({});
StaticMessages.args = {
  ...CommonCase.args,
};
StaticMessages.storyName = "Buttons with static values";
