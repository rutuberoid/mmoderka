import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistory } from "@mimir/app";

export default {
  title: "Data Display/ModerationHistory",
  component: ModerationHistory,
} as ComponentMeta<typeof ModerationHistory>;

const Template: ComponentStory<typeof ModerationHistory> = () => <ModerationHistory />;

export const AllProps = Template.bind({});
AllProps.storyName = "With all data";
