import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistoryItem } from "@mimir/components";

import Moderator from "./Moderator.stories";

export default {
  ...Moderator,
  title: "Data Display/ModerationHistoryItem/Author",
} as ComponentMeta<typeof ModerationHistoryItem>;

const Template: ComponentStory<typeof ModerationHistoryItem> = (args) => <ModerationHistoryItem {...args} />;

export const AddEvent = Template.bind({});
AddEvent.args = {
  eventType: "add",
  eventUserType: "author",
  eventUserId: 231264,
  eventUserName: "Валерий Иванов",
  eventDetails: [],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
};
AddEvent.storyName = "Add";

export const EditEvent = Template.bind({});
EditEvent.args = {
  ...AddEvent.args,
  eventType: "edit",
  eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
};
EditEvent.storyName = "Edit";

export const DeleteEvent = Template.bind({});
DeleteEvent.args = {
  ...AddEvent.args,
  eventType: "delete",
};
DeleteEvent.storyName = "Delete";
