import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistoryItem } from "@mimir/components";

export default {
  title: "Data Display/ModerationHistoryItem/Moderator",
  component: ModerationHistoryItem,
  argTypes: {
    eventType: {
      table: {
        category: "Data",
        type: {
          detail:
            "Event type. One of:\n" +
            '- "approve"\n' +
            '- "reject"\n' +
            '- "report"\n' +
            '- "delete"\n' +
            '- "restore"\n' +
            '- "edit"\n' +
            '- "add"\n' +
            '- "ban"\n' +
            '- "unban"\n' +
            '- "processing"' +
            '- "assign"',
        },
      },
      control: {
        type: "select",
      },
    },
    eventUserType: {
      table: {
        category: "Data",
        type: {
          detail:
            "Event user type. One of:\n" +
            '- "creator"\n' +
            '- "author"\n' +
            '- "user"\n' +
            '- "moderator"\n' +
            '- "automoderator"\n' +
            '- "radioControl"\n' +
            '- "system"',
        },
      },
      control: {
        type: "select",
      },
    },
    eventUserId: {
      table: {
        category: "Data",
        type: {
          summary: "Event user ID",
        },
      },
    },
    eventUserName: {
      table: {
        category: "Data",
        type: {
          summary: "Event user name",
        },
      },
    },
    eventDetails: {
      table: {
        category: "Data",
        type: {
          summary: "Additional info about the current event",
        },
      },
    },

    eventTime: {
      table: {
        category: "Data",
        type: {
          summary: "Event time",
        },
      },
    },
    eventDate: {
      table: {
        category: "Data",
        type: {
          summary: "Event date",
        },
      },
      control: {
        type: "text",
      },
    },
    isFirst: {
      table: {
        category: "Visual",
        type: {
          summary: "Indicates that item is first",
        },
      },
    },
    isLast: {
      table: {
        category: "Visual",
        type: {
          summary: "Indicates that item is last",
        },
      },
    },
  },
} as ComponentMeta<typeof ModerationHistoryItem>;

const Template: ComponentStory<typeof ModerationHistoryItem> = (args) => <ModerationHistoryItem {...args} />;

export const ApproveEvent = Template.bind({});
ApproveEvent.args = {
  eventType: "approve",
  eventUserType: "moderator",
  eventUserId: 231264,
  eventUserName: "Валерий Иванов",
  eventDetails: [],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
};
ApproveEvent.storyName = "Approve";

export const AssignEvent = Template.bind({});
AssignEvent.args = {
  ...ApproveEvent.args,
  eventType: "assign",
};
AssignEvent.storyName = "Assign";

export const DeleteEvent = Template.bind({});
DeleteEvent.args = {
  ...ApproveEvent.args,
  eventType: "delete",
};
DeleteEvent.storyName = "Delete";

export const BanEvent = Template.bind({});
BanEvent.args = {
  ...ApproveEvent.args,
  eventType: "ban",
  eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
};
BanEvent.storyName = "Ban";

export const UnbanEvent = Template.bind({});
UnbanEvent.args = {
  ...ApproveEvent.args,
  eventType: "unban",
};
UnbanEvent.storyName = "Unban";

export const RestoreEvent = Template.bind({});
RestoreEvent.args = {
  ...ApproveEvent.args,
  eventType: "restore",
};
RestoreEvent.storyName = "Restore";
