import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistoryItem } from "@mimir/components";

import Moderator from "./Moderator.stories";

export default {
  ...Moderator,
  title: "Data Display/ModerationHistoryItem/Automoderator",
} as ComponentMeta<typeof ModerationHistoryItem>;

const Template: ComponentStory<typeof ModerationHistoryItem> = (args) => <ModerationHistoryItem {...args} />;

export const DeleteEvent = Template.bind({});
DeleteEvent.args = {
  eventType: "delete",
  eventUserType: "automoderator",
  eventUserId: 231264,
  eventUserName: "Валерий Иванов",
  eventDetails: [],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
};
DeleteEvent.storyName = "Delete";
