import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistoryItem } from "@mimir/components";

import Moderator from "./Moderator.stories";

export default {
  ...Moderator,
  title: "Data Display/ModerationHistoryItem/Creator",
} as ComponentMeta<typeof ModerationHistoryItem>;

const Template: ComponentStory<typeof ModerationHistoryItem> = (args) => <ModerationHistoryItem {...args} />;

export const DeleteEvent = Template.bind({});
DeleteEvent.args = {
  eventType: "delete",
  eventUserType: "creator",
  eventUserId: 231264,
  eventUserName: "Валерий Иванов",
  eventDetails: [],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
};
DeleteEvent.storyName = "Delete";

export const BanEvent = Template.bind({});
BanEvent.args = {
  ...DeleteEvent.args,
  eventType: "ban",
  eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
};
BanEvent.storyName = "Ban";

export const UnbanEvent = Template.bind({});
UnbanEvent.args = {
  ...DeleteEvent.args,
  eventType: "unban",
};
UnbanEvent.storyName = "Unban";

export const RestoreEvent = Template.bind({});
RestoreEvent.args = {
  ...DeleteEvent.args,
  eventType: "restore",
};
RestoreEvent.storyName = "Restore";
