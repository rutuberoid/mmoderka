import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { ModerationHistoryItem } from "@mimir/components";

import Moderator from "./Moderator.stories";

export default {
  ...Moderator,
  title: "Data Display/ModerationHistoryItem/User",
} as ComponentMeta<typeof ModerationHistoryItem>;

const Template: ComponentStory<typeof ModerationHistoryItem> = (args) => <ModerationHistoryItem {...args} />;

export const ReportEvent = Template.bind({});
ReportEvent.args = {
  eventType: "report",
  eventUserType: "user",
  eventUserId: 231264,
  eventUserName: "Валерий Иванов",
  eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
  eventTime: "11:38",
  eventDate: "21.10.2021",
  isFirst: false,
  isLast: false,
};
ReportEvent.storyName = "Report";
