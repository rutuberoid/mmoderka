import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { CommentsBlankCard } from "@mimir/app";

export default {
  title: "Surface/CommentsBlankCard",
  component: CommentsBlankCard,
} as ComponentMeta<typeof CommentsBlankCard>;

const Template: ComponentStory<typeof CommentsBlankCard> = () => <CommentsBlankCard />;

export const Default = Template.bind({});
Default.storyName = "Default";
