import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { PolicyOutlined } from "@mui/icons-material";
import { grey } from "@mui/material/colors";
import { InlineMessage } from "@mimir/components";

export default {
  title: "Data Display/InlineMessage",
  component: InlineMessage,
  argTypes: {
    textsList: {
      table: {
        category: "Visual",
        type: {
          summary: "Array of objects (Typography props) or ReactNodes",
        },
      },
      control: {
        disable: true,
      },
    },
    textsOffset: {
      table: {
        category: "Visual",
        type: {
          summary: "Controls offset distance between texts",
        },
      },
    },
    startIcon: {
      table: {
        category: "Visual",
        type: {
          summary: "Any ReactNode, displayed at the start of the component.",
        },
      },
      control: {
        disable: true,
      },
    },
    endIcon: {
      table: {
        category: "Visual",
        type: {
          summary: "Any ReactNode, displayed at the end of the component.",
        },
      },
      control: {
        disable: true,
      },
    },
  },
} as ComponentMeta<typeof InlineMessage>;

const Template: ComponentStory<typeof InlineMessage> = (args) => <InlineMessage {...args} />;

export const TextsOnly = Template.bind({});
TextsOnly.args = {
  textsList: [
    <span key={"stroke"} color={grey[600]}>
      text <b>bold text</b>
    </span>,
    <span key={"dot"} color={grey[600]}>
      •
    </span>,
    { color: grey[600], children: "12.07.2022 16:42" },
  ],
  textsOffset: 1,
};
TextsOnly.storyName = "Texts only";

export const WithStartIcon = Template.bind({});
WithStartIcon.args = {
  ...TextsOnly.args,
  startIcon: <PolicyOutlined />,
};
WithStartIcon.storyName = "With start icon (button)";

export const WithEndIcon = Template.bind({});
WithEndIcon.args = {
  ...TextsOnly.args,
  endIcon: <PolicyOutlined />,
};
WithEndIcon.storyName = "With end icon (button)";

export const AllProps = Template.bind({});
AllProps.args = {
  ...TextsOnly.args,
  startIcon: <PolicyOutlined />,
  endIcon: <PolicyOutlined />,
};
AllProps.storyName = "All elements displayed";
