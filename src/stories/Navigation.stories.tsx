import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { Navigation } from "@mimir/components";

export default {
  title: "navigation/Navigation",
  component: Navigation,
} as ComponentMeta<typeof Navigation>;

const Template: ComponentStory<typeof Navigation> = ({ isWide }) => <Navigation isWide={isWide} />;

export const NarrowNavigationMenu = Template.bind({});
NarrowNavigationMenu.args = {
  isWide: false,
};
NarrowNavigationMenu.storyName = "Narrow navigation menu";

export const WideNavigationMenu = Template.bind({});
WideNavigationMenu.args = {
  isWide: true,
};
WideNavigationMenu.storyName = "Wide navigation menu";
