import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { CreatorInfoPopover } from "@mimir/components";
import { Box } from "@mui/material";
import { action } from "@storybook/addon-actions";

export default {
  title: "Data Display/CreatorInfoPopover",
  component: CreatorInfoPopover,
  argTypes: {
    creatorName: {
      table: {
        category: "Data",
        type: {
          summary: "Creator name",
        },
      },
    },
    creatorId: {
      table: {
        category: "Data",
        type: {
          summary: "Creator id",
        },
      },
    },
    creatorAvatarUrl: {
      table: {
        category: "Data",
        type: {
          summary: "Creator avatar URL",
        },
      },
    },
    copyCreatorId: {
      table: {
        category: "Event",
        type: {
          summary: 'Copies "creatorAvatarUrl"',
        },
      },
    },
    isBanned: {
      table: {
        category: "Visual",
        type: {
          summary: "Indicates that creator is banned",
        },
      },
    },
    canFilterById: {
      table: {
        category: "Visual",
        type: {
          summary: 'Allows the application to show option for filtering by "creatorId"',
        },
      },
    },
    creatorChannelUrl: {
      table: {
        category: "Data",
        type: {
          summary: "Creator channel URL",
        },
      },
    },
    filterByCreatorId: {
      table: {
        category: "Event",
        type: {
          summary: 'Filters comments by "creatorId"',
        },
      },
    },
    banByCreatorId: {
      table: {
        category: "Event",
        type: {
          summary: 'Bans creator by "creatorId"',
        },
      },
    },
  },
} as ComponentMeta<typeof CreatorInfoPopover>;

const Template: ComponentStory<typeof CreatorInfoPopover> = (args) => (
  <Box
    sx={{
      "paddingY": 3,
      "paddingX": 3,
      "&:hover .hoverCreator": {
        textDecorationStyle: "dashed",
        textDecorationLine: "underline",
        textUnderlineOffset: "6px",
      },
    }}
  >
    <CreatorInfoPopover {...args} />
  </Box>
);

export const AllProps = Template.bind({});
AllProps.args = {
  banByCreatorId: action("User was banned"),
  copyCreatorId: action("Copied creator id"),
  filterByCreatorId: action("Data filter"),
  creatorId: "1522312",
  creatorAvatarUrl: "https://pic.rutube.ru/user/87/3c/873cb1a321bc427821d92f53dd93795a.jpg",
  creatorChannelUrl: "#",
  creatorName: "Olga Waterfall",
  isBanned: true,
  canFilterById: true,
};
AllProps.storyName = "With all data";

export const WithoutFilter = Template.bind({});
WithoutFilter.args = { ...AllProps.args, canFilterById: false };
WithoutFilter.storyName = "Without filter";

export const BrokenAvatar = Template.bind({});
BrokenAvatar.args = { ...AllProps.args, creatorAvatarUrl: "" };
BrokenAvatar.storyName = "Avatar placeholder";

export const NotBannedUser = Template.bind({});
NotBannedUser.args = { ...AllProps.args, isBanned: false };
NotBannedUser.storyName = "Without ban text";

export const LongUserName = Template.bind({});
LongUserName.args = {
  ...AllProps.args,
  creatorName: "PineappleApplePen".repeat(10),
};
LongUserName.storyName = "With long moderator name";
