import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BanModal } from "@mimir/app";

export default {
  title: "Utils/BanModal",
  component: BanModal,
} as ComponentMeta<typeof BanModal>;

const Template: ComponentStory<typeof BanModal> = () => <BanModal />;

export const ControlledData = Template.bind({});
ControlledData.storyName = "Controlled data";
