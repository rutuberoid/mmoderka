import React, { useState } from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { Box } from "@mui/material";
import { ContentPagination } from "@mimir/components";

export default {
  title: "Navigation/ContentPagination",
  component: ContentPagination,
  argTypes: {
    changePage: {
      table: {
        category: "Data",
        type: {
          summary: "Changes current page",
        },
      },
    },
    count: {
      table: {
        category: "Data",
        type: {
          summary: "Total amount of pages",
        },
      },
    },
    page: {
      table: {
        category: "Data",
        type: {
          summary: "Current page",
        },
      },
    },
    maxPerPage: {
      table: {
        category: "Data",
        type: {
          summary: "Maximum amount of content displayed per page",
        },
      },
    },
    perPageAmount: {
      table: {
        category: "Data",
        type: {
          summary: "List of per page amounts",
        },
      },
    },
    changeMaxPerPage: {
      table: {
        category: "Data",
        type: {
          summary: "Changes maximum amount of content displayed per page",
        },
      },
    },
  },
} as ComponentMeta<typeof ContentPagination>;

const Template: ComponentStory<typeof ContentPagination> = (args) => {
  const [page, setPage] = useState<number>(1);
  const [maxPerPage, setMaxPerPage] = useState<number>(10);
  return (
    <Box mt="12%" px={2}>
      <ContentPagination
        {...args}
        page={page}
        changePage={setPage}
        maxPerPage={maxPerPage}
        changeMaxPerPage={setMaxPerPage}
      />
    </Box>
  );
};

export const DefaultCase = Template.bind({});
DefaultCase.args = {
  count: 10,
  perPageAmount: [10, 20, 50, 100].reverse(),
};
DefaultCase.storyName = "Default pagination";
