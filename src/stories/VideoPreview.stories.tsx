import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { VideoPreview } from "@mimir/components";
import { Box } from "@mui/material";
import { action } from "@storybook/addon-actions";

export default {
  title: "Data Display/VideoPreview",
  component: VideoPreview,
  argTypes: {
    videoId: {
      table: {
        category: "Data",
        type: {
          summary: "Video ID",
        },
      },
    },
    videoTitle: {
      table: {
        category: "Data",
        type: {
          summary: "Video title",
        },
      },
    },
    videoUrl: {
      table: {
        category: "Data",
        type: {
          summary: "Video url",
        },
      },
    },
    videoPreviewUrl: {
      table: {
        category: "Data",
        type: {
          summary: "Video preview url",
        },
      },
    },
    copyVideoId: {
      table: {
        category: "Event",
        type: {
          summary: 'Copies "videoId"',
        },
      },
    },
    openVideoModal: {
      table: {
        category: "Event",
        type: {
          summary: "Opens video modal",
        },
      },
    },
  },
} as ComponentMeta<typeof VideoPreview>;

const Template: ComponentStory<typeof VideoPreview> = (args) => {
  const { videoPreviewUrl } = args;
  return (
    <Box
      sx={{
        "paddingY": 3,
        "paddingX": 3,
        "&:hover .playIcon": {
          display: "block",
        },
        "&:hover .hoverCopyButton": {
          display: "inline-flex",
        },
        "&:hover .hoverVideoTitle": {
          color: (theme) => theme.palette.primary.main,
        },
        "&:hover .hoverVideoPreview": {
          backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25)), url(${videoPreviewUrl})`,
        },
      }}
    >
      <VideoPreview {...args} />
    </Box>
  );
};

export const AllProps = Template.bind({});
AllProps.args = {
  copyVideoId: action("Copied video id"),
  openVideoModal: action("Opened video modal"),
  videoId: "3e2495c423c2c60753274d664df0f80e",
  videoTitle: "Холостяк, 9 сезон, 11 выпуск",
  videoUrl: "https://rutube.dev/video/3e2495c423c2c60753274d664df0f80e/",
  videoPreviewUrl: "https://pic.rutube.dev/promoitem/ba/13/ba13306ef8c8557ccbb1f66ee9dab177.jpg",
};
AllProps.storyName = "Base";
