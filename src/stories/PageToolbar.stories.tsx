import React from "react";
import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { PageToolbar } from "@mimir/app";

export default {
  title: "PageToolbar",
  component: PageToolbar,
} as ComponentMeta<typeof PageToolbar>;

export const Sample: ComponentStory<typeof PageToolbar> = () => <PageToolbar toggleNavMenu={() => undefined} />;
