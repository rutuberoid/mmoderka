import type { EventUserType, EventType } from "@mimir/types";

export const commentTranslations = {
  labels: {
    APPROVAL_MANUAL: "Утвердил(а)",
    BAN_MANUAL: "Забанил(а)",
    DELETION_MANUAL: "Удалил(а)",
    DELETION_AUTO: "Удалено",
    DELETION_RADIOCONTROL: "Удалено по требованию",
    DELETED_BY_AUTO: "Автомодерацией",
    DELETED_BY_RADIOCONTROL: "ГРЧЦ",
    RESTORE_MANUAL: "Восстановил(а)",
    UNBAN_MANUAL: "Разбанил(а)",
    TERM: "Срок",
    OVERDUE: "Срок бана истёк",
    REASON: "Причина",
    COMMENT_AMOUNT: "комментариев",
    CHILDISH_CONTENT: "Детский контент",
    SPORT_CONTENT: "Rutube Sport",
    AUTHOR_ID: "ID автора",
    MODERATOR_ID: "ID модератора",
    USER_ID: "ID пользователя",
    CREATOR_ID: "ID креатора",
    EDIT: "изменения",
    REPORT: "жалобу",
    BAN: "причину",
    history: ({ eventUserType, eventUserId }: { eventUserType: EventUserType; eventUserId: number }) => {
      const translation =
        commentTranslations.labels[`${eventUserType.toUpperCase()}_ID` as keyof typeof commentTranslations.labels];

      if (!translation) {
        return;
      }

      return `${translation}: ${eventUserId}`;
    },
    eventDetails: ({ eventType, areReasonsDisplayed }: { eventType: EventType; areReasonsDisplayed: boolean }) => {
      const action = areReasonsDisplayed ? commentTranslations.actions.HIDE : commentTranslations.actions.VIEW;

      switch (eventType) {
        case "edit":
          return `${action} ${commentTranslations.labels.EDIT}`;
        case "report":
          return `${action} ${commentTranslations.labels.REPORT}`;
        case "ban":
          return `${action} ${commentTranslations.labels.BAN}`;
      }
    },
  },
  hints: {
    NO_COMMENTS: "У тебя в работе пока нет комментариев. Загрузить ещё",
    INSULT: "Если в комментарии есть прямое оскорбление, смело удаляй его",
  },
  notifications: {
    RESTORE: "Восстановить",
    COMMENT_DELETED: "Комментарий удалён",
  },
  titles: {
    historyDialog: "История комментария",
    banAuthor: (value: number) => (value === 1 ? "Забанить автора" : "Забанить авторов"),
    history: ({
      eventUserType,
      eventType,
      eventUserName,
    }: {
      eventUserType: EventUserType;
      eventType: EventType;
      eventUserName: string;
    }) => {
      const translations = {
        automoderator: "Комментарий удалён Автомодерацией",
        radioControl: "Комментарий удалён по требованию ГРЧЦ",
        report: "Жалоба от пользователя",
        approve: "Комментарий утвердил(а)",
        ban: "Комментарий забанил(а)",
        reject: "Комментарий отклонил(а)",
        delete: "Комментарий удалил(а)",
        edit: "Комментарий изменён автором",
        add: "Комментарий опубликован автором",
        unban: "Комментарий разбанил(а)",
        processing: "В процессе обработки",
        assign: "Комментарий назначен на",
        restore: "Комментарий восстановил(а)",
      };

      if (eventUserType === "automoderator" || eventUserType === "radioControl") {
        return translations[eventUserType];
      }

      return `${translations[eventType]} ${eventUserName}`;
    },
  },
  actions: {
    HIDE: "Скрыть",
    VIEW: "Посмотреть",
  },
} as const;
