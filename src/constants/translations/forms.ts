export const formTranslations = {
  labels: {
    BAN_REASON: "Причина бана",
    FOREVER: "Навсегда",
    banDuration: (value: number) => {
      let text = `Забанить на ${value} `;
      const remainder = value % 10;

      switch (remainder) {
        case 1:
          text += "день";
          break;
        case 2:
        case 3:
        case 4:
          text += "дня";
          break;
        default:
          text += "дней";
      }

      return text;
    },
  },
  actions: {
    BAN_FOREVER: "Забанить навсегда",
  },
  placeholders: {
    BAN_PICK_REASON: "Введи или выбери причину",
  },
  errors: {
    EMPTY_REASON: "Необходимо выбрать причину",
    REQUIRED_FIELD: "Обязательное поле",
  },
} as const;
