export const errorTranslations = {
  PERMISSION_ERROR: "Недостаточно прав",
  HTTP_ERROR: "Ошибка получения информации",
  EMPTY_RESPONSE_ERROR: "Нет данных",
  REQUEST_ERROR: "Ошибка отправки запроса",
  UNKNOWN_ERROR: "Неизвестная ошибка",
  NOT_AUTHORIZED_ERROR: "Пользователь не авторизован",
} as const;
