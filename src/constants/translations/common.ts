const commonTranslations = {
  titles: {
    DASHBOARD: "Дашборд",
    COMMENTS_AND_CHATS: "Комментарии/чаты",
    VIDEO: "Видео",
    BROADCASTS: "Трансляции",
    AUDIO: "Аудио",
    DICTIONARY: "Словарь",
    MODERATORS: "Модераторы",
  },
  deadEnd: {
    NO_COMPONENT: "Обработка пути не завершена или компонент отсутствует для выбранного пути",
  },
  actions: {
    LOAD: "Загрузить",
    LOGOUT: "Выйти",
    OPEN_MENU_SETTINGS: "Настройки",
    FILTER_BY_ID: "Отфильтровать по ID",
    WATCH_ON_RUTUBE: "Посмотреть на Rutube",
    BAN_CREATOR: "Забанить автора",
    CONFIRM: "Утвердить",
    DELETE: "Удалить",
    RESTORE: "Восстановить",
    HISTORY: "История",
    UNBAN: "Разбанить",
  },
  labels: {
    BANNED: "Забанен",
    ID: "ID",
    PER_PAGE_DISPLAY: "Показывать по: ",
  },
  prepositions: {
    UNTIL: "До",
  },
} as const;

export { commonTranslations };
