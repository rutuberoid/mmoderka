import type { RoutingPathNamesType } from "@mimir/types";

const ROUTING_PATH_NAMES: RoutingPathNamesType[] = ["home", "example", "todos", "todo"];

export { ROUTING_PATH_NAMES };
