type FormattedValuesObjectType = {
  id: string;
  name: string;
  value: string;
  meta: any[][] | undefined;
  isDisabled: boolean;
  isDummy: boolean;
};

type InputValidatorType = (value: string) => string;
type ValueFormatterType = (value: string) => string;
type BlurFuncType = (value: string) => string;
type FocusFuncType = (value: string) => string;
type SortFuncType = (a: FormattedValuesObjectType, b: FormattedValuesObjectType) => number;

type FormValueType = {
  name: string;
  value: number | string;
  // means there is no point in this default value,
  // it is just a stub
  isDummy?: boolean;
  isDisabled?: boolean;
  meta?: any[][];
};

type FormValueSortOptionsType = {
  // Defaults to true. Lowercase everything
  shouldIgnoreCase?: boolean;
  // Defaults to false. Remove trailing spaces
  shouldTrim?: boolean;
  // Defaults to "name". Sort by value of a given property
  matchValue?: keyof Omit<FormValueType, "meta">;
  // Defaults to "asc".
  sortOrder?: "asc" | "desc";
};

type FormatValuesType = {
  list: Record<string, any>[];
  attributes: {
    name: string;
    value: string;
    meta?: {
      [key: string]: (listItem: any) => string | boolean | number | undefined;
    };
    disabled?: {
      // key maps to the value
      [key: string]: boolean;
    };
    dummy?: {
      // key maps to the value
      [key: string]: boolean;
    };
  };
};

type UpdateValuesType = (params?: Record<string, unknown>) => Promise<FormatValuesType>;

type ParamsUpdateFieldDefinitionsType = {
  updateFunc?: UpdateValuesType;
  // means there is no point in this default value,
  // it is just a stub
  dummySelection?: {
    dummyName: string;
    dummyValue: number | string;
    isDisabled?: boolean;
    meta?: any[][];
  };
  defaultSelections?: FormatValuesType;
  requestParams?: Record<string, unknown>;
  inputValidatorFunc?: InputValidatorType;
  valueFormatter?: ValueFormatterType;
  blurFunc?: BlurFuncType;
  focusFunc?: FocusFuncType;
  createSortFunc?: (options?: FormValueSortOptionsType) => void;
  initialDefinition?: Partial<{
    id: string;
    fieldAnchor: string;
    label: string;
    shouldDisplayExternalLabel: boolean;
    isDisabled: boolean;
    isRequired: boolean;
    requiredSign: string;
    hasCompletion: boolean;
    hasMultipleChoice: boolean;
    placeholder: string;
    isPristine: boolean;
    error: string;
    fieldIndex: string;
    values: {
      id: string;
      name: string;
      value: string;
      meta?: (string | number | boolean)[][];
      isDisabled?: boolean;
      isDummy?: boolean;
    }[];
    singleValue: string;
    displayedSingleValue: string;
    selected: string;
    multipleSelected: string[];
    next: string;
    children: string[];
  }>;
  nullReferenceError?: string;
};

type ParamsInitFieldType = {
  fieldAnchor: string;
  label: string;
  shouldDisplayExternalLabel?: boolean;
  isDisabled?: boolean;
  isRequired?: boolean;
  requiredSign?: string;
  canHavePrefix?: boolean;
  index?: number;
  singleValue?: string;
  values?: any[];
  placeholder?: string;
  hasCompletion?: boolean;
  hasMultipleChoice?: boolean;
};

export type {
  FormattedValuesObjectType,
  FormValueSortOptionsType,
  FormValueType,
  FormatValuesType,
  ParamsUpdateFieldDefinitionsType,
  ParamsInitFieldType,
  UpdateValuesType,
  InputValidatorType,
  ValueFormatterType,
  BlurFuncType,
  FocusFuncType,
  SortFuncType,
};
