type ResponseAuthorizeUserInfo = {
  moderator_id: string;
  moderator_name: string;
  moderator_avatar_url: string;
};

export type { ResponseAuthorizeUserInfo };
