type StatDTO = Record<string, { total: number; assigned: number }>;

type StatListDTO = {
  // eslint-disable-next-line id-denylist
  data: Record<string, StatDTO>;
};

export type { StatDTO, StatListDTO };
