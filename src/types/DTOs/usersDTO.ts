import type { AttributeDTO } from "./common";

type BannedUserDTO = {
  task_id: number;
  comment_id: number;
  comment_text: string;
  comment_date: string;
  video_id: string;
  video_title: string;
  video_url: string;
  user_id: number;
  user_name: string;
  user_url: string;
  attributes?: Array<AttributeDTO> | null;
  ban_expire_date: string;
};

type BannedUserListDTO = {
  total_count: number;
  // eslint-disable-next-line id-denylist
  data: BannedUserDTO[];
};

export type { BannedUserDTO, BannedUserListDTO };
