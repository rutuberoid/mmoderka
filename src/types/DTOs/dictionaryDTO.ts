type ReasonDTO = {
  id: number;
  title: string;
};

type ReasonListDTO = {
  // eslint-disable-next-line id-denylist
  data: ReasonDTO[];
};

export type { ReasonDTO, ReasonListDTO };
