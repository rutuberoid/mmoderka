type AttributeDTO = "child_content" | "reported";
type UserDTO = "user" | "moderator" | "automoderator" | "system";
type ErrorDTO = {
  message: string;
  error: string;
};
type SuccessMessageDTO = {
  message: string;
};

type SuccessEmptyMessageDTO = {
  message: "";
};

export type { AttributeDTO, UserDTO, ErrorDTO, SuccessMessageDTO, SuccessEmptyMessageDTO };
