import type { AttributeDTO, UserDTO } from "./common";
type EventDTO = "approve" | "reject" | "delete" | "edit" | "add" | "ban" | "proccesing";

type CommentTaskDTO = {
  task_id: number;
  comment_id: number;
  comment_text: string;
  comment_date: string;
  video_id: string;
  video_title: string;
  video_url: string;
  user_id: number;
  user_name: string;
  user_url: string;
  attributes?: Array<AttributeDTO> | null;
};

type CommentTaskListDTO = {
  total_count: number;
  // eslint-disable-next-line id-denylist
  data: CommentTaskDTO[];
};

type CommentEventDTO = {
  event_date: string;
  event_type: EventDTO;
  event_user_type: UserDTO;
  event_user_id: number;
  event_details?: {
    reasons: Array<number>;
    comment_text: string;
  } | null;
};

type CommentEventListDTO = {
  // eslint-disable-next-line id-denylist
  data: CommentEventDTO[];
};

export type { CommentTaskDTO, CommentEventDTO, EventDTO, CommentTaskListDTO, CommentEventListDTO };
