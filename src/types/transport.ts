import type { ParamsWithErrorOptions } from "./index";

export type HttpErrorType = ParamsWithErrorOptions & { status: number; errorData?: Record<string, unknown> };

export type DisplayedErrorType = Partial<{
  accessError: string;
  permissionError: string;
  badRequestError: string;
  notFoundError: string;
  serverError: string;
  badResponseError: string;
  unknownError: string;
  genericError: string;
  conflictError: string;
}>;

type CommentStatusType = "approve" | "reject";
type CommentAuthorType = "all" | "moderator" | "automoderator";

// services/admin
export type RequestCreateUserType = ParamsWithErrorOptions & {
  userId: number;
};

export type RequestDeleteUserType = ParamsWithErrorOptions & {
  userId: number;
};

// services/auth
export type RequestAuthenticateRedirectType = ParamsWithErrorOptions & {
  callbackPath?: string;
};

export type RequestAuthorizeUserType = ParamsWithErrorOptions;

export type RequestLogoutUserType = ParamsWithErrorOptions;

// services/comments
export type RequestAssignedCommentTasksListType = ParamsWithErrorOptions;
export type RequestCommentTasksListType = ParamsWithErrorOptions & {
  limit?: number;
};
export type RequestApproveCommentTaskListType = ParamsWithErrorOptions & {
  taskId: number;
};
export type RequestRejectCommentTaskListType = ParamsWithErrorOptions & {
  taskId: number;
};
export type RequestCommentTaskHistoryListType = ParamsWithErrorOptions & {
  taskId: number;
};

export type RequestSearchCommentListType = ParamsWithErrorOptions & {
  limit?: number;
  offset?: number;
  status?: CommentStatusType;
  authorId?: number;
  videoId?: number;
  show?: CommentAuthorType;
};

// services/dictionary
export type RequestReasonsListType = ParamsWithErrorOptions;

// services/stat
export type RequestTaskStatsType = ParamsWithErrorOptions;

// services/users
export type RequestBannedUsersListType = ParamsWithErrorOptions & {
  limit?: number;
  offset?: number;
  userId?: number;
  videoId?: number;
};

export type RequestBanUserType = ParamsWithErrorOptions & {
  userId: number;
  reasons: number[];
};
export type RequestUnbanUserType = ParamsWithErrorOptions & {
  userId: number;
};
