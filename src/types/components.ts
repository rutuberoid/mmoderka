import type { TypographyProps } from "@mui/material";
import type { ReactElement, ElementType } from "react";

type NavigationListItemType = { text: string; isActive: boolean };

type RoutingPathNamesType = "home" | "example" | "todos" | "todo";

type TypographyPropsWithComponent = TypographyProps & { component?: ElementType };
type TextsListType = (TypographyPropsWithComponent | ReactElement)[];

type ReactionCodeType = "APPROVAL_MANUAL" | "DELETION_MANUAL" | "BAN_MANUAL" | "UNBAN_MANUAL" | "RESTORE_MANUAL";

export type { NavigationListItemType, RoutingPathNamesType, TextsListType, ReactionCodeType };
