export type EventType =
  | "approve"
  | "reject"
  | "report"
  | "delete"
  | "restore"
  | "edit"
  | "add"
  | "ban"
  | "unban"
  | "assign"
  | "processing";
export type EventUserType = "creator" | "author" | "user" | "moderator" | "automoderator" | "radioControl" | "system";
