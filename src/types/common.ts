export type URLParametersType = Record<string, string>;
export type URLParametersHandlerType = (params?: URLParametersType) => Promise<number>;
export type ParamsWithErrorOptions = {
  errorOptions?: Partial<{
    shouldShowServerError: boolean;
  }>;
};
