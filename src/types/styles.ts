import type { SxProps, Theme } from "@mui/material";

type Styles = Record<string, SxProps<Theme>>;
type CreateStylesFn<T = Record<string, unknown>> = (props?: T) => Styles;
type CreateStylesPropsRequiredFn<T = Record<string, unknown>> = (props: T) => Styles;

export type { Styles, CreateStylesFn, CreateStylesPropsRequiredFn };
