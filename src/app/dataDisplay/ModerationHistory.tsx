import { Typography, Dialog, DialogTitle, DialogContent, IconButton } from "@mui/material";
import { commentTranslations } from "@mimir/constants";
import { Timeline } from "@mui/lab";
import { Close as CloseIcon } from "@mui/icons-material";
import { ModerationHistoryItem } from "@mimir/components";

const ModerationHistory = () => {
  const defaultParams = {
    eventUserId: 231264,
    eventUserName: "Валерий Иванов",
    eventDetails: [],
    eventTime: "11:38",
    eventDate: "21.10.2021",
  };
  const { canShow, closeHistory, historyList } = {
    canShow: true,
    closeHistory: () => undefined,
    historyList: [
      {
        ...defaultParams,
        eventType: "add",
        eventUserType: "author",
      },
      {
        ...defaultParams,
        eventType: "edit",
        eventUserType: "author",
        eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
      },
      {
        ...defaultParams,
        eventType: "delete",
        eventUserType: "author",
      },
      {
        ...defaultParams,
        eventType: "report",
        eventUserType: "user",
        eventUserName: "Маргарита Тихонова",
        eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
      },
      {
        ...defaultParams,
        eventType: "assign",
        eventUserType: "moderator",
      },
      {
        ...defaultParams,
        eventType: "approve",
        eventUserType: "moderator",
      },
      {
        ...defaultParams,
        eventType: "delete",
        eventUserType: "moderator",
      },
      {
        ...defaultParams,
        eventType: "delete",
        eventUserType: "automoderator",
      },
      {
        ...defaultParams,
        eventType: "restore",
        eventUserType: "moderator",
      },
      {
        ...defaultParams,
        eventType: "ban",
        eventUserType: "moderator",
        eventDetails: ["Запись 1", "Запись 2", "Запись 3"],
      },
      {
        ...defaultParams,
        eventType: "unban",
        eventUserType: "moderator",
      },
      {
        ...defaultParams,
        eventType: "delete",
        eventUserType: "radioControl",
      },
      {
        ...defaultParams,
        eventType: "delete",
        eventUserType: "creator",
      },
      {
        ...defaultParams,
        eventType: "restore",
        eventUserType: "creator",
      },
      {
        ...defaultParams,
        eventType: "ban",
        eventUserType: "creator",
      },
      {
        ...defaultParams,
        eventType: "unban",
        eventUserType: "creator",
      },
    ],
  };

  return (
    <Dialog open={canShow} onClose={closeHistory} PaperProps={{ sx: { width: 800, minHeight: 605 } }}>
      <DialogTitle
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-start",
        }}
      >
        <Typography variant="h3" component="span">
          {commentTranslations.titles.historyDialog}
        </Typography>
        <IconButton
          onClick={closeHistory}
          sx={{
            marginTop: "-8px",
            marginRight: "-8px",
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Timeline
          sx={{
            "& .MuiTimelineItem-root:before": {
              flex: 0,
              padding: 0,
            },
            "padding": 0,
            "margin": 0,
          }}
        >
          {historyList.map((item: any, index) => (
            <ModerationHistoryItem
              key={index}
              // eslint-disable-next-line react/jsx-props-no-spreading
              {...item}
              isFirst={index === 0}
              isLast={index === historyList.length - 1}
            />
          ))}
        </Timeline>
      </DialogContent>
    </Dialog>
  );
};

export { ModerationHistory };
