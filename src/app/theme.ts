import { createTheme } from "@mui/material/styles";
import { blue } from "@mui/material/colors";
import { alpha } from "@mui/material";
import type { CSSProperties } from "react";

type CustomValuesType = Record<string, string>;

declare module "@mui/material/styles" {
  // eslint-disable-next-line
  interface Theme {
    custom: {
      appBarHeight: string;
      cardShadow: string;
    };
  }
  // eslint-disable-next-line
  interface ThemeOptions {
    custom: {
      appBarHeight: string;
      cardShadow: string;
    };
  }
  // eslint-disable-next-line
  interface TypographyVariants {
    button1: CSSProperties;
    button2: CSSProperties;
    button3: CSSProperties;
    avatarLetter: CSSProperties;
    inputLabel: CSSProperties;
    helperText: CSSProperties;
    inputText: CSSProperties;
    tooltip: CSSProperties;
  }
  // eslint-disable-next-line
  interface TypographyVariantsOptions {
    button1: CSSProperties;
    button2: CSSProperties;
    button3: CSSProperties;
    avatarLetter: CSSProperties;
    inputLabel: CSSProperties;
    helperText: CSSProperties;
    inputText: CSSProperties;
    tooltip: CSSProperties;
  }
  // eslint-disable-next-line
  interface Palette {
    custom: CustomValuesType;
  }
  // eslint-disable-next-line
  interface PaletteOptions {
    custom: CustomValuesType;
  }
  // eslint-disable-next-line
  interface Shadows {
    custom: CustomValuesType;
  }
  // eslint-disable-next-line
  interface ShadowsOptions {
    custom: CustomValuesType;
  }
}

/* eslint-disable @typescript-eslint/naming-convention */
declare module "@mui/material/Typography" {
  // eslint-disable-next-line
  interface TypographyPropsVariantOverrides {
    button1: true;
    button2: true;
    button3: true;
    avatarLetter: true;
    inputLabel: true;
    helperText: true;
    inputText: true;
    tooltip: true;
  }
}
/* eslint-enable @typescript-eslint/naming-convention */

// default mui fontFamily
const fontFamily = ["Roboto", "Helvetica", "Arial", "sans-serif"].join(",");

export const customTheme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1620,
    },
  },
  shape: {
    borderRadius: 6,
  },
  typography: (palette) => ({
    h1: {
      fontSize: "34px",
      lineHeight: "40px",
      fontWeight: 700,
      letterSpacing: 0,
    },
    h2: {
      fontSize: "24px",
      lineHeight: "28px",
      fontWeight: 700,
      letterSpacing: -0.5,
    },
    h3: {
      fontSize: "20px",
      lineHeight: "23px",
      fontWeight: 600,
      letterSpacing: 0,
    },
    h4: {
      fontSize: "16px",
      lineHeight: "19px",
      fontWeight: 600,
      letterSpacing: 0.25,
    },
    h5: {
      fontSize: "14px",
      lineHeight: "18px",
      fontWeight: 500,
      letterSpacing: 0,
    },
    h6: {
      fontSize: "12px",
      lineHeight: "19px",
      fontWeight: 500,
      letterSpacing: 0.15,
    },
    subtitle1: {
      fontSize: "14px",
      lineHeight: "24px",
      fontWeight: 500,
      letterSpacing: 0.15,
    },
    subtitle2: {
      fontSize: "12px",
      lineHeight: "18px",
      fontWeight: 400,
      color: palette.grey[600],
      letterSpacing: 0.1,
    },
    body1: {
      fontSize: "14px",
      lineHeight: "19px",
      fontWeight: 400,
      letterSpacing: 0.15,
    },
    body2: {
      fontSize: "14px",
      lineHeight: "24px",
      fontWeight: 400,
      letterSpacing: 0.15,
    },
    button1: {
      fontFamily,
      fontSize: "15px",
      lineHeight: "26px",
      fontWeight: 500,
      textTransform: "uppercase",
      letterSpacing: 0.46,
    },
    button2: {
      fontFamily,
      fontSize: "14px",
      lineHeight: "24px",
      fontWeight: 500,
      letterSpacing: 0.4,
    },
    button3: {
      fontFamily,
      fontSize: "13px",
      lineHeight: "22px",
      fontWeight: 500,
      textTransform: "uppercase",
      letterSpacing: 0.46,
    },
    caption: {
      lineHeight: "20px",
      letterSpacing: 0.4,
    },
    overline: {
      lineHeight: "31px",
      letterSpacing: 1,
    },
    avatarLetter: {
      fontFamily,
      fontSize: "20px",
      lineHeight: "20px",
      fontWeight: 400,
      letterSpacing: 0.14,
    },
    inputLabel: {
      fontFamily,
      fontSize: "12px",
      lineHeight: "12px",
      fontWeight: 400,
      letterSpacing: 0.15,
    },
    helperText: {
      fontFamily,
      fontSize: "12px",
      lineHeight: "20px",
      fontWeight: 400,
      letterSpacing: 0.4,
    },
    inputText: {
      fontFamily,
      fontSize: "14px",
      lineHeight: "20px",
      fontWeight: 500,
      letterSpacing: 0.15,
    },
    tooltip: {
      fontFamily,
      fontSize: "10px",
      lineHeight: "14px",
      fontWeight: 500,
      letterSpacing: 0,
    },
  }),
  palette: {
    custom: {
      grey350: "#c4c4c4",
      blue8p: alpha(blue[700], 0.08),
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: ({ ownerState, theme }) => ({
          fontSize: theme.typography.button2.fontSize,
          fontWeight: theme.typography.button2.fontWeight,
          lineHeight: theme.typography.button2.lineHeight,
          letterSpacing: theme.typography.button2.letterSpacing,
          ...(ownerState.size === "medium" && {
            "textTransform": "initial",
            "&:first-letter": {
              textTransform: "uppercase",
            },
          }),
          ...(ownerState.variant === "contained" &&
            ownerState.size === "medium" && {
              "boxShadow": "none",
              "&:hover": {
                boxShadow: "none",
              },
              "&:active": {
                boxShadow: "none",
              },
            }),
          ...(ownerState.variant === "outlined" &&
            ownerState.color === "inherit" && {
              borderColor: theme.palette.custom.grey350,
            }),
        }),
      },
    },
    MuiContainer: {
      styleOverrides: {
        maxWidthXl: {
          "@media (min-width: 1620px)": {
            maxWidth: 1332,
          },
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: "24px",
        },
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          padding: "24px",
        },
      },
    },
    MuiDialogTitle: {
      styleOverrides: {
        root: {
          padding: "24px",
        },
      },
    },
    MuiDialogActions: {
      styleOverrides: {
        root: {
          padding: "24px",
        },
      },
    },
    MuiFormControlLabel: {
      defaultProps: {
        componentsProps: {
          typography: {
            variant: "button2",
          },
        },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: ({ theme }) => ({
          "fontSize": theme.typography.tooltip.fontSize,
          "lineHeight": theme.typography.tooltip.lineHeight,
          "fontWeight": theme.typography.tooltip.fontWeight,
          "letterSpacing": theme.typography.tooltip.letterSpacing,

          "&&.MuiTooltip-tooltipPlacementBottom": {
            marginTop: "4px",
          },
          "&&.MuiTooltip-tooltipPlacementTop": {
            marginBottom: "4px",
          },
          "&&.MuiTooltip-tooltipPlacementLeft": {
            marginRight: "4px",
          },
          "&&.MuiTooltip-tooltipPlacementRight": {
            marginLeft: "4px",
          },
        }),
      },
    },
    MuiMenu: {
      styleOverrides: {
        root: ({ theme }) => ({
          ".MuiMenuItem-root + .MuiMenuItem-root": {
            marginTop: theme.spacing(0.5),
          },
        }),
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: ({ theme }) => ({
          margin: "0 6px",
          borderRadius: theme.shape.borderRadius,
          fontSize: theme.typography.subtitle1.fontSize,
          lineHeight: theme.typography.subtitle1.lineHeight,
          fontWeight: theme.typography.subtitle1.fontWeight,
          letterSpacing: theme.typography.subtitle1.letterSpacing,
        }),
      },
    },
    MuiModal: {
      styleOverrides: {
        root: {
          padding: "24px 32px",
        },
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: {
          "&:active": {
            color: blue[700],
          },
        },
      },
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: ({ theme }) => ({
          color: theme.palette.grey[900],
        }),
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fontSize: "14px",
          fontWeight: 500,
        },
      },
    },
  },
  custom: {
    appBarHeight: "64px",
    cardShadow: "0px 1px 8px rgba(0, 0, 0, 0.1)",
  },
});
