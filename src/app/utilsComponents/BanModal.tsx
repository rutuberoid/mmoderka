import React from "react";
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  Link,
  Typography,
} from "@mui/material";
import { Close } from "@mui/icons-material";
import { ProtoField } from "@mimir/components";
import { commentTranslations, formTranslations } from "@mimir/constants";

const BanModal = () => {
  const {
    creators,
    canOpen,
    hasFields,
    fieldIds,
    changeField,
    fieldGroup,
    applyBan,
    cancelBan,
    isForeverBan,
    toggleForeverBan,
    daysOfBan,
    isActionDisabled,
  } = {
    creators: [
      {
        creatorName: "Сергей Бодров",
        creatorChannelUrl: "#1",
      },
      {
        creatorName: "Олег Бобров",
        creatorChannelUrl: "#2",
      },
    ],
    canOpen: true,
    hasFields: false,
    fieldIds: ["fieldId1"],
    changeField: () => {},
    fieldGroup: new Map(),
    applyBan: () => {},
    cancelBan: () => {},
    isForeverBan: true,
    toggleForeverBan: () => {},
    daysOfBan: 30,
    isActionDisabled: false,
  };

  return (
    <Dialog
      open={canOpen}
      PaperProps={{
        sx: { width: 480, minHeight: 478 },
      }}
      onClose={cancelBan}
    >
      <DialogTitle
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-start",
        }}
      >
        <Box>
          <Typography
            variant={"h3"}
            sx={{
              marginBottom: 0.5,
            }}
          >
            {commentTranslations.titles.banAuthor(creators.length)}
          </Typography>
          <Box>
            {creators.map(({ creatorName, creatorChannelUrl }, index, array) => (
              <Typography key={creatorName} variant={"h3"} component={"span"}>
                <Link
                  href={creatorChannelUrl}
                  target={"_blank"}
                  rel={"noopener noreferrer"}
                  variant={"h3"}
                  sx={{
                    textDecoration: "none",
                  }}
                >
                  {creatorName}
                </Link>
                {index !== array.length - 1 && ", "}
              </Typography>
            ))}
          </Box>
        </Box>
        <IconButton
          size={"small"}
          sx={{
            marginTop: "-5px",
            marginRight: "-5px",
          }}
          onClick={cancelBan}
        >
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent
        sx={{
          overflowY: "unset",
        }}
      >
        {hasFields &&
          fieldIds.map(([id]) => <ProtoField field={fieldGroup.get(id)} key={id} changeField={changeField} hasChips />)}
      </DialogContent>
      <DialogActions
        sx={{
          justifyContent: "space-between",
        }}
      >
        <FormControlLabel
          control={<Checkbox disabled={isActionDisabled} onClick={toggleForeverBan} checked={isForeverBan} />}
          label={formTranslations.labels.FOREVER}
        />
        <Button disabled={isActionDisabled} onClick={applyBan} variant={"contained"}>
          {isForeverBan ? formTranslations.actions.BAN_FOREVER : formTranslations.labels.banDuration(daysOfBan)}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export { BanModal };
