import React from "react";
import type { FC, ReactElement, ReactNode } from "react";
import type { RootStoreModel } from "@mimir/store";
import type { RoutingPathNamesType } from "@mimir/types";
import { observer } from "mobx-react-lite";
import { Box } from "@mui/material";
import { commonTranslations } from "@mimir/constants";
import { useInject } from "@mimir/store";
import { TodoItem } from "@mimir/components";

type StateRouterProps = {
  spinner?: ReactNode;
};

const mapStore = (root: RootStoreModel) => ({
  isLoading: root.routerStore.isLoading,
  currentView: root.routerStore.currentView,
});

const VIEWS_MAP: Record<RoutingPathNamesType, ReactElement> = {
  home: <div>Home</div>,
  example: <div>Example</div>,
  todos: <div>Todos</div>,
  todo: <TodoItem todoId="" />,
};

export const StateRouter: FC<StateRouterProps> = observer(({ spinner }) => {
  const { isLoading, currentView } = useInject(mapStore);

  if (isLoading) {
    return spinner ? <Box>{spinner}</Box> : <span>loading...</span>;
  }

  return (
    <div>
      {currentView
        ? React.cloneElement(VIEWS_MAP[currentView.name as RoutingPathNamesType], currentView.routingProps)
        : commonTranslations.deadEnd.NO_COMPONENT}
    </div>
  );
});
