import type { FC } from "react";
import React from "react";
import { Button, ButtonBase, Card, CardContent, Menu, MenuItem, Stack, Typography } from "@mui/material";
import { InlineMessage } from "@mimir/components";
import { AutorenewOutlined, ExpandMoreOutlined } from "@mui/icons-material";
import { commentTranslations, commonTranslations } from "@mimir/constants";

import { EmptyIcon } from "../../components/assets/EmptyIcon";

const CommentsBlankCard: FC = () => {
  const { loadComments, changeCommentsAmount, commentsAmount, selectedAmount } = {
    loadComments: () => undefined,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    changeCommentsAmount: (amount: number) => undefined,
    commentsAmount: [5, 10, 15],
    selectedAmount: 5,
  };

  const [menuAnchorEl, setMenuAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleAmountChange = (amount: number) => {
    handleMenuClose();
    changeCommentsAmount(amount);
  };

  return (
    <>
      <Card
        elevation={0}
        sx={{
          boxShadow: (theme) => theme.custom.cardShadow,
          borderRadius: 2,
        }}
      >
        <CardContent>
          <Stack spacing={3} alignItems={"center"}>
            <EmptyIcon />
            <Stack spacing={2} alignItems={"center"}>
              <InlineMessage
                textsList={[
                  {
                    variant: "body1",
                    children: commentTranslations.hints.NO_COMMENTS,
                  },
                ]}
                endIcon={
                  <ButtonBase
                    sx={{ color: (theme) => theme.palette.primary.main }}
                    onClick={handleMenuOpen}
                    disableRipple
                  >
                    <Typography variant={"body1"}>
                      {selectedAmount} {commentTranslations.labels.COMMENT_AMOUNT}
                    </Typography>
                    <ExpandMoreOutlined />
                  </ButtonBase>
                }
              />
              <Button
                startIcon={<AutorenewOutlined />}
                onClick={() => loadComments()}
                variant={"outlined"}
                color={"inherit"}
              >
                {commonTranslations.actions.LOAD}
              </Button>
            </Stack>
            <Typography variant={"subtitle2"}>{commentTranslations.hints.INSULT}</Typography>
          </Stack>
        </CardContent>
      </Card>
      <Menu
        open={!!menuAnchorEl}
        anchorEl={menuAnchorEl}
        onClose={handleMenuClose}
        anchorOrigin={{
          horizontal: "left",
          vertical: "bottom",
        }}
        transformOrigin={{
          horizontal: 0,
          vertical: 0,
        }}
      >
        {commentsAmount.map((amount) => (
          <MenuItem key={amount} onClick={() => handleAmountChange(amount)}>
            {amount} {commentTranslations.labels.COMMENT_AMOUNT}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export { CommentsBlankCard };
