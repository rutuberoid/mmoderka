import React from "react";
import { AppBar, Avatar, Button, IconButton, ListItemIcon, Menu, MenuItem, Toolbar, Typography } from "@mui/material";
import {
  Menu as MenuIcon,
  ExpandMore as ExpandMoreIcon,
  SettingsOutlined as SettingsOutlinedIcon,
  Logout as LogoutIcon,
} from "@mui/icons-material";
import { Logo } from "@mimir/components";
import { commonTranslations } from "@mimir/constants";

type PageToolbarProps = {
  toggleNavMenu: () => void;
};

// TODO: wrap component into observer and connect with store
// TODO: make examples with store in storybook
const PageToolbar = ({ toggleNavMenu }: PageToolbarProps) => {
  const { moderatorName, avatarUrl, logout, openSettings } = {
    moderatorName: "TooLongNameForAUsualPerson VeryLongLastName",
    avatarUrl: "https://static.rutube.ru/static/img/_thumb_default_03.png?size=s",
    logout: () => undefined,
    openSettings: () => undefined,
  };

  const [menuAnchorEl, setMenuAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchorEl(event.currentTarget);
    openSettings();
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleSettingsClick = () => {
    handleMenuClose();
    openSettings();
  };

  const handleLogoutClick = () => {
    handleMenuClose();
    logout();
  };

  return (
    <AppBar position={"fixed"} color={"inherit"} elevation={0}>
      <Toolbar>
        <IconButton onClick={toggleNavMenu}>
          <MenuIcon />
        </IconButton>
        <IconButton
          sx={{
            "&:hover": {
              backgroundColor: "transparent",
            },
          }}
          disableRipple
        >
          <Logo />
        </IconButton>
        <Button
          sx={{
            marginLeft: "auto",
            maxWidth: 300,
          }}
          onClick={handleMenuOpen}
          startIcon={<Avatar alt={moderatorName} src={avatarUrl} />}
          endIcon={
            <ExpandMoreIcon
              sx={{
                color: (theme) => theme.palette.grey[600],
              }}
            />
          }
          disableRipple
        >
          <Typography
            sx={{
              color: (theme) => theme.palette.grey[900],
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
            variant={"button2"}
            component="span"
          >
            {moderatorName}
          </Typography>
        </Button>
        <Menu
          open={!!menuAnchorEl}
          onClose={handleMenuClose}
          anchorEl={menuAnchorEl}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          transformOrigin={{
            vertical: -8,
            horizontal: "right",
          }}
          MenuListProps={{
            sx: {
              width: "320px",
            },
          }}
        >
          <MenuItem onClick={handleSettingsClick}>
            <ListItemIcon>
              <SettingsOutlinedIcon />
            </ListItemIcon>
            {commonTranslations.actions.OPEN_MENU_SETTINGS}
          </MenuItem>
          <MenuItem onClick={handleLogoutClick}>
            <ListItemIcon>
              <LogoutIcon />
            </ListItemIcon>
            {commonTranslations.actions.LOGOUT}
          </MenuItem>
        </Menu>
      </Toolbar>
    </AppBar>
  );
};

export { PageToolbar };
