import type { RootStoreModel } from "@mimir/store";
import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import { PageLayout, Navigation } from "@mimir/components";
import { useInject } from "@mimir/store";

import { CommentsBlankCard, PageToolbar } from "./surface";

const mapStore = (_root: RootStoreModel) => ({});

const App = observer(() => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const _store = useInject(mapStore);
  // eslint-disable-next-line
  const [shouldExpandNavMenu, toggleNavMenu] = useState(true);
  return (
    <PageLayout
      pageHeader={<PageToolbar toggleNavMenu={() => toggleNavMenu((prevState) => !prevState)} />}
      navPanel={<Navigation isWide={shouldExpandNavMenu} />}
      content={<CommentsBlankCard />}
    />
  );
});

export { App };
