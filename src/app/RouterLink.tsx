import React from "react";
import type { FC, ReactNode } from "react";
import { Link } from "@mui/material";
import type { RootStoreModel } from "@mimir/store";
import type { URLParametersType, RoutingPathNamesType } from "@mimir/types";
import { observer } from "mobx-react-lite";
import { useInject } from "@mimir/store";

type LinkProps = {
  viewName: RoutingPathNamesType;
  children: ReactNode;
  params?: URLParametersType;
};

const mapStore = (root: RootStoreModel) => ({
  setView: root.routerStore.setView,
  getViewByName: root.routerStore.getViewByName,
});

const RouterLink: FC<LinkProps> = observer(({ viewName, params, children }) => {
  const { setView, getViewByName } = useInject(mapStore);
  const clickLink = () => {
    setView(viewName, params);
  };
  const view = getViewByName(viewName);

  return view ? (
    <Link component="button" href={view.expandParameters(params)} onClick={clickLink}>
      {children}
    </Link>
  ) : null;
});

export { RouterLink };
