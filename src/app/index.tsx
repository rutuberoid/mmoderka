import "../../wdyr";
import React, { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import { StoreProvider } from "@mimir/store";

import { customTheme } from "./theme";
import { App } from "./App";

import { worker } from "../mocks/browser";

if (SHOULD_MOCK_API) {
  void worker.start();
}

const root = createRoot(document.getElementById("root") as Element);
root.render(
  <StoreProvider>
    <ThemeProvider theme={customTheme}>
      <StrictMode>
        <CssBaseline />
        <App />
      </StrictMode>
    </ThemeProvider>
  </StoreProvider>
);
