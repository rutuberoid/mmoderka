import { revealHttpError, transport } from "@mimir/utils";
import type { RequestReasonsListType, ReasonListDTO } from "@mimir/types";
import type { AxiosResponse } from "axios";

const getReasonsList = async (params: RequestReasonsListType = {}) => {
  const { errorOptions } = params;
  try {
    const { data }: AxiosResponse<ReasonListDTO> = await transport({
      method: "GET",
      url: "/task/reasons",
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export { getReasonsList };
