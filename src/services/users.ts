import { revealHttpError, transport } from "@mimir/utils";
import type {
  RequestBannedUsersListType,
  RequestBanUserType,
  RequestUnbanUserType,
  BannedUserListDTO,
  SuccessMessageDTO,
} from "@mimir/types";
import type { AxiosResponse } from "axios";

const getBannedUsersList = async (params: RequestBannedUsersListType = {}) => {
  const { limit, offset, userId, videoId, errorOptions } = params;
  try {
    const { data }: AxiosResponse<BannedUserListDTO> = await transport({
      method: "GET",
      url: "/user",
      params: {
        // eslint-disable-next-line camelcase
        user_id: userId,
        // eslint-disable-next-line camelcase
        video_id: videoId,
        limit,
        offset,
      },
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postBanUser = async ({ userId, reasons, errorOptions }: RequestBanUserType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: `/user/${userId}/ban`,
      data: reasons,
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postUnbanUser = async ({ userId, errorOptions }: RequestUnbanUserType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: `/user/${userId}/unban`,
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export { getBannedUsersList, postBanUser, postUnbanUser };
