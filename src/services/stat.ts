import { revealHttpError, transport } from "@mimir/utils";
import type { RequestTaskStatsType, StatListDTO } from "@mimir/types";
import type { AxiosResponse } from "axios";

const getTaskStatsList = async (params: RequestTaskStatsType = {}) => {
  const { errorOptions } = params;
  try {
    const { data }: AxiosResponse<StatListDTO> = await transport({
      method: "GET",
      url: "/task/stats",
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export { getTaskStatsList };
