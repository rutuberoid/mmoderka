import { revealHttpError, transport } from "@mimir/utils";
import type { RequestCreateUserType, RequestDeleteUserType, SuccessMessageDTO } from "@mimir/types";
import type { AxiosResponse } from "axios";

const postCreateUser = async ({ userId, errorOptions }: RequestCreateUserType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: "/roles",
      data: {
        // eslint-disable-next-line camelcase
        user_id: userId,
      },
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const deleteUser = async ({ userId, errorOptions }: RequestDeleteUserType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "DELETE",
      url: `/roles/${userId}`,
    });
    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export { postCreateUser, deleteUser };
