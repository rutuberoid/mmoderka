import { revealHttpError, transport } from "@mimir/utils";
import type {
  RequestAuthorizeUserType,
  RequestLogoutUserType,
  ResponseAuthorizeUserInfo,
  SuccessMessageDTO,
} from "@mimir/types";
import type { AxiosResponse } from "axios";

const getAuthorizeUserInfo = async ({ errorOptions }: RequestAuthorizeUserType = {}) => {
  try {
    const { data }: AxiosResponse<ResponseAuthorizeUserInfo> = await transport({
      method: "GET",
      url: "/login",
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postLogoutUser = async (params: RequestLogoutUserType = {}) => {
  const { errorOptions } = params;
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: "/logout",
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export { getAuthorizeUserInfo, postLogoutUser };
