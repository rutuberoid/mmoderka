import { revealHttpError, transport } from "@mimir/utils";
import type {
  RequestCommentTaskHistoryListType,
  RequestAssignedCommentTasksListType,
  RequestSearchCommentListType,
  RequestApproveCommentTaskListType,
  RequestCommentTasksListType,
  RequestRejectCommentTaskListType,
  CommentTaskListDTO,
  SuccessMessageDTO,
  CommentEventListDTO,
} from "@mimir/types";
import type { AxiosResponse } from "axios";

const getCommentTasksList = async (params: RequestAssignedCommentTasksListType = {}) => {
  const { errorOptions } = params;
  try {
    const { data }: AxiosResponse<CommentTaskListDTO> = await transport({
      method: "GET",
      url: "/task/comment",
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postCommentTasksList = async ({ limit, errorOptions }: RequestCommentTasksListType) => {
  try {
    const { data }: AxiosResponse<CommentTaskListDTO> = await transport({
      method: "POST",
      url: "/task/comment",
      params: {
        limit,
      },
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postApproveCommentTask = async ({ taskId, errorOptions }: RequestApproveCommentTaskListType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: `/task/comment/${taskId}/approve`,
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const postRejectCommentTask = async ({ taskId, errorOptions }: RequestRejectCommentTaskListType) => {
  try {
    const { data }: AxiosResponse<SuccessMessageDTO> = await transport({
      method: "POST",
      url: `/task/comment/${taskId}/reject`,
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const getCommentTaskHistoryList = async ({ taskId, errorOptions }: RequestCommentTaskHistoryListType) => {
  try {
    const { data }: AxiosResponse<CommentEventListDTO> = await transport({
      method: "GET",
      url: `/task/comment/${taskId}/history`,
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

const getSearchCommentTasksList = async (params: RequestSearchCommentListType = {}) => {
  const { limit, offset, status, authorId, videoId, show, errorOptions } = params;
  try {
    const { data }: AxiosResponse<CommentTaskListDTO> = await transport({
      method: "GET",
      url: "/task/comment/search",
      params: {
        // eslint-disable-next-line camelcase
        author_id: authorId,
        // eslint-disable-next-line camelcase
        video_id: videoId,
        limit,
        offset,
        status,
        show,
      },
    });

    return data;
  } catch (error) {
    throw revealHttpError({ ...error, errorOptions });
  }
};

export {
  getCommentTasksList,
  postCommentTasksList,
  postApproveCommentTask,
  postRejectCommentTask,
  getCommentTaskHistoryList,
  getSearchCommentTasksList,
};
