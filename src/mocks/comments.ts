/* eslint-disable camelcase, id-denylist, @typescript-eslint/no-unnecessary-condition */
import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genResponse, genResponseWithError, genUnauthorizedResponse, isUserAuthenticated } from "./utils";
import { COMMENT_ITEM, HISTORY_ITEM, RESPONSE_SUCCESS_MESSAGE } from "./DTOs";

const commentsHandlers = [
  // Get tasks assigned to moderator
  rest.get(`${baseURLTest}/task/comment`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 200, {
      total_count: 1,
      data: [COMMENT_ITEM],
    });
  }),
  // Assign tasks to moderator
  rest.post(`${baseURLTest}/task/comment`, (req, res, ctx) => {
    const limit = Number(req.url.searchParams.get("limit")) || 10;
    const hasModeratorTasks = false;
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    if (hasModeratorTasks) {
      return genResponseWithError(res, ctx, 405, "There are already assigned tasks on this moderator");
    }

    return genResponse(res, ctx, 200, {
      total_count: limit,
      data: [...new Array(limit)].map(() => COMMENT_ITEM),
    });
  }),
  // Approve comment
  rest.post(`${baseURLTest}/task/comment/:task_id/approve`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();
    const isTaskExist = true;
    const isTaskApprovable = true;

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    if (!isTaskApprovable) {
      return genResponseWithError(
        res,
        ctx,
        400,
        "Cannot approve comment because of status or it is assigned to another moderator"
      );
    }

    if (!isTaskExist) {
      return genResponseWithError(res, ctx, 404, "Task with this task_id is not found");
    }

    return genResponse(res, ctx, 202, RESPONSE_SUCCESS_MESSAGE);
  }),
  // Reject comment
  rest.post(`${baseURLTest}/task/comment/:task_id/reject`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();
    const isTaskExist = true;
    const isTaskApprovable = true;

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    if (!isTaskApprovable) {
      return genResponseWithError(
        res,
        ctx,
        400,
        "Cannot reject comment because of status or it is assigned to another moderator"
      );
    }

    if (!isTaskExist) {
      return genResponseWithError(res, ctx, 404, "Task with this task_id is not found");
    }

    return genResponse(res, ctx, 202, RESPONSE_SUCCESS_MESSAGE);
  }),
  // Comment history
  rest.get(`${baseURLTest}/task/comment/:task_id/history`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();
    const isTaskExist = true;

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    if (!isTaskExist) {
      return genResponseWithError(res, ctx, 404, "Task with this task_id is not found");
    }

    return genResponse(res, ctx, 202, {
      data: [HISTORY_ITEM],
    });
  }),
  // Search comments with params: limit, offset, status, author_id, video_id, show
  rest.get(`${baseURLTest}/task/comment/search`, (req, res, ctx) => {
    const limit = Number(req.url.searchParams.get("limit")) || 10;
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 200, {
      total_count: limit,
      data: [...new Array(limit)].map(() => COMMENT_ITEM),
    });
  }),
];

export { commentsHandlers };
