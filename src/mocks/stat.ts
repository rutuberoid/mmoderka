import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genResponse, genUnauthorizedResponse, isUserAuthenticated } from "./utils";
import { TASKS_STATS } from "./DTOs";

const statHandlers = [
  // Get open tasks and tasks in work for every type of content
  rest.get(`${baseURLTest}/task/stats`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 200, TASKS_STATS);
  }),
];

export { statHandlers };
