/* eslint-disable id-denylist, camelcase */
import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genResponse, genResponseWithError, genUnauthorizedResponse, isUserAuthenticated } from "./utils";
import { BANNED_USER_ITEM, RESPONSE_SUCCESS_MESSAGE } from "./DTOs";

const userHandlers = [
  // Get list of banned users with filters: limit, offset, user_id, video_id
  rest.get(`${baseURLTest}/user`, (req, res, ctx) => {
    const limit = Number(req.url.searchParams.get("limit")) || 10;
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 200, {
      total_count: limit,
      data: [...new Array(limit)].map(() => BANNED_USER_ITEM),
    });
  }),
  // Ban user
  rest.post(`${baseURLTest}/user/:userId/ban`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();
    const isInvalidBanReasons = Array.isArray(req.body) && !req.body.length;

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (isInvalidBanReasons) {
      return genResponseWithError(res, ctx, 400, "Incorrect reason ban list");
    }

    return genResponse(res, ctx, 202, RESPONSE_SUCCESS_MESSAGE);
  }),
  // Unban user
  rest.post(`${baseURLTest}/user/:userId/unban`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 202, RESPONSE_SUCCESS_MESSAGE);
  }),
];

export { userHandlers };
