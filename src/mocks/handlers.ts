import type { DefaultBodyType, MockedRequest, RestHandler } from "msw";

import { adminHandlers } from "./admin";
import { authHandlers } from "./auth";
import { commentsHandlers } from "./comments";
import { dictionaryHandlers } from "./dictionary";
import { statHandlers } from "./stat";
import { userHandlers } from "./users";

const handlers = ([] as RestHandler<MockedRequest<DefaultBodyType>>[]).concat(
  adminHandlers,
  authHandlers,
  commentsHandlers,
  dictionaryHandlers,
  statHandlers,
  userHandlers
);

export { handlers };
