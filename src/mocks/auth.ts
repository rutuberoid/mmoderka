import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genUnauthorizedResponse, isUserAuthenticated, genResponse, userLogin, userLogout } from "./utils";
import { RESPONSE_SUCCESS_MESSAGE, AUTHORIZED_USER_INFO } from "./DTOs";

const authHandlers = [
  rest.get(`${baseURLTest}/login`, (req, res, ctx) => {
    userLogin();

    if (!isUserAuthenticated()) {
      return genResponse(res, ctx, 401);
    }

    return genResponse(res, ctx, 200, AUTHORIZED_USER_INFO);
  }),

  rest.post(`${baseURLTest}/logout`, (_, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    userLogout();

    return genResponse(res, ctx, 200, RESPONSE_SUCCESS_MESSAGE);
  }),
];

export { authHandlers };
