import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genResponse, genResponseWithError, genUnauthorizedResponse, isUserAuthenticated } from "./utils";
import { RESPONSE_SUCCESS_MESSAGE } from "./DTOs";

const adminHandlers = [
  // Create new user in moderation
  rest.post(`${baseURLTest}/roles`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 201, RESPONSE_SUCCESS_MESSAGE);
  }),
  // Delete user
  rest.delete(`${baseURLTest}/roles/:userId`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();
    const isUserExists = true;

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (!isUserExists) {
      return genResponseWithError(res, ctx, 404, "User with this user_id is not found");
    }

    return genResponse(res, ctx, 202, RESPONSE_SUCCESS_MESSAGE);
  }),
];

export { adminHandlers };
