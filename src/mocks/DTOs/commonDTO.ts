import type { SuccessMessageDTO } from "@mimir/types";

const RESPONSE_SUCCESS_MESSAGE: SuccessMessageDTO = {
  message: "Success",
};

export { RESPONSE_SUCCESS_MESSAGE };
