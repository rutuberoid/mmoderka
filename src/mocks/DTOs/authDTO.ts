/* eslint-disable camelcase */
import type { ResponseAuthorizeUserInfo } from "@mimir/types";

const AUTHORIZED_USER_INFO: ResponseAuthorizeUserInfo = {
  moderator_id: "123",
  moderator_name: "Tester",
  moderator_avatar_url: "https://test.url",
};

export { AUTHORIZED_USER_INFO };
