/* eslint-disable camelcase */

import type { CommentEventDTO, CommentTaskDTO } from "@mimir/types";

const COMMENT_ITEM: CommentTaskDTO = {
  task_id: 1,
  comment_id: 1,
  comment_text: "sometimes by accident, sometimes on purpose",
  comment_date: "2022-08-01T11:01:35.903Z",
  video_id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  video_title: "Where can I get some",
  video_url: "https://rutube.dev/video/004230c90b77b9728f2e7d3714f2fd52/",
  user_id: 24128704,
  user_name: "Uranus3030",
  user_url: "https://rutube.dev/channel/24128704/",
  attributes: ["child_content"],
};

const HISTORY_ITEM: CommentEventDTO = {
  event_date: "2022-08-01T12:50:04.105Z",
  event_type: "approve",
  event_user_type: "user",
  event_user_id: 0,
  event_details: {
    reasons: [38],
    comment_text: "joe biden wake up",
  },
};

export { COMMENT_ITEM, HISTORY_ITEM };
