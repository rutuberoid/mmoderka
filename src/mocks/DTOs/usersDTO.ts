/* eslint-disable camelcase */

import type { BannedUserDTO } from "@mimir/types";

const BANNED_USER_ITEM: BannedUserDTO = {
  task_id: 0,
  comment_id: 0,
  comment_text: "string",
  comment_date: "2022-08-02T05:01:36.126Z",
  video_id: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  video_title: "string",
  video_url: "string",
  user_id: 0,
  user_name: "string",
  user_url: "string",
  attributes: ["child_content"],
  ban_expire_date: "2022-08-02T05:01:36.126Z",
};

export { BANNED_USER_ITEM };
