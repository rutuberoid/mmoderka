/* eslint-disable camelcase, @typescript-eslint/naming-convention */

import type { ReasonDTO } from "@mimir/types";

const ACTION_REASONS: ReasonDTO[] = [
  {
    id: 1,
    title: "user_deactivated",
  },
  {
    id: 2,
    title: "user_banned",
  },
  {
    id: 3,
    title: "user_deleted_video",
  },
  {
    id: 4,
    title: "moder_deleted_video",
  },
  {
    id: 5,
    title: "new_video",
  },
  {
    id: 6,
    title: "error_upload_video",
  },
  {
    id: 7,
    title: "illegal_content",
  },
  {
    id: 8,
    title: "delete_by_rightward",
  },
  {
    id: 9,
    title: "user_locked",
  },
  {
    id: 10,
    title: "default_deleted_video",
  },
  {
    id: 11,
    title: "default_does_not_exists_video",
  },
  {
    id: 12,
    title: "default_hidden_video",
  },
  {
    id: 13,
    title: "custom_restrict",
  },
  {
    id: 14,
    title: "special",
  },
  {
    id: 15,
    title: "converting_video",
  },
  {
    id: 16,
    title: "downloading_video",
  },
  {
    id: 17,
    title: "delete_rockbound",
  },
  {
    id: 20,
    title: "recording_video",
  },
  {
    id: 21,
    title: "\u0423\u0434\u0430\u043b\u0435\u043d\u043e \u043a\u0430\u043a \u0421\u041f\u0410\u041c",
  },
  {
    id: 22,
    title: "recording_video_error",
  },
  {
    id: 23,
    title: "\u041c\u0430\u0436\u043e\u0440: \u0441\u0435\u0440\u0438\u044f 3",
  },
  {
    id: 24,
    title: "purchase_paid",
  },
  {
    id: 25,
    title: "purchase_failed",
  },
  {
    id: 26,
    title: "purchase_canceled",
  },
  {
    id: 27,
    title: "wait_livestream",
  },
  {
    id: 28,
    title: "stop_livestream",
  },
  {
    id: 29,
    title: "purchase_prepayments",
  },
  {
    id: 30,
    title: "user_inactive",
  },
  {
    id: 31,
    title: "\u043c\u0435\u043c\u043e\u0440\u0430\u043d\u0434\u0443\u043c",
  },
  {
    id: 32,
    title: "moderation",
  },
  {
    id: 33,
    title: "checking rules",
  },
  {
    id: 34,
    title: "illegal_check",
  },
  {
    id: 35,
    title: "live_without_live",
  },
  {
    id: 36,
    title: "source_deactivated",
  },
  {
    id: 37,
    title: "comm1",
  },
  {
    id: 38,
    title: "government_request",
  },
  {
    id: 39,
    title: "copyright_abuse",
  },
  {
    id: 40,
    title: "absence_video",
  },
  {
    id: 41,
    title: "gambling_abuse",
  },
  {
    id: 42,
    title: "meta",
  },
  {
    id: 43,
    title: "shocking_content",
  },
  {
    id: 44,
    title: "financial_abuse",
  },
];

export { ACTION_REASONS };
