import type { StatDTO } from "@mimir/types";

const TASKS_STATS: StatDTO = {
  comments: {
    total: 0,
    assigned: 0,
  },
};

export { TASKS_STATS };
