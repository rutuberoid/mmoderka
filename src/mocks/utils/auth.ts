import { store } from "@mswjs/cookies";
import { baseURLTest } from "@mimir/utils";

const userLogin = () => {
  if (process.env.NODE_ENV === "development") {
    return (document.cookie = "m-jwt=encrypted-code");
  }

  const request = new Request(baseURLTest!);
  const response = new Response(null, {
    headers: new Headers({ "set-cookie": "m-jwt=encrypted-code" }),
  });

  return store.add(request, response);
};

const userLogout = () => {
  if (process.env.NODE_ENV === "development") {
    return (document.cookie = document.cookie.replace(/m-jwt=[^;]*;?/, ""));
  }

  return store.clear();
};

const isUserAuthenticated = () => {
  if (process.env.NODE_ENV === "development") {
    return document.cookie.includes("m-jwt");
  }

  const request = new Request(baseURLTest!);
  return !!Array.from(store.get(request)).length;
};

export { userLogin, userLogout, isUserAuthenticated };
