import type { DefaultBodyType, MockedResponse, RestContext, ResponseComposition } from "msw";

const redirect = (destination: string, statusCode: number) => (res: MockedResponse<DefaultBodyType>) => {
  res.status = statusCode;
  res.headers.set("Location", destination);
  return res;
};

const genResponse = (
  res: ResponseComposition<DefaultBodyType>,
  ctx: RestContext,
  statusCode: number,
  answer?: string | Record<string, any>
) => res(ctx.status(statusCode), ctx.json(answer));

const genResponseWithError = (
  res: ResponseComposition<DefaultBodyType>,
  ctx: RestContext,
  statusCode: number,
  text: string
) => genResponse(res, ctx, statusCode, { message: text, error: text });

const genUnauthorizedResponse = (res: ResponseComposition<DefaultBodyType>, ctx: RestContext) =>
  genResponseWithError(res, ctx, 401, "Incorrect JWT token");

export { redirect, genResponse, genResponseWithError, genUnauthorizedResponse };
