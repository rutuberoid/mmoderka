import { rest } from "msw";
import { baseURLTest } from "@mimir/utils";

import { genResponse, genUnauthorizedResponse, isUserAuthenticated } from "./utils";
import { ACTION_REASONS } from "./DTOs";

const dictionaryHandlers = [
  // Get ban reasons
  rest.get(`${baseURLTest}/task/reasons`, (req, res, ctx) => {
    const isAuthenticated = isUserAuthenticated();

    if (!isAuthenticated) {
      return genUnauthorizedResponse(res, ctx);
    }

    return genResponse(res, ctx, 200, ACTION_REASONS);
  }),
];

export { dictionaryHandlers };
