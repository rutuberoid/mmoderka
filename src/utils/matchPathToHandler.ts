import type { MatchFunction } from "path-to-regexp";
import { match } from "path-to-regexp";
import type { URLParametersHandlerType } from "@mimir/types";

export const matchPathToHandler = (routeHandlers: Record<string, URLParametersHandlerType>) => {
  const matchers = Object.entries(routeHandlers).map(([path, pathHandler]) => [match(path), pathHandler]);
  const pathMatcher = (path: string) =>
    matchers.some(([matcher, pathHandler]) => {
      const matchedData = (matcher as MatchFunction<Record<string, string>>)(path);
      if (matchedData) {
        if (Reflect.ownKeys(matchedData.params).length > 0) {
          (pathHandler as URLParametersHandlerType)(matchedData.params);
        } else {
          (pathHandler as URLParametersHandlerType)();
        }
        return true;
      }
      return false;
    });
  return pathMatcher;
};
