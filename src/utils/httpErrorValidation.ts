import type { HttpErrorType, DisplayedErrorType } from "@mimir/types";
import { errorTranslations } from "@mimir/constants";

const httpErrorsMap = new Map([
  [0, "BadResponseError"],
  [400, "BadRequestError"],
  [401, "AccessDeniedError"],
  [403, "ForbiddenError"],
  [404, "NotFoundError"],
  [409, "ConflictError"],
  [500, "ServerError"],
]);

export class ServerErrorConstructor extends Error {
  constructor(errorName: string, serverError?: Record<string, unknown> | null) {
    let message = "";
    if (serverError) {
      message = JSON.stringify(serverError);
    }
    super(message);
    this.name = errorName;
    Object.setPrototypeOf(this, ServerErrorConstructor.prototype);
  }
}

export const revealHttpError = ({ status = 0, errorData, errorOptions }: HttpErrorType) => {
  const errorName = httpErrorsMap.get(status) || "HTTPError";
  const resultError = new ServerErrorConstructor(errorName, errorOptions?.shouldShowServerError ? errorData : null);

  return resultError;
};

export const identifyHttpError = (error: Error, options?: DisplayedErrorType) => {
  const identifiedError: {
    shouldLogout: boolean;
    clientMessage: string;
    serverError: Record<string, unknown> | null;
  } = { shouldLogout: false, clientMessage: "", serverError: null };

  switch (error.name) {
    case "AccessDeniedError":
      identifiedError.shouldLogout = true;
      identifiedError.clientMessage =
        typeof options?.accessError === "string"
          ? options.accessError
          : options?.genericError || errorTranslations.NOT_AUTHORIZED_ERROR;
      break;
    case "ForbiddenError":
      identifiedError.clientMessage =
        typeof options?.permissionError === "string"
          ? options.permissionError
          : options?.genericError || errorTranslations.PERMISSION_ERROR;
      break;
    case "BadRequestError":
      identifiedError.clientMessage =
        typeof options?.badRequestError === "string"
          ? options.badRequestError
          : options?.genericError || errorTranslations.REQUEST_ERROR;
      break;
    case "ConflictError":
      identifiedError.clientMessage =
        typeof options?.conflictError === "string"
          ? options.conflictError
          : options?.genericError || errorTranslations.HTTP_ERROR;
      break;
    case "NotFoundError":
      identifiedError.clientMessage =
        typeof options?.notFoundError === "string"
          ? options.notFoundError
          : options?.genericError || errorTranslations.HTTP_ERROR;
      break;
    case "ServerError":
      identifiedError.clientMessage =
        typeof options?.serverError === "string"
          ? options.serverError
          : options?.genericError || errorTranslations.HTTP_ERROR;
      break;
    case "BadResponseError":
      identifiedError.clientMessage =
        typeof options?.badResponseError === "string"
          ? options.badResponseError
          : options?.genericError || errorTranslations.EMPTY_RESPONSE_ERROR;
      break;
    default:
      identifiedError.clientMessage =
        typeof options?.unknownError === "string"
          ? options.unknownError
          : options?.genericError || errorTranslations.UNKNOWN_ERROR;
  }
  if (error.message) {
    identifiedError.serverError = JSON.parse(error.message);
  }
  return identifiedError;
};
