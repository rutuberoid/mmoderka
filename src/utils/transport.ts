import axios from "axios";
import { nanoid } from "nanoid";
import { TRACE_NAME } from "@mimir/constants";

const genTraceId = () => `mimir-${nanoid(12)}`;

const transport = axios.create({
  baseURL: API_URL,
});

transport.interceptors.request.use((config) => {
  config.headers = {
    ...config.headers,
    [TRACE_NAME]: genTraceId(),
  };
  return config;
});

transport.interceptors.response.use(
  (response) => response,
  (error) => {
    const status: number = error.response.status;
    const errorData: Record<string, unknown> = error.response.data || error.response;
    errorData[TRACE_NAME] = error.config.headers?.[TRACE_NAME];
    const httpError = { status, errorData };

    return Promise.reject(httpError);
  }
);

const baseURLTest = transport.defaults.baseURL;

export { transport, baseURLTest };
