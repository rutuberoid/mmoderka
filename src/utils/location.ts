const getMainDomain = () => {
  const protocol = window.location.protocol;
  const domains = window.location.host.split(".");
  let mainDomain = "";
  if (domains.length > 2) {
    mainDomain = `${domains[domains.length - 2]}.${domains[domains.length - 1]}`;
  } else {
    mainDomain = location.host;
  }

  return `${protocol}//${mainDomain}`;
};

export { getMainDomain };
