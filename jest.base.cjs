const config = {
  preset: "ts-jest/presets/default-esm",
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.json",
      useESM: true,
    },
    "NODE_ENV": "testing",
    "API_URL": "http://localhost/api",
  },
  rootDir: "./",
  roots: [
    "<rootDir>/src/store",
    "<rootDir>/src/components",
    "<rootDir>/src/utils",
    "<rootDir>/src/services",
    "<rootDir>/src/tests",
  ],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
  moduleDirectories: ["node_modules"],
  testPathIgnorePatterns: ["/node_modules/", "/dist"],
  moduleNameMapper: {
    "^@mimir/(.*)$": "<rootDir>/src/$1",
    "^@test/(.*)$": "<rootDir>/src/$1",
  },
  cacheDirectory: "<rootDir>/tmp/jest_rt",
  setupFilesAfterEnv: ["<rootDir>/tools/jest.setup.ts"],
  bail: 1,
  clearMocks: true,
  collectCoverage: false,
  coverageProvider: "v8",
  maxWorkers: 4,
  // Measured in seconds
  slowTestThreshold: 10,
};

module.exports = config;
