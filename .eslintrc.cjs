module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended", "plugin:react-hooks/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"],
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: "latest",
    sourceType: "module",
  },
  ignorePatterns: [".yarn", "*.cjs", "*.d.ts"],
  plugins: [
    "@typescript-eslint",
    // This plugin is from eslint-plugin-import
    "import",
    "react",
  ],
  settings: {
    react: {
      version: "detect",
    },
  },
  overrides: [
    {
      files: ["*.ts", "*.tsx", "*.js", "*.jsx"],
      /*
       * Check all eslint rules here:
       * https://eslint.org/docs/latest/rules/<name of the rule>
       * Check all typescript-eslint rules here:
       * https://typescript-eslint.io/rules/<name of the rule>
       */
      rules: {
        "curly": ["error", "all"],
        "array-callback-return": "error",
        "no-self-compare": "error",
        "arrow-body-style": ["error", "as-needed"],
        "camelcase": ["error", { properties: "always" }],
        "eqeqeq": "error",
        "func-names": ["error", "always"],
        "func-style": ["error", "expression"],
        "guard-for-in": "error",
        "id-denylist": ["error", "data", "err", "e", "cb", "callback", "result"],
        "id-length": ["error", { min: 2, max: 35, exceptions: ["_"] }],
        "import/order": [
          "error",
          {
            "groups": ["external", "builtin", "internal", "sibling", "parent", "index"],
            "newlines-between": "always",
          },
        ],
        "init-declarations": ["error", "always"],
        "multiline-comment-style": ["error", "separate-lines"],
        "new-cap": "error",
        "no-alert": "error",
        "no-array-constructor": "error",
        "no-console": "error",
        "no-else-return": "error",
        "no-extend-native": "error",
        "no-floating-decimal": "error",
        "no-inline-comments": "error",
        "no-lone-blocks": "error",
        "no-multi-assign": "error",
        "no-negated-condition": "error",
        "no-nested-ternary": "error",
        "no-param-reassign": "error",
        "no-restricted-exports": ["error", { restrictedNamedExports: ["default"] }],
        "no-restricted-globals": [
          "error",
          {
            name: "fdescribe",
            message: 'Do not commit "fdescribe". Use "describe" instead.',
          },
          {
            name: "fit",
            message: 'Do not commit "fit". Use "it" instead.',
          },
        ],
        "no-restricted-properties": [
          "error",
          {
            object: "describe",
            property: "only",
            message: 'Do not commit "describe.only". Use "describe" instead.',
          },
          {
            object: "test",
            property: "only",
            message: 'Do not commit "test.only". Use "it" instead.',
          },
          {
            object: "it",
            property: "only",
            message: 'Do not commit "it.only". Use "it" instead.',
          },
        ],
        "no-return-assign": "error",
        "no-shadow": "error",
        "no-throw-literal": "error",
        "no-unneeded-ternary": "error",
        "no-useless-catch": "error",
        "no-useless-computed-key": "error",
        "prefer-template": "error",
        "array-bracket-newline": ["error", { multiline: true }],
        "brace-style": ["error", "1tbs"],
        "function-call-argument-newline": ["error", "consistent"],
        "generator-star-spacing": ["error", { before: false, after: true }],
        "key-spacing": "error",
        "lines-around-comment": ["error", { afterBlockComment: false, beforeBlockComment: false }],
        "newline-per-chained-call": ["error", { ignoreChainWithDepth: 2 }],
        // [Other linters start] These rules are managed by prettier or typescript-eslint
        "quote-props": "off",
        "arrow-parens": "off",
        "no-unused-vars": "off",
        "no-duplicate-imports": "off",
        "no-empty-function": "off",
        // [Other linters end]
        // Rules for typescript-eslint
        "@typescript-eslint/no-empty-function": ["error", { allow: ["arrowFunctions"] }],
        "@typescript-eslint/no-unused-vars": ["error", { argsIgnorePattern: "_" }],
        "import/no-duplicates": "error",
        "import/no-default-export": "error",
        "@typescript-eslint/consistent-generic-constructors": "error",
        "@typescript-eslint/consistent-type-definitions": ["error", "type"],
        "@typescript-eslint/naming-convention": [
          "error",
          {
            selector: ["variable", "property"],
            types: ["boolean"],
            format: ["PascalCase"],
            prefix: ["is", "should", "has", "can", "did", "will", "are", "have", "was"],
            filter: {
              regex: "^__",
              match: false,
            },
          },
        ],
        "@typescript-eslint/consistent-type-imports": "error",
        "@typescript-eslint/member-delimiter-style": "error",
        "@typescript-eslint/no-require-imports": "warn",
        "@typescript-eslint/no-unnecessary-boolean-literal-compare": "error",
        "@typescript-eslint/prefer-string-starts-ends-with": "warn",
        "@typescript-eslint/prefer-ts-expect-error": "warn",
      },
    },
    {
      files: ["*.tsx", "*.jsx"],
      rules: {
        "react/hook-use-state": "error",
        "react/no-children-prop": ["error", { allowFunctions: true }],
        "react/jsx-no-target-blank": "error",
        "react/jsx-props-no-spreading": ["error", { exceptions: ["Button"] }],
        "react/jsx-key": "error",
        "react/destructuring-assignment": ["error", "always", { destructureInSignature: "always" }],
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "arrow-function",
            unnamedComponents: "function-expression",
          },
        ],
      },
    },
  ],
};
