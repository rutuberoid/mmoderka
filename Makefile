SHELL := /bin/bash
# NOTE: command below must precede any include operations
export ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
STAGED_FILES=$(shell git diff --cached --name-only --diff-filter=ACM)

.PHONY: eslint-perf
eslint-perf:
	@ TIMING=1 yarn eslint ./src

.PHONY: setup-git
setup-git:
	@if [ -n "$(GIT_MAIL)" ]; then \
		git config user.email "$(GIT_MAIL)"; \
	fi
	@if [ -n "$(GIT_NAME)" ]; then \
		git config user.name "$(GIT_NAME)"; \
	fi
	@git config pull.rebase true
	@git config commit.verbose true
	@git config core.hookspath .githooks

.PHONY: setup
setup: setup-git
	@yarn install
	@yarn cspell link add @cspell/dict-ru_ru

.PHONY: hooks
hooks:
	git config core.hookspath .githooks

.PHONY: add-spell-changes
add-spell-changes:
	@yarn cspell --words-only --unique $(STAGED_FILES) | sort --ignore-case >> project-words.txt
	@git add project-words.txt

.PHONY: bundle-analyze
bundle-analyze:
	@yarn build meta
	@xdg-open https://www.bundle-buddy.com/esbuild || open https://www.bundle-buddy.com/esbuild
	@echo "Upload file dist/esbuild.json on the opened website."

.PHONY: promote-prod
promote-prod:
	@if [ -z "$(IMAGE_TAG)" ]; then \
		echo "Cannot run this command without an \"IMAGE_TAG\" env variable"; \
		exit 1; \
	fi
	@curl -X POST \
		--fail \
		-F token=7b81eb0ed584a8f931e50c4ea9c05f \
		-F "ref=master" \
		-F "variables[IMAGE_TAG]=dev-$(IMAGE_TAG)" \
		https://gitlab.rutube.ru/api/v4/projects/626/trigger/pipeline
