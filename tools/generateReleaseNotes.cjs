const fetch = require("node-fetch");
const fs = require("fs");
const { resolve } = require("path");

require("dotenv").config({ path: resolve(".env") });

const {
  SPRINT_NUMBER,
  DIST_REGISTRY_RELEASE_NOTES,
  PROJECT_ACCESS_TOKEN,
  CI_API_V4_URL,
  CI_PROJECT_ID,
  CI_MERGE_REQUEST_IID,
} = process.env;
const FILE_NAME = `releaseNotes-${SPRINT_NUMBER}.json`;
const RELEASE_NOTES_API = `${DIST_REGISTRY_RELEASE_NOTES}${FILE_NAME}`;
const JIRA_URL = "https://jira.rutube.ru/browse";
const MERGE_REQUEST_URL = `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}`;
const API = {
  releaseNotes: `${DIST_REGISTRY_RELEASE_NOTES}${FILE_NAME}`,
  mergeRequest: MERGE_REQUEST_URL,
  commits: `${MERGE_REQUEST_URL}/commits`,
};

const loadReleaseNotes = async () => {
  try {
    const response = await fetch(RELEASE_NOTES_API);

    if (response.ok) {
      const data = await response.json();

      return data;
    }
  } catch (error) {
    console.log(error);
  }
};

const fetchData = async (url) => {
  try {
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${PROJECT_ACCESS_TOKEN}`,
      },
    });

    if (!response.ok) {
      throw new Error();
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.log(`Request to ${url} was failed`);
    throw new Error(error);
  }
};

const generateReleaseNotesRecord = async () => {
  try {
    const mergeRequest = await fetchData(API.mergeRequest);
    const mergeCommits = await fetchData(API.commits);

    const {
      iid,
      title,
      source_branch,
      author: { username },
      description,
      web_url: webUrl,
    } = mergeRequest;
    const [branchTypeName, issueInJira] = source_branch.split("/");
    const jiraUrl = `${JIRA_URL}/${issueInJira}`;
    const commits = mergeCommits.map(({ title }) => title);

    return { iid, branchTypeName, jiraUrl, title, username, description, webUrl, commits };
  } catch (error) {
    console.log("Release notes record was not generated");
    process.exit(1);
  }
};

const getReleaseNotesResult = (releaseNotes, record) => {
  if (!releaseNotes) {
    return { releaseNotes: [record] };
  }

  const existingMrIndex = releaseNotes.findIndex(({ iid }) => iid === record.iid);

  if (existingMrIndex === -1) {
    releaseNotes.push(record);
  } else {
    releaseNotes[existingMrIndex] = record;
  }

  return { releaseNotes };
};

const generateReleaseNotes = async () => {
  try {
    const response = await loadReleaseNotes();
    const record = await generateReleaseNotesRecord();
    const releaseNotes = getReleaseNotesResult(response?.releaseNotes, record);

    fs.writeFileSync(FILE_NAME, JSON.stringify(releaseNotes));
  } catch (error) {
    console.log(error);
  }
};

generateReleaseNotes();
