import React from "react";

if (SHOULD_RUN_WDYR) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports
  const whyDidYouRender = require("@welldone-software/why-did-you-render");
  whyDidYouRender(React, {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    trackAllPureComponents: true,
  });
}
