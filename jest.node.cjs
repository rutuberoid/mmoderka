const commonConfig = require("./jest.base.cjs");

const config = {
  ...commonConfig,
  testMatch: [
    "<rootDir>/src/tests/store/*.spec.[jt]s?(x)",
    "<rootDir>/src/tests/services/*.spec.[jt]s?(x)",
    "<rootDir>/src/tests/utils/*.spec.[jt]s?(x)",
  ],
  globals: {
    ...commonConfig.globals,
    window: {
      location: {
        href: "http://test2.test1.it/case",
        protocol: "http:",
        host: "test2.test1.it",
      },
    },
  },
  testEnvironment: "node",
};

module.exports = config;
