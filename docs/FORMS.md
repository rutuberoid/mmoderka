# Form store guidance

There are four types of fields:

- **input**
- **select**
- **combobox**
- **multiselect**

## Examples

Examples of usage are stored in - [docs/examples/forms](examples/forms)
