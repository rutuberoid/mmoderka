# Testing tips

### General rules

1. All test files must have `.spec.ts(x)` extension to let jest runner register them.
1. When testing react components, importing React is not necessary.
1. Don't use global (per file variables), inline them. The best way to do
   is to create a pure function, that will be invoked in each test case.
1. Don't import constants and more importantly don't assert against them.
   Assert only against variables or literals that exist only in that test case.

### Testing "store" modules

When testing **store** modules, use special alias **@test/store/models** instead of **@mimir/store** alias in order
to isolate each store from each other.
Stores have hooks and initialization process builtin on import, so it is strongly recommended to isolate the
imports.

Type imports can still use application wide aliases.

### Extensions

1. [Testing playground](https://chrome.google.com/webstore/detail/testing-playground/hejbmebodbijjdhflfknehhcgaklhano) - helps to find the best queries to select elements
