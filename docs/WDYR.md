# Why-Did-You-Render usage guide

### Get Started

1. Type in terminal `yarn build dev wdyr`
2. Open browser's console.
3. Do some actions in opened webpage
4. **WDYR** will log all unnecessary re-renders into console

### Road to perfection

In perfect case there should be none of **WDYR** logs.  
**WDYR** shows causes of components re-renders.
If it's possible to fix re-render - **fix it**.
