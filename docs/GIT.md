## Code Of Conduct

1. Strategies
   - Use `git rebase` for updating your local branch with the contents from _dev_ (or any other branch)
   - Use worktrees to switch faster between contexts. Install special **bash** command to facilitate
     worktrees creation `sudo install ./tools/gwa /usr/bin`. After installation use it `gwa branch .worktrees/directory`
   - Push all your changes in a local branch to the remote server at the end of the day.
   - Create **merge requests** any time you need help, don't forget to mark it as draft to make it clear for reviewers.
1. Housekeeping
   - The contents of **master** branch MUST be deployable at any time. Keep it clean!
   - Each ready merge request must have approval from a team lead.
   - Threads in any merge request can be ONLY resolved by their author.
   - Use only English for describing changes in commits
   - Clear stale branches in the remote repo
   - All _pull requests_ must have title and description according to the [team standards](https://confluence.rutube.ru/pages/viewpage.action?pageId=67996483).
   - All _pull requests_ must have correct labels assigned.

## Hooks

The project has tuned hooks. If not enabled, run `make hooks`.

1. `pre-commit` hook, that:
   - formats files. Enabled by default.
   - lints changes. Enabled by default.
   - outputs changes in the terminal for skimming. Enabled by default.
   - (optional) tests changes and related tests. Disabled by default
   - (optional) runs type checks. Disabled by default.
1. `prepare-commit-msg` prepares a message for commit according to team accepted format.
   NB :exclamation: If you are scared of vim or nano interface, you can set up your preferred text editor for git
   with this command `git config core.editor "code --wait"` - vscode example.
   Just replace the part in double quotes with the name of your editor.
   Personally, I suggest using vim. Oh boy, it is fast!
1. `commit-msg` hook lints a commit message according to [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
   that are based on [angular conventions](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type).
1. `pre-push` hook, that:
   - checks branch name
   - runs all tests. Enabled by default
   - runs type checks. Enabled by default.

:exclamation: NB: To trigger all the hooks in 100% cases you MUST use bash (if on Windows, use bash in WSL[1|2]) instead of GUI

```bash
# Usage:

# Omit message (that part after `-m` flag), if you want to generate standard commit message
git commit

# GENERATED EXAMPLE: `build(RMODF-1234): Set up commit messages linting`

# Use message, if you want to create custom commit message
git commit -m "I spit on your standards!!! Or just want to commit it asap"
```

> NB! Although you are free to use git as you wish, please remember,
> that these hooks allow the team to enforce team's best practices and to create uniform commit messages, that
> are later used for automatic [Changelog generation](https://github.com/conventional-changelog/standard-version).

**It is strongly recommended to respect the use of these hooks**

## Branch strategy - Gitflow

We are using classic [gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).
The only difference is that you are allowed to create features from features, read subtasks.

## Merge requests
### Merge request title

Each merge request title must be in **lowercase** English and include the following structure:

`<type of the branch>(<issue name>): <short description of the change>`

Example 1: `feat(rmodf-99): explain merge request naming convention`
Example 2: `release(1.1.1): release docs`

There are several allowed types:
- feat
- fix
- chore
- refactor
- hotfix

Issue name must have the format:
- [a-z]-[0-9]+ - for Jira issue names
- ([0-9]+\.)([0-9]+\.)([0-9]+) - for release versions
