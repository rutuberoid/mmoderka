import type { FormatValuesType, ParamsInitFieldType } from "@mimir/types";
import { types } from "mobx-state-tree";
import { FieldGroupStore } from "@test/store/models";

const EXAMPLE_FIELDS: ParamsInitFieldType[] = [
  // Field for a date picker
  {
    fieldAnchor: "datepicker",
    label: "Birth Date",
    placeholder: "Type your age",
    singleValue: "2022-09-29",
    isRequired: true,
  },
  // autocomplete with multiselect
  {
    fieldAnchor: "select",
    label: "Multiselect with autocomplete",
    hasCompletion: true,
    hasMultipleChoice: true,
    isRequired: true,
    placeholder: "Multiselect with autocomplete placeholder",
  },
  // Select with autocomplete
  {
    fieldAnchor: "select",
    label: "Select with autocomplete",
    hasCompletion: true,
    isRequired: true,
    placeholder: "Select with autocomplete placeholder",
  },
  // Select field
  {
    fieldAnchor: "select",
    label: "Common select",
    isRequired: true,
    placeholder: "Common select placeholder",
  },
  // Input
  {
    fieldAnchor: "input",
    label: "Full Name",
    placeholder: "Type your name",
    isRequired: true,
    requiredSign: "**",
  },
  // Simple multiselect
  {
    fieldAnchor: "multiselect",
    label: "Multiselect-old",
    hasMultipleChoice: true,
    isRequired: true,
    requiredSign: "***",
  },
  // TODO: two fields below supposed to be checkboxes
  {
    fieldAnchor: "checkbox1",
    label: "Get Free Money",
    isDisabled: true,
    singleValue: "",
    isRequired: true,
  },
  {
    fieldAnchor: "checkbox2",
    label: "Agree with Agreement",
    isRequired: true,
    singleValue: "",
    requiredSign: "****",
  },
];

type UpdateValuesType = () => Promise<FormatValuesType>;
type ExampleStoreVolatileType = {
  loadGenders: UpdateValuesType | null;
  loadSubscriptions: UpdateValuesType | null;
};

export const ExampleStore2 = types
  .compose(
    "ExampleStore2",
    FieldGroupStore,
    types.model({
      // Value that simplifies select options
      isSimplified: types.optional(types.boolean, false),
    })
  )
  .volatile(
    (): ExampleStoreVolatileType => ({
      loadGenders: null,
      loadSubscriptions: null,
    })
  )
  .actions((self) => ({
    updateLoadFunctions({ loadGenders, loadSubscriptions }: Partial<ExampleStoreVolatileType>) {
      if (typeof loadGenders === "function") {
        self.loadGenders = loadGenders;
      }
      if (typeof loadSubscriptions === "function") {
        self.loadSubscriptions = loadSubscriptions;
      }
    },
    generateDefaultFields() {
      EXAMPLE_FIELDS.forEach((field) => {
        self.initField(field);
      });

      self.fieldGroup.forEach(async (field) => {
        switch (field.fieldAnchor) {
          // These two fields should have options
          case "select":
            // Before update values we have to assign an update function
            field.updateFieldInitialDefinitions({
              defaultSelections: {
                list: [
                  {
                    id: "male",
                    label: "Male",
                  },
                  {
                    id: "female",
                    label: "Female",
                  },
                ],
                attributes: {
                  name: "label",
                  value: "id",
                },
              },
              dummySelection: {
                dummyName: "dumb",
                dummyValue: "dumbValue",
              },
            });
            // In case when isSimplified=true we don't want to load other values
            if (!self.isSimplified && self.loadGenders) {
              field.updateFieldInitialDefinitions({
                updateFunc: self.loadGenders,
              });
            }
            // After updateFunc assignment we call asynchronous function "updateValues"
            await field.updateValues();
            // Here we set first element's value as we know that
            // there will be at least two values after update
            field.setValue(field.values[0].value);
            break;
          case "multiselect":
            if (typeof self.loadSubscriptions === "function") {
              // Set update function
              field.updateFieldInitialDefinitions({
                updateFunc: self.loadSubscriptions,
                dummySelection: {
                  dummyName: "dumb",
                  dummyValue: "dumbValue",
                },
              });
              // Update values
              await field.updateValues();
              // Here we collect all option values except dumb value
              const optionValues = field.values.slice(1).map((option) => option.value);
              // Then we set them as active
              field.setValue(optionValues);
              // Probably field can be disable, so toggle it
              field.isDisabled && field.toggleDisabled(false);
            } else {
              // Disable field in case when "loadSubscriptions" function is not assigned
              field.toggleDisabled(true);
            }
            break;
        }
      });
    },
    changeField(fieldId: string, value: string) {
      // Doesn't change value in case when massDisable is turned on
      self.innerChangeField(fieldId, value);
    },
    touchField(fieldId: string) {
      // Safely field touch
      self.innerTouchField(fieldId);
    },
    collectFieldData() {
      try {
        // Throws an error when required field doesn't have a value
        self.detectError();
      } catch (error) {
        return error;
      }

      const fieldValues: Record<string, unknown> = {};

      // By field anchors we take values that we need
      // We pick singleValue/selected/multipleSelected knowing that exactly they contain the data
      self.fieldGroup.forEach((field) => {
        switch (field.fieldAnchor) {
          case "datepicker":
          case "input":
          case "checkbox1":
          case "checkbox2":
            fieldValues[field.fieldAnchor] = field.singleValue;
            break;
          case "select":
            fieldValues[field.fieldAnchor] = field.selected?.value;
            break;
          case "multiselect":
            fieldValues[field.fieldAnchor] = field.multipleSelected.map(({ value }) => value);
            break;
        }
      });

      return fieldValues;
    },
  }));
