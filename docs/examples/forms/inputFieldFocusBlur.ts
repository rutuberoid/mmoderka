import type { ParamsInitFieldType } from "@mimir/types";
import { types } from "mobx-state-tree";
import { FieldGroupStore } from "@test/store/models";

const EXAMPLE_FIELDS: ParamsInitFieldType[] = [
  // Input fields
  {
    fieldAnchor: "input1",
    label: "Input 1",
    placeholder: "The most important input",
    isRequired: true,
    requiredSign: "**",
    singleValue: "",
  },
  {
    fieldAnchor: "input2",
    label: "Input 1",
    isDisabled: true,
    singleValue: "",
  },
  {
    fieldAnchor: "input3",
    label: "Input 3",
    singleValue: "",
    requiredSign: "'",
  },
];

const inputBlurFirstFunc = (value: string) => value.split("").join("-");

const inputBlurSecondFunc = (value: string) => {
  if (value.includes("&")) {
    return value.split("&")[0];
  }
  return value.slice(0, 5);
};

const inputFocusFunc = (value: string) => value;

export const ExampleStore3 = types
  .compose("ExampleStore3", FieldGroupStore, types.model())
  .actions((self) => ({
    generateDefaultFields() {
      EXAMPLE_FIELDS.forEach((field) => {
        self.initField(field);
      });
      self.fieldGroup.forEach((field) => {
        switch (field.fieldAnchor) {
          case "input1":
            field.updateFieldInitialDefinitions({
              // blur and focus functions change 'singleValue' on blur/focus and
              // set the value to 'displayedSingleValue'
              blurFunc: inputBlurFirstFunc,
              focusFunc: inputFocusFunc,
            });
            break;
          case "input2":
          case "input3":
            field.updateFieldInitialDefinitions({
              blurFunc: inputBlurSecondFunc,
              focusFunc: inputFocusFunc,
            });
            break;
        }
      });
    },
    changeField(fieldId: string, value: string) {
      // Doesn't change value in case when massDisable is turned on
      self.innerChangeField(fieldId, value);
    },
    touchField(fieldId: string) {
      // Safely field touch
      self.innerTouchField(fieldId);
    },
    collectFieldData() {
      try {
        // Throws an error when required field doesn't have a value
        self.detectError();
      } catch (error) {
        return error;
      }

      const fieldValues: Record<string, unknown> = {};

      // As this example is simplified we can collect data from 'singleValue'
      self.fieldGroup.forEach((field) => {
        fieldValues[field.fieldAnchor] = field.singleValue;
      });

      return fieldValues;
    },
  }))
  .actions((self) => ({
    // this function calls by itself after create store
    afterCreate() {
      self.generateDefaultFields();
    },
  }));
