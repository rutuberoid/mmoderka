import type { ParamsInitFieldType } from "@mimir/types";
import { types } from "mobx-state-tree";
import { FieldGroupStore } from "@test/store/models";

const EXAMPLE_FIELDS: ParamsInitFieldType[] = [
  // Group of selects
  {
    fieldAnchor: "select1",
    label: "Field 1",
    isRequired: true,
    index: 1,
    singleValue: "",
  },
  {
    fieldAnchor: "select2",
    label: "Field 2",
    isRequired: true,
    index: 2,
    requiredSign: "#",
  },
  {
    fieldAnchor: "select3",
    label: "Field 3",
    placeholder: "some text 3",
    isRequired: true,
    requiredSign: "**",
    index: 3,
  },
  {
    fieldAnchor: "select4",
    label: "Field 4",
    placeholder: "some text 4",
    isRequired: true,
    requiredSign: "^",
    index: 4,
  },
  {
    fieldAnchor: "select5",
    label: "Field 5",
    placeholder: "some text 5",
    isRequired: true,
    requiredSign: "^",
    index: 5,
  },
];

const genSelectOptions = async () => {
  const list = [];

  for (let index = 1; index < 5; index++) {
    list.push({
      id: `option${index}`,
      label: `${index}@${`${Math.random()}`.slice(-5)}`,
    });
  }

  await new Promise((resolve) => setTimeout(resolve, 3000));

  return {
    list: list,
    attributes: {
      name: "label",
      value: "id",
    },
  };
};

const genSelectOtherOptions = async () => {
  await new Promise((resolve) => setTimeout(resolve, 2500));

  return {
    list: [
      {
        id: "optionQ1",
        label: "Option Q1",
      },
      {
        id: "optionQ2",
        label: "Option Q2",
      },
      {
        id: "optionQ3",
        label: "Option Q3",
      },
    ],
    attributes: {
      name: "label",
      value: "id",
    },
  };
};

export const ExampleStore1 = types
  .compose("ExampleStore1", FieldGroupStore, types.model())
  .actions((self) => ({
    generateDefaultFields() {
      EXAMPLE_FIELDS.forEach((field) => {
        self.initField(field);
      });

      self.fieldGroup.forEach(async (field) => {
        switch (field.fieldAnchor) {
          case "select1":
          case "select2":
            field.updateFieldInitialDefinitions({
              updateFunc: genSelectOptions,
              // dummy option to reset other options
              dummySelection: {
                dummyName: "dumb",
                isDisabled: true,
                dummyValue: "dumbValue",
              },
              // default values
              defaultSelections: {
                list: [{ id: "testId", name: "testName" }],
                attributes: {
                  value: "id",
                  name: "name",
                },
              },
            });
            // createSortFunc sets 'field.orderFunc' and sorts options on 'updateValues'
            field.createSortFunc({
              sortOrder: "desc",
            });
            break;
          case "select3":
          case "select4":
          case "select5":
            field.updateFieldInitialDefinitions({
              updateFunc: genSelectOtherOptions,
            });
            break;
        }
        await field.updateValues();
      });
    },
    changeField(fieldId: string, value: string) {
      // Doesn't change value in case when massDisable is turned on
      self.innerChangeField(fieldId, value);
    },
    touchField(fieldId: string) {
      // Safely field touch
      self.innerTouchField(fieldId);
    },
    collectFieldData() {
      try {
        // Throws an error when required field doesn't have a value
        self.detectError();
      } catch (error) {
        return error;
      }

      const fieldValues: Record<string, unknown> = {};

      // As we have only selects in this example, we can simplify value collection
      self.fieldGroup.forEach((field) => {
        if (field.selected) {
          fieldValues[field.fieldAnchor] = field.selected.value;
        }
      });

      return fieldValues;
    },
  }))
  .actions((self) => ({
    // this function calls by itself after create store
    afterCreate() {
      // generate fields
      self.generateDefaultFields();

      // Getting all necessary fields
      const firstField = self.getFieldByAttribute({ anchor: "select1" });
      const secondField = self.getFieldByAttribute({ anchor: "select2" });
      const thirdField = self.getFieldByAttribute({ anchor: "select3" });
      const forthField = self.getFieldByAttribute({ anchor: "select4" });

      // set 'select2' as next for 'select1'
      if (firstField && secondField) {
        firstField.setChildren(secondField.id);
      }

      // set ['select3','select4'] as children for 'select2'
      if (secondField && thirdField && forthField) {
        secondField.setChildren([thirdField.id, forthField.id]);
      }

      // P.S. when we change the value of some field that has next field(s)
      // it causes a chain updates of all connected children fields
      // not just reset their values but call 'updateValues' function for each of them

      // Just because some fields are children of other fields
      // doesn't disabled them while previous fields doesn't have values

      // select5 is not connected to other fields so
      // it wouldn't update it's values when another fields change
    },
  }));
