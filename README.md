<h1 align="center">Mimir</h1>

As the Norse god, who carried his wisdom and remembered everything, our project helps moderators
to see all the content throughout time on Rutube and distinguish between the bad and good one.

# Glossary and Indication

Text formatting indication:

- `text` - commands and code dictionary terms
- _text_ - files and file paths
- **text** - business logic terms and emphasis text

# Homepage

1. dev -
1. prod -

# Package managers

Use yarn (>=3.0.0) with this repo and node (>=16.10.0) with [corepack](https://nodejs.org/dist/latest/docs/api/corepack.html) support.

```bash
# run these commands after node is installed
corepack enable
corepack prepare yarn@3.2.1 --activate
make setup
# OR to configure git also (use your company gitlab credentials)
make setup GIT_NAME="Kononov Zakhar" GIT_MAIL="zkononov@rutube.ru"
```

Now you are ready to launch, but it is strongly recommended to read the docs thoroughly.

```bash
yarn build dev
```

# GIT

### Requirements

Git version must be above 2.

We are using classic [gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).
The only difference is that you are allowed to create features from features, read subtasks.

Setup GIT config by running this command (use your gitlab name and email): `make setup-git GIT_NAME="Kononov Zakhar" GIT_MAIL="zkononov@rutube.ru"

You **MUST** read [introduction into GIT](./docs/GIT.md) for this project.

# Application architecture

### View

We are using React 18 as a view engine.

    THE CORE RULE: use React to display the data and to register all
    events from the user. ALL business logic processing must be done in
    View-Model layer (Mobx-State-Tree as a solution)

Since React 17 a developer can opt out of importing React in the _jsx/tsx_
files. We chose to import React explicitly, as it greatly reduces bundle size (saves around 1.5Kb)

### View-Model

We are using Mobx-State-Tree as a solution for a View-Model processing.

This layer calls Model layer and passes down business logic values to the View layer. In response
to events from the View layer (user actions) MST changes business logic values and repeats its
communication with other layers.

### Model

This layer is responsible for communication with data outside of our application (browser API, application server API).
Model layer has public interface that can be called only be View-Model layer.

# File structure

### Application file structure

File structure of this application is organized according to [Onion architecture](https://www.codeproject.com/Articles/1028481/Understanding-Onion-Architecture-2).

> DEFINITION: The inner level can't depend on its outer layer but can depend on layers beneath.

Each module has eslint restrictions rules, that will detect prohibited imports.

The table beneath explains the dependency graph of the app.

| **Can module in the row import modules in columns?** | **store** | **components** | **constants** | **services** | **types** | **utils** | **stories** | **tests** | **app** | **mocks** |
| :--------------------------------------------------: | :-------: | :------------: | :-----------: | :----------: | :-------: | :-------: | :---------: | :-------: | :-----: | :-------: |
|                      **store**                       |     -     |       no       |      yes      |     yes      |    yes    |    yes    |     no      |    no     |   no    |    no     |
|                    **components**                    |  [no][2]  |       -        |      yes      |      no      |    yes    |    yes    |     no      |    no     |   no    |    no     |
|                    **constants**                     |    no     |       no       |       -       |      no      |    yes    |    no     |     no      |    no     |   no    |    no     |
|                     **services**                     |    no     |       no       |      yes      |      -       |    yes    |    yes    |     no      |    no     |   no    |    no     |
|                      **types**                       |    no     |       no       |      yes      |      no      |     -     |    no     |     no      |    no     |   no    |    no     |
|                      **utils**                       |    no     |       no       |      yes      |      no      |    yes    |     -     |     no      |    no     |   no    |    no     |
|                     **stories**                      |    yes    |      yes       |      no       |      no      |    yes    |    yes    |      -      |    no     |   yes   |    no     |
|                      **tests**                       |  [no][1]  |      yes       |      no       |     yes      |    yes    |    yes    |     no      |     -     |   no    |    yes    |
|                       **app**                        |    yes    |      yes       |      yes      |      no      |    yes    |    yes    |     no      |    no     |    -    |    yes    |
|                      **mocks**                       |    no     |       no       |      no       |      no      |    yes    |    yes    |     no      |    no     |   no    |     -     |

### Module file structure

Each module must have a flat structure and export all necessary files to other files via transport file (_index.ts_). Although,
if it is really necessary, files can be grouped under directories in the module.

    It is important to reexport in this transport file all files that are used outside this module, as
    these transport files serve as entry points for the bundler.

This will help to treat any module as a flat structure and also solve circular dependency problem between modules.
Internal imports (imports within the same module) must be also done via this transport file.

```bash
./src
├── app
│   └── .eslintrc.cjs
├── components
│   ├── .eslintrc.cjs
│   ├── icons
│   │   └── index.ts
│   ├── index.ts
│   └── styles
│       └── index.ts
├── constants
│   ├── .eslintrc.cjs
│   └── index.ts
├── services
│   ├── .eslintrc.cjs
│   └── index.ts
├── store
│   ├── .eslintrc.cjs
│   ├── index.ts
│   ├── models
│   │   └── index.ts
│   └── utils
│       └── index.ts
├── stories
│   └── .eslintrc.cjs
├── tests
│   └── .eslintrc.cjs
├── types
│   ├── .eslintrc.cjs
│   └── index.ts
└── utils
    ├── .eslintrc.cjs
    └── index.ts
```

### Imports

All imports must be done through transport files - _index.ts_, to evade the problem of circular dependencies.

- If you need to import file from **another** module, use alias, e.g., `@mimir/store`.
- If you need to import file from **the same** module, use relative path.

NOTE! **mocks** module is an exception and doesn't need a transport file. This is done to highlight
the importance of the separation between browser and node mocks.

#### Caveats

1: Material-ui has some strange behavior, when importing components directly and not through transport file.
For example, this `import AbcOutlined from '@mui/icons-material/AbcOutlined';` won't bundle, when this
`import {AbcOutlined} from '@mui/icons-material';` will. So if you want your application to work use
destructuring, when import `@mui/*` components.

# Bundler

This project uses [esbuild](https://esbuild.github.io/) as a bundling tool.

- The bundler is configured to bundle only script files and assets. If you need to bundle other
  file types, for example, yml, check out [existing plugins](https://github.com/esbuild/community-plugins) for that.
- Entry point of the application is located in _dist/index.html_. :exclamation: Do not remove this directory.
- The output bundle is using ESM format, because this is the only way for esbuild to split the code.
- There are two ways to launch the project: in production mode and in development mode (with or without mock API).

```bash
# Run this command to clear dist/static directory and bundle the project.
yarn build

# Run this command to clear dist/static directory, bundle the project and launch
# dev server in watch mode.
yarn build dev

# Run this command to clear dist/static directory, bundle the project and launch
# dev server in watch mode.
yarn build dev mock-api
```

### Development mode

Note that _index.html_ loads a script file with `module` attribute. This directive enables several
strict rules, like CORS. So to make a browser load our script file, _index.html_ must be served
by a server, which must return valid CORS headers. In development mode this is achieved by
[browsersync](https://browsersync.io/).

### Bundle analysis

If you want to see how much the bundle weights, the project
has a make command, that will generate a meta file, that can be used
in bundle analysis services, like [bundle-buddy](https://www.bundle-buddy.com/esbuild).
Just upload _dist/esbuild.json_ file on the website and check the result.

```bash
# Run this command to generate build meta file
make bundle-analyze
```

### Code splitting

All unused imports are shaken away by esbuild, that check `sideEffects` property in _package.json_.
Be careful with adding files to `sideEffects` list, as it may break the application or increase the bundle size. Default value (`false`)
removes all unused chunks of code, this can have a detrimental effect on imports, that are not used in
an importing module (for example, a configuration for singleton axios). More on [sideEffects property](https://webpack.js.org/guides/tree-shaking/#mark-the-file-as-side-effect-free).

The code splitting helps in building the codebase incrementally in response to changes in certain modules. Plus
it works greatly with browser caching capabilities.

# Linters

### Spell check

The project has spell checker enabled - [cspell](https://cspell.org/docs/getting-started/), to help you write correctly in English.
The check is run during **pre-commit** stage, as the result the terminal will abort the commit process
and highlight any spelling errors.

If you think that highlighted words are correctly written, add them all into the dictionary file, and
remove one by one those words, that can be considered as real mistakes.
After that, correct manually all mistakes in the files, outputted by cspell util.

```bash
# For example, add all spelling mistakes from all markdown files to the dictionary.
# (Don't forget to remove real mistakes)
yarn cspell --words-only --unique "**/*.md" | sort --ignore-case >> project-words.txt
```

The project has make directive, that allows adding spelling mistakes only from staged files

```bash
# Add unknown words from staged files to project dictionary
# (Don't forget to remove real mistakes)
make add-spell-changes
```

# How-to's

- [How to test](./docs/TESTING.md)
- [How to GIT](./docs/GIT.md)
- [How to WDYR](./docs/WDYR.md)

# Legacy

1. https://gitlab.rutube.ru/rutube/camel
1. https://gitlab.rutube.ru/rutube/manul

[1]: _store_ module can be imported in _tests_ module via **@test/store/models** alias, that was defined for this case.
This alias helps to isolate models from each other in unit tests and to evade creation of the application store, that happens
on **@mimir/store** import.

[2]: Sometimes we need types from store in components. It's an exceptional case when we can import from store module into components module.
