#!/usr/bin/env bash
# Place here only variables that don't depend on git hooks
declare -A TYPES=( 
  [build]="build - Build or config change"
  [ci]="ci - CI change"
  [docs]="docs - Documentation change"
  [feat]="feat - New feature"
  [fix]="fix - Fixes and bugfixes"
  [perf]="perf - Performance improvement"
  [refactor]="refactor - Refactoring"
  [style]="style - Code style change"
  [test]="test - Add or change tests"
  [wip]="wip - Work in progress"
)

PROJECT="(RMODF|INFRA)"
BRANCH_NAME="$(git rev-parse --abbrev-ref HEAD)"
ISSUE=$(echo "${BRANCH_NAME##*/}" | tr '[:upper:]' '[:lower:]')
RELEASE_REGEX="release\/([0-9]+\.)([0-9]+\.)([0-9]+)(-rc[0-9]+)?"
BRANCH_REGEXP="^((feat|fix|chore|refactor|hotfix)\/${PROJECT}-[0-9]+)|(master|dev|$RELEASE_REGEX)$"
COMMIT_REGEXP="^($(sed "s/ /|/g" <<< "${!TYPES[@]}"))\(${ISSUE}\): .+"

