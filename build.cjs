const { build } = require("esbuild");
const isCi = require("is-ci");
const browserSync = require("browser-sync");
const chalk = require("chalk");
const fs = require("fs");

const myArgs = process.argv.slice(2);

const generateBuild = async (args) => {
  const isDev = args.includes("dev");
  const shouldRunWDYR = args.includes("wdyr");
  const shouldMockAPI = args.includes("mock-api");
  const shouldProduceMeta = args.includes("meta");
  let bs = null;
  if (isDev) {
    bs = browserSync.create();
  }
  if (fs.existsSync("./dist/static/")) {
    fs.rmSync("./dist/static", { recursive: true });
  }
  let buildResult = null;

  try {
    buildResult = await build({
      metafile: shouldProduceMeta,
      charset: "utf8",
      tsconfig: "./tsconfig.build.json",
      bundle: true,
      logLevel: isDev ? "silent" : "info",
      entryPoints: [
        "./src/app/index.tsx",
        "./src/components/index.ts",
        "./src/store/index.ts",
        "./src/utils/index.ts",
        "./src/constants/index.ts",
        "./src/services/index.ts",
      ],
      outdir: "dist/static/",
      format: "esm",
      loader: {
        ".js": "jsx",
        // image file format loaders
        ".png": "file",
        ".bmp": "file",
        ".jpg": "file",
        ".jpeg": "file",
        ".webp": "file",
        // font file format loaders
        ".woff": "file",
        ".ttf": "file",
      },
      color: !isCi,
      outbase: "src",
      minify: !isDev,
      sourcemap: isDev,
      splitting: true,
      plugins: [],
      target: "esnext",
      incremental: isDev,
      publicPath: isDev ? "/static" : process.env.PUBLIC_PATH,
      define: {
        NODE_ENV: JSON.stringify(isDev ? "development" : "production"),
        API_URL: JSON.stringify(isDev ? "/api" : process.env.API_URL),
        SHOULD_MOCK_API: JSON.stringify(!!shouldMockAPI),
        SHOULD_RUN_WDYR: JSON.stringify(!!shouldRunWDYR),
      },
    });
    if (shouldProduceMeta) {
      if (fs.existsSync("./dist")) {
        fs.writeFile("./dist/esbuild.json", JSON.stringify(buildResult.metafile), (err) => {
          if (err) {
            console.log("\n", chalk.bgRed.white(" Build failed. Metafile write error. "));
            return;
          }
        });
      }
    }
    console.log("\n", chalk.bgGreen.white(" Build was successful. "));
  } catch (error) {
    console.log("\n", chalk.bgRed.white(" Build failed. "));
    console.error(error);
    process.exit(1);
  }
  if (isDev) {
    const port = 8081;
    console.log("\n", chalk.cyan(`Launching dev server at http://localhost:${port}!`));
    const ignoredPaths = ["./src/tests", "./src/mocks", "./src/stories"];

    // Server initialization
    bs.init({
      ui: {
        port: port + 1,
      },
      startPath: "/",
      port,
      cors: true,
      logLevel: "info",
      logFileChanges: true,
      notify: true,
      // Provide separate index.html
      single: true,
      server: {
        baseDir: "dist",
        index: "index.html",
      },
      files: "src/",
      watchOptions: {
        ignoreInitial: true,
        ignored: ignoredPaths,
      },
    });

    // Listening for changes under the src folder
    bs.watch(["src/"], { ignored: ignoredPaths }).on("change", async (filename) => {
      console.log("\n", chalk.bgCyan.white(`Source file changed - ${filename}`));
      // Repackaging
      buildResult.rebuild();
    });
  }
};
generateBuild(myArgs);
