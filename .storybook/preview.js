import { CssBaseline, ThemeProvider } from "@mui/material";
import { customTheme } from "../src/app/theme";
import { StoreProvider } from "../src/store";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  (Story) => (
    <StoreProvider>
      <ThemeProvider theme={customTheme}>
        <CssBaseline />
        <Story />
      </ThemeProvider>
    </StoreProvider>
  ),
];
